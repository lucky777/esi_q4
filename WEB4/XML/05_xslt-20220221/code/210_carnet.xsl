<?xml version="1.0" ?>

<!-- xsltproc -v -o 210_carnet_output_xsltproc.xml 210_carnet.xsl 210_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:210_carnet.xml -xsl:210_carnet.xsl -o:210_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 210_carnet.xml -XSL 210_carnet.xsl -OUT 210_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="carnet">
        <!-- copie de l'élément courant, ses attributs et de sa descendance -->
        <xsl:copy-of select="." />
        ee
    </xsl:template>

</xsl:stylesheet>
