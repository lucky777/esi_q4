<?xml version="1.0" ?>

<!-- xsltproc -v -o 150_carnet_output_xsltproc.html 150_carnet.xsl 150_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:150_carnet.xml -xsl:150_carnet.xsl -o:150_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 150_carnet.xml -XSL 150_carnet.xsl -OUT 150_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <title>Carnet</title>
            </head>
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="personne">
        <h2>

            <xsl:number count="personne" level="single" format="a/" />
            <xsl:text> </xsl:text>

            <xsl:value-of select="@titre" />

            <xsl:text> </xsl:text>

            <xsl:value-of select="prenom" />

            <xsl:text> </xsl:text>

            <xsl:value-of select="nom" />
        </h2>

        <xsl:apply-templates select="adresse" />

    </xsl:template>

    <xsl:template match="adresse">

        <xsl:value-of select="rue" />,

        <xsl:value-of select="numero" /><br/>

        <xsl:value-of select="codepostal" />

        <xsl:text> </xsl:text>

        <xsl:value-of select="ville" /><br/>

        <xsl:value-of select="pays" />
    </xsl:template>

</xsl:stylesheet>
