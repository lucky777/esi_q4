<?xml version="1.0" ?>

<!-- xsltproc -v -o 080_carnet_output_xsltproc.html 080_carnet.xsl 080_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:080_carnet.xml -xsl:080_carnet.xsl -o:080_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 080_carnet.xml -XSL 080_carnet.xsl -OUT 080_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="text" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <!-- <!DOCTYPE html> -->
        <!-- KO -->
        <!-- <xsl:text disable-output-escaping="yes"> <!DOCTYPE html> </xsl:text> -->
        <!-- KO car effet sur la manière dont les caractères sont codés dans le document produit, pas sur l'input -->
        &lt;!DOCTYPE html&gt;
        <!-- OK -->
        <!-- <xsl:text> &lt;!DOCTYPE html&gt; </xsl:text> -->
        <!-- OK -->
        <!-- <xsl:text disable-output-escaping="no"> &lt;!DOCTYPE html&gt; </xsl:text>
        <xsl:text disable-output-escaping="yes"> &lt;!DOCTYPE html&gt; </xsl:text> -->
        <!-- je ne vois pas de différence... -->

        <!-- <html> -->
        <!-- KO -->
        <!-- &lt;html&gt; -->
        <!-- OK -->
        <!-- <xsl:text disable-output-escaping="yes"> <html> <head> </xsl:text> -->
        <!-- KO -->
        &lt;html&gt; 
        
        &lt;head&gt; 
            &lt;meta charset="UTF-8"/&gt; 
            &lt;title&gt;Carnet&lt;/title&gt; 
        &lt;/head&gt; 
        
        &lt;body&gt;

            <xsl:apply-templates />
        &lt;/body&gt; 
        
        &lt;/html&gt;
    </xsl:template>

</xsl:stylesheet>
