<?xml version="1.0" ?>

<!-- xsltproc -v -o 270_carnet_output_xsltproc.xml 270_carnet.xsl 270_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:270_carnet.xml -xsl:270_carnet.xsl -o:270_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 270_carnet.xml -XSL 270_carnet.xsl -OUT 270_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />

    <xsl:variable name="carnet_tag_ko">
        notebook_ko
    </xsl:variable>
    <!-- ceci passe avec saxon, mais pas avec xsltproc (no output) ni avec xalan (output mal formé) : voir plus bas utilisation des variables carnet_tag_ko et carnet_tag_ok -->
    <!-- xsltproc :
    runtime error: file 270_carnet.xsl line 63 element element
    xsl:element: The effective name '
        notebook_ko
    ' is not a valid QName. -->
    <!-- xalan :
    file:///C:/path-to/05_xslt/enonce/code/270_carnet.xsl; Ligne #71; Colonne #46; Valeur incorrecte pour l'attribut name :
        notebook_ko -->

    <xsl:variable name="carnet_tag_ok">notebook_ok</xsl:variable>

    <!-- <xsl:variable name="personne_tag">
        person
    </xsl:variable>

    <xsl:variable name="nom_tag">
        name
    </xsl:variable>

    <xsl:variable name="prenom_tag">
        firstname
    </xsl:variable>

    <xsl:variable name="titre_tag">
        title
    </xsl:variable>

    <xsl:variable name="adresse_tag">
        address
    </xsl:variable>

    <xsl:variable name="rue_tag">
        street
    </xsl:variable>

    <xsl:variable name="numero_tag">
        number
    </xsl:variable>

    <xsl:variable name="codepostal_tag">
        postalcode
    </xsl:variable>

    <xsl:variable name="ville_tag">
        town
    </xsl:variable>

    <xsl:variable name="pays_tag">
        country
    </xsl:variable> -->

    <xsl:template match="text()" />

    <xsl:template match="/">
        <!-- <xsl:element name="{$carnet_tag_ko}"> -->
        <xsl:element name="{$carnet_tag_ok}">
        <!-- rnvs : essayer $carnet_tag_ko et $carnet_tag_ok -->
            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>

    <xsl:template match="personne">
        <xsl:copy-of select="." />
        <xsl:apply-templates />
    </xsl:template>

</xsl:stylesheet>
