<?xml version="1.0" ?>

<!-- xsltproc -v -o 160_carnet_output_xsltproc.html 160_carnet.xsl 160_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:160_carnet.xml -xsl:160_carnet.xsl -o:160_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 160_carnet.xml -XSL 160_carnet.xsl -OUT 160_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <title>Carnet</title>
            </head>
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="text()">
        <!-- rien -->
    </xsl:template>

    <xsl:template match="personne">
        <h2>

            <xsl:number count="personne" level="single" format="1/" />
            <xsl:text> </xsl:text>

            <xsl:value-of select="@titre" />

            <xsl:text> </xsl:text>

            <xsl:value-of select="prenom" />

            <xsl:text> </xsl:text>

            <xsl:value-of select="nom" />
        </h2>

        <xsl:apply-templates/>

    </xsl:template>

    <xsl:template match="adresse">

        <xsl:value-of select="rue" />,

        <xsl:value-of select="numero" /><br/>

        <xsl:value-of select="codepostal" />

        <xsl:text> </xsl:text>

        <xsl:value-of select="ville" /><br/>

        <xsl:value-of select="pays" />
    </xsl:template>

</xsl:stylesheet>
