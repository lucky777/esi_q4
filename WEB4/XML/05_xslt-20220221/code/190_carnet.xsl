<?xml version="1.0" ?>

<!-- xsltproc -v -o 190_carnet_output_xsltproc.xml 190_carnet.xsl 190_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:190_carnet.xml -xsl:190_carnet.xsl -o:190_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 190_carnet.xml -XSL 190_carnet.xsl -OUT 190_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="carnet">
        <carnet>
            <xsl:value-of select="." />
            <!-- conversion d'un nœud (node) en string : concaténation de ses nœuds textuels dans l'ordre du document -->
        </carnet>
    </xsl:template>

</xsl:stylesheet>
