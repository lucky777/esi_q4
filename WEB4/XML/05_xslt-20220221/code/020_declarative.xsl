<!-- procedural example -->

<!-- xsltproc -v -o 020_carnet_declarative_output_xsltproc.xml 020_declarative.xsl 020_carnet_declarative.xml -->

<!-- java -jar saxon-he-10.0.jar -s:020_carnet_declarative.xml -xsl:020_declarative.xsl -o:020_carnet_declarative_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 020_carnet_declarative.xml -XSL 020_declarative.xsl -OUT 020_carnet_declarative_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:transform xmlns="https://esi-bru.be/WEBR4/XML" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="xml" indent="yes" />

    <!-- pour conserver les whitespace-only text nodes -->
    <!-- <xsl:preserve-space elements="*" /> -->
    <!-- à l'exception de ceux de -->
    <!-- <xsl:strip-space elements="carnet" /> -->
    <!-- selon le processeur xslt : comportement un peu différent avec strip-space : xsltproc un peu bizarre... -->
    <!-- rem. : je ne comprends pas très bien l'effet de ces éléments car les résultats sont assez différent d'un processeur à l'autre -->

    <!-- root template: main entry point -->
    <xsl:template match="/">
        <booknote>
            <xsl:apply-templates />
            <!-- commenter la ligne ci-dessus -->
            <!-- <xsl:apply-templates select="carnet/personne"/> -->
        </booknote>
    </xsl:template>

    <xsl:template match="personne">
        <person>
            <title>
                <xsl:value-of select="@titre" />
            </title>
            <name>
                <xsl:value-of select="concat(prenom, ' ', nom)" />
            </name>
        </person>
        <!-- <xsl:apply-templates /> -->
        <!-- décommenter la ligne ci-dessus -->
    </xsl:template>

</xsl:transform>
