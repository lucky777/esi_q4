<?xml version="1.0"?>

<!-- xsltproc -v -o 060_carnet_default_explicit_output_xsltproc.txt 060_carnet_default_explicit.xsl 060_carnet_default_explicit.xml -->

<!-- java -jar saxon-he-10.0.jar -s:060_carnet_default_explicit.xml -xsl:060_carnet_default_explicit.xsl -o:060_carnet_default_explicit_output_saxon.txt -->

<!-- java -jar xalan.jar -IN 060_carnet_default_explicit.xml -XSL 060_carnet_default_explicit.xsl -OUT 060_carnet_default_explicit_output_xalan.txt -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="text" />

    <xsl:template match="*|/">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="text()">
        <xsl:value-of select="." />
    </xsl:template>


</xsl:stylesheet>
