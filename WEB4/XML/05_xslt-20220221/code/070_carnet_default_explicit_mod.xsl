<?xml version="1.0"?>

<!-- xsltproc -v -o 070_carnet_default_explicit_mod_output_xsltproc.txt 070_carnet_default_explicit_mod.xsl 070_carnet_default_explicit_mod.xml -->

<!-- java -jar saxon-he-10.0.jar -s:070_carnet_default_explicit_mod.xml -xsl:070_carnet_default_explicit_mod.xsl -o:070_carnet_default_explicit_mod_output_saxon.txt -->

<!-- java -jar xalan.jar -IN 070_carnet_default_explicit_mod.xml -XSL 070_carnet_default_explicit_mod.xsl -OUT 070_carnet_default_explicit_mod_output_xalan.txt -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="text" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="*|/">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="text()">
        &lt;&lt;aa
        <xsl:value-of select="." />
        aa&gt;&gt;
    </xsl:template>

</xsl:stylesheet>
