<?xml version="1.0"?>

<!-- xsltproc -v -o 136_carnet_output_xsltproc.html 136_carnet.xsl 136_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:136_carnet.xml -xsl:136_carnet.xsl -o:136_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 136_carnet.xml -XSL 136_carnet.xsl -OUT 136_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <title>Carnet</title>
            </head>
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="text()" />
    <!-- avec ceci les éléments texte ne sont plus recopiés dans html produit, c'est une surcharge du template par défaut des nœuds texte (voir 060_carnet_default_explicit.xsl) -->

    <xsl:template match="personne">
        <h2>

            <xsl:value-of select="@titre" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="prenom" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="nom" />
        </h2>

        <!-- <xsl:apply-templates select="adresse" /> -->

        <xsl:apply-templates />
        <!-- même avec ceci : nom & prénom _pas_ affichés une 2e fois après le titre car text() par recopié -->

    </xsl:template>

    <xsl:template match="adresse">
        <xsl:value-of select="rue" />
        ,
        <xsl:value-of select="numero" />
        <br />
        <xsl:value-of select="codepostal" />
        <xsl:text> </xsl:text>
        <xsl:value-of select="ville" />
        <br />
        <xsl:value-of select="pays" />

        <!-- <xsl:apply-templates /> -->
        <!-- même avec apply-templates, les adresses ne sont pas affichées 2 x car text() pas recopiés en output -->
    </xsl:template>

</xsl:stylesheet>
