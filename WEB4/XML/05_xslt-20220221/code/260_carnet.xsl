<?xml version="1.0" ?>

<!-- xsltproc -v -o 260_carnet_output_xsltproc.xml 260_carnet.xsl 260_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:260_carnet.xml -xsl:260_carnet.xsl -o:260_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 260_carnet.xml -XSL 260_carnet.xsl -OUT 260_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />

    <xsl:variable name="carnet_tag">notebook</xsl:variable>
    <!-- essentiellement identique à 240_carnet.xsl -->

    <xsl:template match="text()" />
    <!-- empêche l'affichage des textes -->

    <xsl:template match="/">
        <xsl:element name="{$carnet_tag}">
            <xsl:apply-templates />
            <!-- Q. : et si ce apply-templates est mis en commentaire ? -->
        </xsl:element>
    </xsl:template>

    <xsl:template match="personne">
        <xsl:copy-of select="." />
        <xsl:apply-templates />
        <!-- pas de réaffichage car pas d'affichage des textes -->
    </xsl:template>

</xsl:stylesheet>
