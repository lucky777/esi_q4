<?xml version="1.0" ?>

<!-- xsltproc -v -o 300_carnet_output_xsltproc.xml 300_carnet.xsl 300_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:300_carnet.xml -xsl:300_carnet.xsl -o:300_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 300_carnet.xml -XSL 300_carnet.xsl -OUT 300_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<!-- équivalent à 280_carnet.xsl, avec ici xsl:choose dans la fonction plutôt que lors de l'appel -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />

    <xsl:template match="carnet">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">notebook</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="personne">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">person</xsl:with-param>
            <xsl:with-param name="elt_attribute_name">title</xsl:with-param>
            <xsl:with-param name="elt_attribute_value"><xsl:value-of select="@titre"/></xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="nom">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">name</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="prenom">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">firstname</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="adresse">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">address</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="rue">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">street</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="numero">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">number</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="codepostal">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">postalcode</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="ville">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">town</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="pays">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">country</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="create_elt">

        <xsl:param name="elt_name" />

        <xsl:param name="elt_attribute_name" />

        <xsl:param name="elt_attribute_value" />

        <xsl:element name="{$elt_name}">

            <xsl:if test="string-length($elt_attribute_name) > 0">

                <xsl:attribute name="{$elt_attribute_name}">

                    <xsl:choose>
                        <xsl:when test="$elt_attribute_value = 'Mme'">Mrs</xsl:when>
                        <xsl:when test="$elt_attribute_value = 'M.'">Mr</xsl:when>
                        <xsl:otherwise><xsl:value-of select="$elt_attribute_value" /></xsl:otherwise>
                        <!-- dans otherwise, on ne traduit pas : pour généricité avec d'autres attributs -->
                    </xsl:choose>

                </xsl:attribute>

            </xsl:if>

            <xsl:apply-templates />

        </xsl:element>

    </xsl:template>

</xsl:stylesheet>
