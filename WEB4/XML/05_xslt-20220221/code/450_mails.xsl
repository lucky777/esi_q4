<?xml version="1.0"?>

<!-- xsltproc -v -o 450_mails_output_xsltproc.md 450_mails.xsl 450_mails.xml -->

<!-- java -jar saxon-he-10.0.jar -s:450_mails.xml -xsl:450_mails.xsl -o:450_mails_output_saxon.md -->

<!-- java -jar xalan.jar -IN 450_mails.xml -XSL 450_mails.xsl -OUT 450_mails_output__xalan.md -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="https://esi-bru.be/WEBR4/XML">

<!-- illustration de la fonction current()
https://developer.mozilla.org/en-US/docs/Web/XPath/Functions/current
https://www.w3schools.com/xml/func_current.asp -->

    <xsl:output method="text" />
    <xsl:strip-space elements="*" />

    <xsl:template match="text()" />

    <xsl:template match="/"># Mails

Dans l'ordre du document, on a :
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="in:message">
<xsl:variable name="content" select="in:content" />
<!-- <xsl:variable name="length" select="'40'" /> -->
<xsl:variable name="length" select="'400'" />
<!-- <xsl:variable name="length" select="'4000'" /> -->
1. **<xsl:value-of select="//in:user[@id=current()/in:sender]/in:name" />** (<xsl:value-of select="in:sender" />) a écrit : « <xsl:choose> <xsl:when test="string-length($content) &lt; $length"> <xsl:value-of select="$content" /></xsl:when><xsl:otherwise><xsl:value-of select="substring($content, 0, $length)" />...</xsl:otherwise></xsl:choose> »</xsl:template>

</xsl:stylesheet>