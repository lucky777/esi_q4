<?xml version="1.0" ?>

<!-- xsltproc -v -o 200_carnet_output_xsltproc.xml 200_carnet.xsl 200_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:200_carnet.xml -xsl:200_carnet.xsl -o:200_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 200_carnet.xml -XSL 200_carnet.xsl -OUT 200_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="carnet">
        <!-- aa -->
        <xsl:copy>
            <!-- bb -->
            <xsl:value-of select="." />
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
