<?xml version="1.0" ?>

<!-- xsltproc -v -o 220_carnet_output_xsltproc.xml 220_carnet.xsl 220_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:220_carnet.xml -xsl:220_carnet.xsl -o:220_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 220_carnet.xml -XSL 220_carnet.xsl -OUT 220_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="personne">
        <!-- abc -->
        <xsl:copy-of select="." />
        <!-- <xsl:copy-of select="adresse/rue" /> -->
    </xsl:template>

    <!-- ne pas oublier template par défaut : apply-template appelé pour racine et tout élément (voir 060_carnet_default_explicit.xsl) -->

</xsl:stylesheet>
