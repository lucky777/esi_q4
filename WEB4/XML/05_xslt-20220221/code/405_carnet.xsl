<?xml version="1.0" ?>

<!-- xsltproc -v -o 405_carnet_output_xsltproc.xml 405_carnet.xsl 405_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:405_carnet.xml -xsl:405_carnet.xsl -o:405_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 405_carnet.xml -XSL 405_carnet.xsl -OUT 405_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="https://esi-bru.be/WEBR4/XML">

    <xsl:variable name="esi_namespace">https://www.he2b.be/campus-esi/WEBR4/XML</xsl:variable>

    <xsl:variable name="brol_namespace">https://www.brol.be</xsl:variable>
    
    <!-- voir aussi xsl:namespace-alias -->

    <xsl:output method="xml" indent="yes" />

    <xsl:template match="text()" />
    <!-- ceci n'est pas le cas dans 300_carnet.xsl et justifie le paramètre print_value de la fonction create_elt -->

    <xsl:template match="carnet">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">notebook</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="in:personne">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">person</xsl:with-param>
            <xsl:with-param name="elt_attribute_name">title</xsl:with-param>
            <xsl:with-param name="elt_attribute_value"><xsl:value-of select="@titre" /></xsl:with-param>
            <xsl:with-param name="elt_namespace" select="$esi_namespace" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="nom">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">name</xsl:with-param>
            <xsl:with-param name="print_value" select="true()" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="prenom">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">firstname</xsl:with-param>
            <xsl:with-param name="print_value" select="true()" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="in:adresse">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">address</xsl:with-param>
            <xsl:with-param name="elt_namespace" select="$esi_namespace" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="rue">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">street</xsl:with-param>
            <xsl:with-param name="print_value" select="true()" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="numero">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">number</xsl:with-param>
            <xsl:with-param name="print_value" select="true()" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="codepostal">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">postalcode</xsl:with-param>
            <xsl:with-param name="print_value" select="true()" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="ville">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">town</xsl:with-param>
            <xsl:with-param name="print_value" select="true()" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="in:pays">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">country</xsl:with-param>
            <xsl:with-param name="print_value" select="true()" />
            <xsl:with-param name="elt_namespace" select="$brol_namespace" />
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="create_elt">

        <xsl:param name="elt_name" />

        <xsl:param name="elt_attribute_name" />

        <xsl:param name="elt_attribute_value" />

        <xsl:param name="print_value" select="false()" />
        <!-- notez l'utilisation de la fonction false() sans quoi il s'agit de la chaîne de caractères false -->

        <xsl:param name="elt_namespace" />

        <xsl:if test="string-length($elt_namespace) = 0">

            <xsl:element name="{$elt_name}">
                <!-- dans output : xmlns="" -->

                <xsl:if test="string-length($elt_attribute_name) &gt; 0">

                    <xsl:attribute name="{$elt_attribute_name}">

                        <xsl:choose>
                            <xsl:when test="$elt_attribute_value = 'Mme'">Mrs</xsl:when>
                            <xsl:when test="$elt_attribute_value = 'M.'">Mr</xsl:when>
                            <xsl:otherwise>uUu</xsl:otherwise>
                        </xsl:choose>

                    </xsl:attribute>

                </xsl:if>

                <xsl:if test="$print_value">

                    <xsl:value-of select="." />

                </xsl:if>

                <xsl:apply-templates/>

            </xsl:element>

        </xsl:if>

        <xsl:if test="string-length($elt_namespace) > 0">

            <xsl:element name="{$elt_name}" namespace="{$elt_namespace}">

                <xsl:if test="string-length($elt_attribute_name) &gt; 0">

                    <xsl:attribute name="{$elt_attribute_name}">

                        <xsl:choose>
                            <xsl:when test="$elt_attribute_value = 'Mme'">Mrs</xsl:when>
                            <xsl:when test="$elt_attribute_value = 'M.'">Mr</xsl:when>
                            <xsl:otherwise><xsl:value-of select="$elt_attribute_value" /></xsl:otherwise>
                        </xsl:choose>

                    </xsl:attribute>

                </xsl:if>

                <xsl:if test="$print_value">

                    <xsl:value-of select="." />

                </xsl:if>

                <xsl:apply-templates />

            </xsl:element>

        </xsl:if>

    </xsl:template>

</xsl:stylesheet>
