<?xml version="1.0" ?>

<!-- xsltproc -v -o 140_carnet_output_xsltproc.html 140_carnet.xsl 140_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:140_carnet.xml -xsl:140_carnet.xsl -o:140_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 140_carnet.xml -XSL 140_carnet.xsl -OUT 140_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <title>Carnet</title>
            </head>
            <body>

                <xsl:for-each select="//personne">
                    <h2>

                        <xsl:value-of select="@titre" />

                        <xsl:value-of select="prenom" />

                        <xsl:value-of select="nom" />
                    </h2>

                    <xsl:value-of select="adresse/rue" />,

                    <xsl:value-of select="adresse/numero" /><br/>

                    <xsl:value-of select="adresse/codepostal" />

                    <xsl:value-of select="adresse/ville" /><br/>

                    <xsl:value-of select="adresse/pays" />

                    <!-- dans les outputs ci-dessus : la virgule (,) est suivie par une espace et des passages à la ligne (qui sont conservés de l'input à l'output) mais il n'y a pas d'espace entre le code postal et la ville car le processeur xslt vire totalement de l'output les nœuds textes constitués uniquement d'espaces blanches (' ', \n, etc.) -->

                </xsl:for-each>

            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
