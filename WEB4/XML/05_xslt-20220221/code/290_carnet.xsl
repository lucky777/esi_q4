<?xml version="1.0" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- par rapport à 280 : retours à la ligne dans paramètres -->

<!-- xsltproc -v -o 290_carnet_output_xsltproc.xml 290_carnet.xsl 290_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:290_carnet.xml -xsl:290_carnet.xsl -o:290_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 290_carnet.xml -XSL 290_carnet.xsl -OUT 290_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<!-- ko pour xsltproc et xalan, même cas de figure que 270_carnet.xsl -->

    <xsl:output method="xml"/>

    <xsl:template match="carnet">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                notebook
            </xsl:with-param>
            <!-- xsltproc et xalan n'aime pas ce retour à la ligne, et les suivants dans les paramètres -->
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="personne">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                person
            </xsl:with-param>

            <xsl:with-param name="elt_attribute_name">
                title
            </xsl:with-param>

            <xsl:with-param name="elt_attribute_value">
                ooo</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="nom">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                name
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="prenom">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                firstname
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="adresse">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                address
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="rue">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                street
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="numero">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                number
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="codepostal">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                postalcode
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="ville">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                town
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="pays">

        <xsl:call-template name="create_elt">

            <xsl:with-param name="elt_name">
                country
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="create_elt">

        <xsl:param name="elt_name" />

        <xsl:param name="elt_attribute_name" />

        <xsl:param name="elt_attribute_value" />

        <xsl:element name="{$elt_name}">

            <xsl:if test="string-length($elt_attribute_name) > 0">

                <xsl:attribute name="{$elt_attribute_name}">
                    <xsl:value-of select="$elt_attribute_value" />
                </xsl:attribute>
            </xsl:if>

            <xsl:apply-templates />
        </xsl:element>

    </xsl:template>
</xsl:stylesheet>
