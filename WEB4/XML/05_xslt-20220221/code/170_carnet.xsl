<?xml version="1.0"?>

<!-- xsltproc -v -o 170_carnet_output_xsltproc.xml 170_carnet.xsl 170_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:170_carnet.xml -xsl:170_carnet.xsl -o:170_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 170_carnet.xml -XSL 170_carnet.xsl -OUT 170_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="carnet">
        <!-- copie de l'élément courant seul (pas sa descendance, ni ses attributs) -->
        <xsl:copy>
            aaa
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
