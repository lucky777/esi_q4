<!-- procedural example -->

<!-- xsltproc -v -o 010_carnet_procedural_output_xsltproc.xml 010_procedural.xsl 010_carnet_procedural.xml -->

<!-- java -jar saxon-he-10.0.jar -s:010_carnet_procedural.xml -xsl:010_procedural.xsl -o:010_carnet_procedural_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 010_carnet_procedural.xml -XSL 010_procedural.xsl -OUT 010_carnet_procedural_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:transform xmlns="https://esi-bru.be/WEBR4/XML" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="xml" indent="yes"/>

    <!-- root template: main entry point -->
    <xsl:template match="/">
        <booknote>

            <xsl:for-each select="carnet/personne">
                <person>

                    <xsl:call-template name="outputName"/>

                    <xsl:call-template name="outputTitle"/>
                </person>
            </xsl:for-each>
        </booknote>
    </xsl:template>

    <!-- outputs name element -->
    <xsl:template name="outputName">
        <name><xsl:value-of select="concat(prenom, ' ', nom)"/></name>
    </xsl:template>

    <!-- outputs title element -->
    <xsl:template name="outputTitle">
        <title><xsl:value-of select="@titre"/></title>
    </xsl:template>

</xsl:transform>
