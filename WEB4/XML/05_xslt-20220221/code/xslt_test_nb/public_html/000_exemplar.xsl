<!-- exemplar document -->

<!-- xsltproc -v -o 000_carnet_exemplar_output_xsltproc.xml 000_exemplar.xsl 000_carnet_exemplar.xml -->

<!-- java -jar saxon-he-10.0.jar -s:000_carnet_exemplar.xml -xsl:000_exemplar.xsl -o:000_carnet_exemplar_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 000_carnet_exemplar.xml -XSL 000_exemplar.xsl -OUT 000_carnet_exemplar_output_xalan.xml -TT -TG -TS -TTC -->

<booknote xmlns="https://esi-bru.be/WEBR4/XML" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xsl:version="1.0">

    <!-- <xsl:output method="xml" indent="yes"/> -->
    <!-- element output only allowed as child of stylesheet -->

    <person>
        <title><xsl:value-of select="/carnet/personne/@titre"/></title>
        <name><xsl:value-of select="concat(/carnet/personne/prenom,
' ', /carnet/personne/nom)"/></name>
    </person>

    <person>
        <title><xsl:value-of select="/carnet/personne[2]/@titre"/></title>
        <name><xsl:value-of select="concat(/carnet/personne[2]/prenom,
' ', /carnet/personne[2]/nom)"/></name>
    </person>

    <person>
        <title><xsl:value-of select="/carnet/personne/@titre"/></title>
        <name><xsl:value-of select="concat(/carnet/personne/prenom,
' ', /carnet/personne/nom)"/></name>
    </person>
    <!-- 3.4.20 string string string(object?) Description string converts its argument into a string. The conversion details depend on the type of argument object. If the argument is omitted, it defaults to a node-set with the context node as its only
    member. Type Description node-set The string-value of the node in the node-set that is first in document order. If the node-set is empty, an empty string is returned. ... -->

</booknote>
