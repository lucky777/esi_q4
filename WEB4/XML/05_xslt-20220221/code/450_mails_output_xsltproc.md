# Mails

Dans l'ordre du document, on a :
        
1. **Charlie** (u3) a écrit : « De toutes les horreurs qui menacent la Terre et surtout ses habitants, la plus dévastatrice – mais aussi celle qui a la plus faible probabilité de survenir – vient de l’espace : la collision avec un astéroïde de belle taille nous ferait vivre le cataclysme qui fut fatal aux dinosaures (sauf aux ancêtres des oiseaux) il y a 66 millions d’années. »
1. **Julia** (u1) a écrit : « (void) »
1. **Charlie** (u3) a écrit : « Il y a 140 000 ans, la Terre est sortie d’une ère glaciaire. La fonte de la calotte du Groenland a alors injecté des masses d’eau douce dans l’Atlantique Nord, ralentissant la circulation du Gulf Stream. Une équipe internationale dirigée par Jasper Wassenburg, de l’Institut Max-Planck de chimie (Mayence, Allemagne), vient d’en préciser les conséquences sur la mousson indienne, en s’appuyant notam... »
1. **Noûr** (u2) a écrit : « C’est l’histoire d’une femme qui, après avoir été infectée par le virus du sida, serait la seconde personne au monde à avoir complètement et naturellement éliminé toute trace du VIH de son organisme. Le système immunitaire de cette patiente a-t-il réussi à développer une immunité stérilisante, capable de totalement et définitivement éradiquer le VIH ? Il semble bien que cela soit le cas. C’est en... »