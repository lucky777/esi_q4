<?xml version="1.0" ?>

<!-- xsltproc -v -o 120_carnet_output_xsltproc.html 120_carnet.xsl 120_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:120_carnet.xml -xsl:120_carnet.xsl -o:120_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 120_carnet.xml -XSL 120_carnet.xsl -OUT 120_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"/>
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8"/>
                <title>Carnet</title>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="personne">
        <h2>

            <xsl:value-of select="@titre"/>

            <xsl:value-of select="prenom"/>

            <xsl:value-of select="nom"/>
        </h2>

        <!-- <xsl:apply-templates/> -->
        <xsl:apply-templates select="adresse"/>
        <!-- <xsl:apply-templates select="inexistant"/> -->

    </xsl:template>

</xsl:stylesheet>
