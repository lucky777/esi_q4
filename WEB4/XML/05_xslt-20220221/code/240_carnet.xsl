<?xml version="1.0" ?>

<!-- xsltproc -v -o 240_carnet_output_xsltproc.xml 240_carnet.xsl 240_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:240_carnet.xml -xsl:240_carnet.xsl -o:240_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 240_carnet.xml -XSL 240_carnet.xsl -OUT 240_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- <xsl:output method="xml" indent="yes" /> -->
    <xsl:output method="xml" />

    <!-- variable globale car déclarée directement sous la racine
         stylesheet (hors template) -->
    <xsl:variable name="rootname">كتاب</xsl:variable>

    <!-- xslt : les variables sont constantes : impossible de changer leur valeur après leur initialisation -->

    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">

        <!-- <xsl:element name="$rootname"> -->
        <!-- ko : xsl:element: The value '$rootname' of the attribute 'name' is not a valid QName. -->
        <xsl:element name="{$rootname}">
        <!-- ok : attribute value template -->
        <!-- raccourci pour <xsl:value-of select="$rootname" /> -->
        <!-- peut être utilisé pour certains attributs de certains éléments xslt -->

            1) $rootname 2) {$rootname}

            <xsl:text>
                3) $rootname
            </xsl:text>

            <xsl:text>
                4) {$rootname}
            </xsl:text>

            <!-- xsl:value-of pour accéder au contenu d'une variable -->
            5) <xsl:value-of select="$rootname" />

            <!-- <xsl:value-of select="{$rootname}" /> -->
            <!-- compilation error: file 240_carnet.xsl line 41 element value-of -->
            <!-- xsl:value-of : could not compile select expression '{$rootname}' -->

            6) <test>
                <!-- variable locale -->
                <xsl:variable name="brol">c'est la fête</xsl:variable>
                <xsl:attribute name="attr">
                    <xsl:value-of select="$brol" />
                </xsl:attribute>
                bla
            </test>

            <!-- variable locale -->
            <!-- ko car select : expression xpath ou immédiat entre " ou ' -->
            <!-- <xsl:variable name="var_ko1" select="c&apos;est bon (ko1)"/> -->
            <!-- <xsl:variable name="var_ko2" select="'c&apos;est bon (ko2)'"/> -->
            <xsl:variable name="var_ok" select='"c&apos;est bon (ok)"'/>

            <!-- équivalent avec attribute value template -->
            <!-- erreur car brol n'existe plus ici -->
            <!-- <test attr="{$brol}"> -->
            <!-- <test attr="{$var_ko1}"> -->
            7) <test attr="{$var_ok}">
                boum
            </test>

            8) <brol>
                <xsl:attribute name="attr">
                    <xsl:value-of select="carnet/personne[last()]/adresse/numero" />
                </xsl:attribute>
            </brol>

            <!-- équivalent avec attribute value template -->
            9) <brol attr="{carnet/personne[last()-1]/adresse/numero}" />

            <!-- nom d'élément dynamique : en fonction de données du document en input -->
            10) <xsl:element name="{carnet/personne[last()-1]/adresse/ville}">
                aaa
            </xsl:element>

            <!-- variables utilisées dans expressions XPath ($var) ou pour certains attributs d'éléments xsl entre curly-braces {$var} -->

            11) concat($rootname, ' ', 'abc') : 12) <xsl:value-of select="concat($rootname, ' ', 'abc')"/>

            13) concat('abc', ' ', $rootname) : 14) <xsl:value-of select="concat('abc', ' ', $rootname)"/>

            15) select="carnet/personne/nom" : 16) <xsl:value-of select="carnet/personne/nom"/>
            <!-- conversion d'un node set en string => conversion du 1er élément du node set en string : ici il y a 3 personnes, c'est le nom de la 1re qui est converti en string -->

            17) select="carnet/personne/nom[2]" : 18) <xsl:value-of select="carnet/personne/nom[2]"/>

            19) select="carnet/personne[2]/nom" : 20) <xsl:value-of select="carnet/personne[2]/nom"/>

            21) select="//nom" : 22) <xsl:value-of select="//nom"/>
            <!-- conversion node-set en string : le premier node dans l'ordre du document est converti -->
            <!-- conversion node en string : concaténation des text nodes de sa descendance -->

           23)  select="//nom[2]" : 24) <xsl:value-of select="//nom[2]"/>
            <!-- chaque personne a 1! nom... on n'a pas un node set constitué de 3 noms... je ne comprends pas tout à fait... -->

            25) select="//personne[2]/nom" : 26) <xsl:value-of select="//personne[2]/nom"/>

            27) concat($rootname, ' ', //personne[2]//numero) : 28) <xsl:value-of select="concat($rootname, ' ', //personne[2]//numero)"/>

            29) concat(//personne[2]//numero, ' ', $rootname) : 30) <xsl:value-of select="concat(//personne[2]//numero, ' ', $rootname)"/>

            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>

    <xsl:template match="personne">
        <xsl:copy-of select="." />
    </xsl:template>

</xsl:stylesheet>
