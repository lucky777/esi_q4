<?xml version="1.0" ?>

<!-- xsltproc -v -o 280_carnet_output_xsltproc.xml 280_carnet.xsl 280_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:280_carnet.xml -xsl:280_carnet.xsl -o:280_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 280_carnet.xml -XSL 280_carnet.xsl -OUT 280_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />

    <!-- <xsl:template match="text()" /> -->
    <!-- (dé)commenter ci-dessus -->

    <xsl:template match="carnet">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">notebook</xsl:with-param>
            <!-- <xsl:apply-templates /> -->
            <!-- interdit ici : uniquement with-param au sein de call-template -->
        </xsl:call-template>

        <!-- <xsl:apply-templates /> -->
        <!-- mauvaise idée ici car l'élément notebook est créé complètement dans create_elt donc ici il est fermé... -->
        <!-- abc -->
    </xsl:template>

    <xsl:template match="personne">

        <!-- <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">person</xsl:with-param>
            <xsl:with-param name="elt_attribute_name">title</xsl:with-param>
            <xsl:choose>
                <xsl:when test="@titre = 'Mme'">
                    <xsl:with-param name="elt_attribute_value">Mrs</xsl:with-param>
                </xsl:when>
                <xsl:when test="@titre = 'M.'">
                    <xsl:with-param name="elt_attribute_value">Mr</xsl:with-param>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:with-param name="elt_attribute_value">oOo</xsl:with-param>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:call-template> -->
        <!-- ceci ne fonctionne pas car la contenu de call-template est limité à with-param -->

        <xsl:choose>
            <xsl:when test="@titre = 'Mme'">
                <xsl:call-template name="create_elt">
                    <xsl:with-param name="elt_name">person</xsl:with-param>
                    <xsl:with-param name="elt_attribute_name">title</xsl:with-param>
                    <xsl:with-param name="elt_attribute_value">Mrs</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="@titre = 'M.'">
                <xsl:call-template name="create_elt">
                    <xsl:with-param name="elt_name">person</xsl:with-param>
                    <xsl:with-param name="elt_attribute_name">title</xsl:with-param>
                    <xsl:with-param name="elt_attribute_value">Mr</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="create_elt">
                    <xsl:with-param name="elt_name">person</xsl:with-param>
                    <xsl:with-param name="elt_attribute_name">title</xsl:with-param>
                    <xsl:with-param name="elt_attribute_value">oOo</xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template match="nom">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">name</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="prenom">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">firstname</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="adresse">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">address</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="rue">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">street</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="numero">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">number</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="codepostal">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">postalcode</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="ville">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">town</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <xsl:template match="pays">

        <xsl:call-template name="create_elt">
            <xsl:with-param name="elt_name">country</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <!-- ci-dessous définition du template qui joue le rôle de fonction -->

    <xsl:template name="create_elt">

        <!-- ci-dessous : paramètres du template -->

        <xsl:param name="elt_name" />

        <xsl:param name="elt_attribute_name" />
        <!-- on peut donner une valeur par défaut à un param avec select="abc", par ailleurs la valeur par défaut par défaut est select="" -->

        <xsl:param name="elt_attribute_value" />

        <!-- ci-dessous : équivalent du coprs de fonction -->

        <xsl:element name="{$elt_name}">

            <xsl:if test="string-length($elt_attribute_name) > 0">
                <xsl:attribute name="{$elt_attribute_name}">
                    <xsl:value-of select="$elt_attribute_value" />
                </xsl:attribute>
            </xsl:if>

            <xsl:apply-templates />
            <!-- c'est bien ici qu'il faut mettre en œuvre la récursion sur les nœuds enfants -->
        </xsl:element>

    </xsl:template>

</xsl:stylesheet>
