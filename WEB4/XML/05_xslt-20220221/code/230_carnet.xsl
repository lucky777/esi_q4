<?xml version="1.0" ?>

<!-- xsltproc -v -o 230_carnet_output_xsltproc.xml 230_carnet.xsl 230_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:230_carnet.xml -xsl:230_carnet.xsl -o:230_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 230_carnet.xml -XSL 230_carnet.xsl -OUT 230_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <notebook>
            <!-- a -->
            <xsl:apply-templates />
            <!-- x -->
        </notebook>
    </xsl:template>

    <xsl:template match="personne">
        <!-- p -->
        <xsl:copy-of select="." />
        <!-- <xsl:copy-of select="adresse/rue" /> -->
        <!-- q -->
        <!-- <xsl:apply-templates /> -->
    </xsl:template>

</xsl:stylesheet>
