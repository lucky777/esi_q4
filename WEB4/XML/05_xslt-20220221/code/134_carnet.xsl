<?xml version="1.0" ?>

<!-- xsltproc -v -o 134_carnet_output_xsltproc.html 134_carnet.xsl 134_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:134_carnet.xml -xsl:134_carnet.xsl -o:134_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 134_carnet.xml -XSL 134_carnet.xsl -OUT 134_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" />
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <title>Carnet</title>
            </head>
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="personne">
        <h2>

            <xsl:value-of select="@titre" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="prenom" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="nom" />

            <!-- <xsl:text><xsl:value-of select="@titre" /> <xsl:value-of select="prenom" /> <xsl:value-of select="nom" /></xsl:text> -->
            <!-- ci-dessus KO car contenu de text doit être #PCDATA : https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/text-->
        </h2>

        <!-- <xsl:apply-templates select="adresse" /> -->

        <xsl:apply-templates />
        <!-- avec ceci : nom & prénom affichés une 2e fois après le titre car les nœuds texte sont par défaut copié dans le fichier en sortie -->

    </xsl:template>

    <xsl:template match="adresse">
        <xsl:value-of select="rue" />, <xsl:value-of select="numero" /><br />
        <xsl:value-of select="codepostal" /><xsl:text> </xsl:text><xsl:value-of select="ville" /><br />
        <xsl:value-of select="pays" />

        <!-- <xsl:apply-templates /> -->
        <!-- avec ceci : les adresses sont affichées 2 x -->
    </xsl:template>

</xsl:stylesheet>
