<?xml version="1.0" ?>

<!-- xsltproc -v -o 090_carnet_output_xsltproc.html 090_carnet.xsl 090_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:090_carnet.xml -xsl:090_carnet.xsl -o:090_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 090_carnet.xml -XSL 090_carnet.xsl -OUT 090_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"/>
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <!-- <!DOCTYPE html> -->
        <!-- A DOCTYPE is not allowed in content. -->
        <html>
            <head>
                <meta charset="UTF-8"/>
                <title>Carnet</title>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
