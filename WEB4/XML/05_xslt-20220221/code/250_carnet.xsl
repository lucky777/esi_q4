<?xml version="1.0" ?>

<!-- xsltproc -v -o 250_carnet_output_xsltproc.xml 250_carnet.xsl 250_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:250_carnet.xml -xsl:250_carnet.xsl -o:250_carnet_output_saxon.xml -->

<!-- java -jar xalan.jar -IN 250_carnet.xml -XSL 250_carnet.xsl -OUT 250_carnet_output_xalan.xml -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml"/>

    <xsl:variable name="rootname" select="'كتاب'" />
    <!-- possiblité de contenu dynamique avec select : expression XPath : voir variable $dynamic ci-dessous -->
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">

        <xsl:element name="{$rootname}">

            1) $rootname : 2) <xsl:value-of select="$rootname" />

            <xsl:variable name="dynamic" select="concat('A', //personne[2]//numero)" />

            3) $dynamic : 4) <xsl:value-of select="$dynamic" />

            <abc>
                <xsl:attribute name="{$dynamic}">22</xsl:attribute>
                abc
            </abc>

            <!-- noter les passages à la ligne dans les valeurs de l'attribut ci-dessous -->
            <def>
                <xsl:attribute name="{$dynamic}">
                    44
                </xsl:attribute>
                def
            </def>

            <ghi>
                <xsl:attribute name="{$dynamic}"><xsl:value-of select="//personne[2]//numero" /></xsl:attribute>
                ghi
            </ghi>

            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>

    <xsl:template match="personne">
        <xsl:copy-of select="." />
        <xsl:apply-templates />
    </xsl:template>

</xsl:stylesheet>
