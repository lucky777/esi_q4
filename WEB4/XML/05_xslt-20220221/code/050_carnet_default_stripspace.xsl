<?xml version="1.0"?>

<!-- xsltproc -v -o 050_carnet_default_stripspace_output_xsltproc.txt 050_carnet_default_stripspace.xsl 050_carnet_default_stripspace.xml -->

<!-- java -jar saxon-he-10.0.jar -s:050_carnet_default_stripspace.xml -xsl:050_carnet_default_stripspace.xsl -o:050_carnet_default_stripspace_output_saxon.txt -->

<!-- java -jar xalan.jar -IN 050_carnet_default_stripspace.xml -XSL 050_carnet_default_stripspace.xsl -OUT 050_carnet_default_stripspace_output_xalan.txt -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="text" />

    <!-- pour retirer du document d'origine les noeuds textuels constitués uniquement d'espaces blanches -->
    <xsl:strip-space elements="*" />
    <!-- rien -->
</xsl:stylesheet>
