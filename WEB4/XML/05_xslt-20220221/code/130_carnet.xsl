<?xml version="1.0" ?>

<!-- xsltproc -v -o 130_carnet_output_xsltproc.html 130_carnet.xsl 130_carnet.xml -->

<!-- java -jar saxon-he-10.0.jar -s:130_carnet.xml -xsl:130_carnet.xsl -o:130_carnet_output_saxon.html -->

<!-- java -jar xalan.jar -IN 130_carnet.xml -XSL 130_carnet.xsl -OUT 130_carnet_output_xalan.html -TT -TG -TS -TTC -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"/>
    <!-- <xsl:strip-space elements="*" /> -->

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <title>Carnet</title>
            </head>
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="personne">
        <h2>

            <xsl:value-of select="@titre" />

            <xsl:value-of select="prenom" />

            <xsl:value-of select="nom" />
        </h2>

        <xsl:apply-templates select="adresse" />

    </xsl:template>

    <xsl:template match="adresse">
        <xsl:value-of select="rue" />, <xsl:value-of select="numero" /><br />
        <xsl:value-of select="codepostal" /> <xsl:value-of select="ville" /><br />
        <xsl:value-of select="pays" />

        <!-- dans les outputs ci-dessus : la virgule (,) est suivie par une espace mais il n'y a pas d'espace entre le code postal et la ville car le processeur xslt vire totalement de l'output les nœuds textes constitués uniquement d'espaces blanches -->

    </xsl:template>

</xsl:stylesheet>
