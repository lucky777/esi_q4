# XML Schema : contraintes : unique, key et keyref

XPath est utilisé pour localiser dans le document XML les éléments ou attributs contraints.

## unique

Il y a trois informations à donner :

+   l'élément de fichier XML au sein duquel un autre élément (de sa descendance) doit être unique : c'est dans la définition de _cet élément_ dans le schéma XSD que l'élément `<unique>` exprimant la contrainte d'unicité est placée ;
+   l'élément du fichier XML qui doit être unique : il est renseigné dans le fichier XSD via l'attribut `xpath` de l'élément `<selector>` enfant de l'élément `<unique>`, l'expression XPath est relative à l'élément où `<unique>` est placé ;
+   l'élément ou l'attribut (ou leurs combinaisons) qui contrôle l'unicité, c'est-à-dire dont la valeur doit effectivement être unique dans le contexte désiré : il est renseigné dans le fichier XSD via l'attribut `xpath` de l'élément `<field>` enfant de l'élément `<unique>`, l'expression XPath est relative à l'élément désigné par l'expression `xpath` de l'élément `<selector>` enfant de l'élément `<unique>`.

### Documentation

Élément :

-   `<unique>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_unique>
-   `<selector>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_selector>
-   `<field>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_field>

### Élément unique dans tout le fichier XML

#### XML

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<samples xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML
                    00_samples_data_unique_within_samples.xsd">
    <sample>
        <datas>
            <data>11</data>
            <data>22</data>
            <data>55</data>
            <data>77</data>
            <data>88</data>
        </datas>
    </sample>
    <sample>
        <datas>
            <data>33</data>
        </datas>
    </sample>
    <sample>
        <datas>
            <data>44</data>
            <data>66</data>
            <data>99</data>
        </datas>
    </sample>
</samples>
```

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_samples_data_unique_within_samples.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="DataType">
        <xsd:restriction base="xsd:integer"/>
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="data" type="esi:DataType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SampleType">
        <xsd:sequence >
            <xsd:element name="datas" type="esi:DatasType"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SamplesType">
        <xsd:sequence>
            <xsd:element name="sample" type="esi:SampleType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="samples" type="esi:SamplesType">
        <xsd:unique name="unique_data_within_samples">
            <xsd:selector xpath="esi:sample/esi:datas/esi:data"/>
            <xsd:field xpath="."/>
        </xsd:unique>
    </xsd:element>

</xsd:schema>
```

On a ici :

+   la contrainte `<unique>` est placé dans la définition de l'élément racine du fichier XML, l'élément qui doit être unique doit donc l'être dans tout le document ;
+   l'expression XPath de l'élément `<selector>` renseigne, depuis la racine `<samples>` du document XML, les éléments `<data>` : ce sont donc ces derniers éléments qui doivent être uniques dans le document XML ;
+   l'expression XPath de l'élément `<field>` renseigne, depuis chacun des éléments `<data>` (voir `<selector>` ci-dessus), l'élément `<data>` lui-même : c'est donc sa valeur qui contrôle l'unicité.

Notez bien que les tests de nœuds dans les expressions XPath utilisent les noms complets des éléments : préfixe d'espace de noms et nom local.

#### Validation

```shell
xmllint --noout --schema 00_samples_data_unique_within_samples.xsd
00_samples_data_unique_within_samples.xml
```

### Élément unique au sein d'un élément différent de la racine

#### XML

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<samples xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML
                    01_samples_data_unique_within_sample.xsd">
    <sample>
        <datas>
            <data>11</data>
            <data>22</data>
            <data>55</data>
            <data>77</data>
            <data>88</data>
        </datas>
    </sample>
    <sample>
        <datas>
            <data>11</data>
        </datas>
    </sample>
    <sample>
        <datas>
            <data>11</data>
            <data>22</data>
            <data>77</data>
        </datas>
    </sample>
</samples>
```

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 01_samples_data_unique_within_sample.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="DataType">
        <xsd:restriction base="xsd:integer"/>
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="data" type="esi:DataType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SampleType">
        <xsd:sequence >
            <xsd:element name="datas" type="esi:DatasType"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SamplesType">
        <xsd:sequence>
            <xsd:element name="sample" type="esi:SampleType"
                         maxOccurs="unbounded">
                <xsd:unique name="unique_data_within_sample">
                    <xsd:selector xpath="esi:datas/esi:data"/>
                    <xsd:field xpath="."/>
                </xsd:unique>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="samples" type="esi:SamplesType"/>

</xsd:schema>
```

On a ici :

+   la contrainte `<unique>` est placé dans la définition de l'élément `<sample>` du fichier XML, l'élément qui doit être unique doit donc l'être au sein de chacun des éléments `<sample>` du document ;
+   l'expression XPath de l'élément `<selector>` renseigne, depuis chaque élément `<sample>` du document XML, les éléments `<data>` : ce sont donc ces derniers éléments qui doivent être uniques dans les éléments `<sample>` ;
+   l'expression XPath de l'élément `<field>` renseigne, depuis chacun des éléments `<data>` (voir `<selector>` ci-dessus), l'élément `<data>` lui-même : c'est donc sa valeur qui contrôle l'unicité.

Notez bien que les tests de nœuds dans les expressions XPath utilisent les noms complets des éléments : préfixe d'espace de noms et nom local.

#### Validation

```shell
xmllint --noout --schema 01_samples_data_unique_within_sample.xsd
01_samples_data_unique_within_sample.xml
```

### Élément unique au sein d'un élément différent de la racine : exemple avec erreur

#### XML

Idem que précédemment (`01_samples_data_unique_within_sample.xml`).

```xml
<?xml version="1.0" encoding="UTF-8"?>
<samples xmlns="https://esi-bru.be/WEBR4/XML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="https://esi-bru.be/WEBR4/XML
                             02_samples_data_unique_within_sample_ko.xsd">
    <sample>
        <datas>
            <data>11</data>
            <data>22</data>
            <data>55</data>
            <data>77</data>
            <data>88</data>
        </datas>
    </sample>
    <sample>
        <datas>
            <data>11</data>
        </datas>
    </sample>
    <sample>
        <datas>
            <data>11</data>
            <data>22</data>
            <data>77</data>
        </datas>
    </sample>
</samples>
```

#### XSD

Variation des éléments `<selector>` et `<field>`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 02_samples_data_unique_within_sample_ko.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="DataType">
        <xsd:restriction base="xsd:integer" />
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="data" type="esi:DataType" maxOccurs="unbounded" />
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SampleType">
        <xsd:sequence>
            <xsd:element name="datas" type="esi:DatasType" />
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SamplesType">
        <xsd:sequence>
            <xsd:element name="sample" type="esi:SampleType" maxOccurs="unbounded">
                <xsd:unique name="unique_data_within_sample">
                    <xsd:selector xpath="esi:datas" />
                    <xsd:field xpath="esi:data" />
                    <!-- ko : Field "./esi:data" of identity constraint
                              "unique_data_within_sample" matches more than
                              one value within the scope of its selector;
                              fields must match unique values. -->
                </xsd:unique>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="samples" type="esi:SamplesType" />

</xsd:schema>
```

#### Validation

```shell
xmllint --noout --schema 02_samples_data_unique_within_sample_ko.xsd
02_samples_data_unique_within_sample_ko.xml

Element '{https://esi-bru.be/WEBR4/XML}data': The XPath 'esi:data' of a field of unique identity-constraint '{https://esi-bru.be/WEBR4/XML}unique_data_within_sample' evaluates to a node-set with more than one member.
Element '{https://esi-bru.be/WEBR4/XML}data': The XPath 'esi:data' of a field of unique identity-constraint '{https://esi-bru.be/WEBR4/XML}unique_data_within_sample' evaluates to a node-set with more than one member.
Element '{https://esi-bru.be/WEBR4/XML}data': The XPath 'esi:data' of a field of unique identity-constraint '{https://esi-bru.be/WEBR4/XML}unique_data_within_sample' evaluates to a node-set with more than one member.
Element '{https://esi-bru.be/WEBR4/XML}data': The XPath 'esi:data' of a field of unique identity-constraint '{https://esi-bru.be/WEBR4/XML}unique_data_within_sample' evaluates to a node-set with more than one member.
Element '{https://esi-bru.be/WEBR4/XML}data': The XPath 'esi:data' of a field of unique identity-constraint '{https://esi-bru.be/WEBR4/XML}unique_data_within_sample' evaluates to a node-set with more than one member.
Element '{https://esi-bru.be/WEBR4/XML}data': The XPath 'esi:data' of a field of unique identity-constraint '{https://esi-bru.be/WEBR4/XML}unique_data_within_sample' evaluates to a node-set with more than one member.
02_samples_data_unique_within_sample_ko.xml fails to validate
```

La validation échoue car l'expression XPath indiquée dans `<field>` retourne un ensemble de valeurs, or le contrôle de l'unicité ne peut se faire qu'à partir d'une valeur unique (ensemble d'un seul élément, singleton).

### Élément unique au sein d'un élément différent de la racine : exemple alternatif

#### XML

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<samples xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML
                    03_samples_data_unique_within_sample.xsd">
    <sample>
        <datas>
            <data>11</data>
            <data>22</data>
            <data>55</data>
            <data>77</data>
            <data>88</data>
        </datas>
    </sample>
    <sample>
        <datas>
            <data>11</data>
        </datas>
    </sample>
    <sample>
        <datas>
            <data>11</data>
            <data>22</data>
            <data>77</data>
        </datas>
    </sample>
</samples>
```

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 03_samples_data_unique_within_datas.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="DataType">
        <xsd:restriction base="xsd:integer"/>
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="data" type="esi:DataType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SampleType">
        <xsd:sequence >
            <xsd:element name="datas" type="esi:DatasType">
                <xsd:unique name="unique_data_within_datas">
                    <xsd:selector xpath="esi:data"/>
                    <xsd:field xpath="."/>
                </xsd:unique>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SamplesType">
        <xsd:sequence>
            <xsd:element name="sample" type="esi:SampleType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="samples" type="esi:SamplesType"/>

</xsd:schema>
```

On a ici :

+   la contrainte `<unique>` est placé dans la définition de l'élément `<datas>` du fichier XML, l'élément qui doit être unique doit donc l'être au sein de chacun des éléments `<datas>` du document ;
+   l'expression XPath de l'élément `<selector>` renseigne, depuis chaque élément `<datas>` du document XML, les éléments `<data>` : ce sont donc ces derniers éléments qui doivent être uniques dans les éléments `<datas>` ;
+   l'expression XPath de l'élément `<field>` renseigne, depuis chacun des éléments `<data>` (voir `<selector>` ci-dessus), l'élément `<data>` lui-même : c'est donc sa valeur qui contrôle l'unicité.

Notez bien que les tests de nœuds dans les expressions XPath utilisent les noms complets des éléments : préfixe d'espace de noms et nom local.

_Remarque_ : Comme il n'y a qu'un seul enfant `<datas>` par `<sample>`, la contrainte d'unicité au sein de `<datas>` revient effectivement à la même contrainte que celle d'unicité au sein de `<sample>`.

#### Validation

```shell
xmllint --noout --schema 03_samples_data_unique_within_datas.xsd
03_samples_data_unique_within_datas.xml
```

### Élément unique au sein d'un élément différent de la racine : nouvel exemple alternatif

#### XML

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 05_samples_alt.xml -->
<samples xmlns="https://esi-bru.be/WEBR4/XML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 05_samples_alt.xsd">
    <sample>
        <dataset>
            <control>11</control>
            <data>11</data>
            <data>33</data>
            <data>55</data>
            <data>66</data>
            <data>88</data>
        </dataset>
        <dataset>
            <control>22</control>
            <data>11</data>
            <data>44</data>
            <data>55</data>
        </dataset>
        <dataset>
            <control>55</control>
            <data>00</data>
            <data>22</data>
            <data>55</data>
            <data>11</data>
            <data>11</data>
        </dataset>
    </sample>
    <sample>
        <dataset>
            <control>11</control>
            <data>33</data>
        </dataset>
        <dataset>
            <control>88</control>
            <data>33</data>
            <data>11</data>
        </dataset>
    </sample>
    <sample>
        <dataset>
            <!-- <control>88</control> -->
            <data>33</data>
        </dataset>
        <dataset>
            <data>33</data>
            <data>11</data>
            <data>33</data>
        </dataset>
        <dataset>
            <control>88</control>
            <data>33</data>
        </dataset>
    </sample>
</samples>
```

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 05_samples_alt.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:esi="https://esi-bru.be/WEBR4/XML" elementFormDefault="qualified">

    <xsd:simpleType name="ControlType">
        <xsd:restriction base="xsd:integer"/>
    </xsd:simpleType>

    <xsd:simpleType name="DataType">
        <xsd:restriction base="xsd:integer"/>
    </xsd:simpleType>

    <xsd:complexType name="DataSetType">
        <xsd:sequence>
            <xsd:element name="control" type="esi:ControlType" minOccurs="0"/>
            <xsd:element name="data" type="esi:DataType" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SampleType">
        <xsd:sequence >
            <xsd:element name="dataset" type="esi:DataSetType" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- on désire : <dataset> unique au sein de <sample> avec
                     <control> comme valeur de contrôle de l'unicité -->
    <xsd:complexType name="SamplesType">
        <xsd:sequence>
            <xsd:element name="sample" type="esi:SampleType" maxOccurs="unbounded">
                <xsd:unique name="unique_dataset_within_sample">
                    <xsd:selector xpath="esi:dataset"/>
                    <xsd:field xpath="esi:control"/>
                </xsd:unique>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="samples" type="esi:SamplesType"/>

</xsd:schema>
```

On a ici :

+   la contrainte `<unique>` est placé dans la définition de l'élément `<sample>` du fichier XML, l'élément qui doit être unique doit donc l'être au sein de chacun des éléments `<sample>` du document ;
+   l'expression XPath de l'élément `<selector>` renseigne, depuis chaque élément `<sample>` du document XML, les éléments `<dataset>` : ce sont donc les éléments `<dataset>` qui doivent être uniques dans les éléments `<sample>` ;
+   l'expression XPath de l'élément `<field>` renseigne, depuis chacun des éléments `<dataset>` (voir `<selector>` ci-dessus), son élément enfant `<control>` : c'est donc la valeur de l'élément `<control>` qui contrôle l'unicité d'un élément `<dataset>` au sein d'un `<sample>` ;
+   la présence d'un  `<control>` dans un `<sample>` n'est pas obligatoire ; cela n'empêche pas d'imposer une contrainte d'unicité sur les `<dataset>` via l'élément `<control>`.

Notez bien que les tests de nœuds dans les expressions XPath utilisent les noms complets des éléments : préfixe d'espace de noms et nom local.

#### Validation

```shell
xmllint --noout --schema 05_samples_alt.xsd 05_samples_alt.xml
```

## key et keyref

Une différence entre les contraintes `<unique>` et `<key>` est que l'élément ou l'attribut renseigné par le `<field>` peut ne pas exister dans le document XML pour `<unique>`, mais doit être présent dans le document XML avec `<key>` : <https://stackoverflow.com/questions/1253609/difference-between-xsdkey-and-xsdunique>.

Une autre différence est que la contrainte `<key>` peut être référencée par un élément `<keyref>`. Dans le langage des SGBD, l'élément `<key>` sert à définir une clé primaire (unique et non nulle) tandis que l'élément `<keyref>` sert à définir une clé étrangère.

### Documentation

Élément :

-   `<key>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_key>
-   `<keyref>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_keyref>

### Exemple où tout va bien

#### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_messages.xml -->
<messages xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 10_messages.xsd">
    <message id="m1"/>
    <message id="m2" replyTo="m1"/>
    <message id="m3" replyTo="m1"/>
    <message id="m4" replyTo="m3"/>
    <message id="m5" replyTo="m5"/>
    <message id="m6" replyTo="m7"/>
    <message id="m7" replyTo="m1"/>
</messages>
```

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_messages.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="IdType">
        <xsd:restriction base="xsd:string">
            <xsd:pattern value="m[0-9]+"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="MessageType">
        <xsd:attribute name="id" type="esi:IdType"
                       use="required"/>
        <xsd:attribute name="replyTo" type="esi:IdType"/>
    </xsd:complexType>

    <xsd:complexType name="MessagesType">
        <xsd:sequence>
            <xsd:element name="message" type="esi:MessageType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="messages" type="esi:MessagesType">

        <!-- on désire unicité dans tout le document -->
        <xsd:key name="idMessage">
            <!-- message doit être unique -->
            <xsd:selector xpath="esi:message"/>
            <!-- id utilisé pour tester l'unicité -->
            <xsd:field xpath="@id"/>
        </xsd:key>

        <!-- keyref au même niveau que key -->
        <xsd:keyref name="refIdMessage" refer="esi:idMessage">
            <xsd:selector xpath="esi:message"/>
            <xsd:field xpath="@replyTo"/>
        </xsd:keyref>
    </xsd:element>

</xsd:schema>
```

On a ici :

+   la contrainte `<key>` est placé dans la définition de l'élément racine `<messages>` du fichier XML, l'élément qui doit être unique doit donc l'être dans tout le document :
    +   l'expression XPath de son enfant `<selector>` renseigne, depuis la racine `<messages>` du document XML, les éléments `<message>` : ce sont donc ces derniers éléments qui doivent être uniques dans le document XML ;
    +   l'expression XPath de son enfant `<field>` renseigne, depuis chacun des éléments `<message>` (voir `<selector>` ci-dessus), l'attribut `id` : c'est la valeur de cet attribut qui contrôle l'unicité ;
+   l'élément `<keyref>` crée un lien à un élément `<key>`, il est placé au même niveau que cet élément `<key>` et y est attaché via son attribut `refer` qui prend la valeur de l'attribut `name` de `<key>`:
    +   l'expression XPath de son enfant `<selector>` renseigne, depuis la racine `<messages>` du document XML, les éléments `<message>` : ce sont donc ces derniers éléments qui sont liés à la clé ;
    +   l'expression XPath de son enfant `<field>` renseigne, depuis chacun des éléments `<message>` (voir `<selector>` ci-dessus), l'attribut `replyTo` : c'est la valeur de cet attribut qui doit être égale à une de celles qui contrôlent l'unicité de la clé.

Notez bien que les tests de nœuds dans les expressions XPath utilisent les noms complets des _éléments_ : préfixe d'espace de noms et nom local. Pour ce qui concerne les noms d'_attributs_, il ne sont pas qualifiés car :

+   dans le fichier XML, comme on n'utilise pas de préfixe pour l'espace de noms `https://esi-bru.be/WEBR4/XML`, tous les éléments sont placés dans l'espace de noms global, mais pas les attributs : voir <https://stackoverflow.com/a/46865> et <https://www.w3.org/TR/REC-xml-names/#defaulting> ;
+   dans le fichier XSD, la valeur par défaut de l'attribut `attributeFormDefault` de la racine `<schema>` est `unqualified` : voir <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_schema>

#### Validation

```shell
xmllint --noout --schema 10_messages.xsd 10_messages.xml
```

### Exemple où un problème peut se poser

On reprend l'exemple d'utilisation de la contrainte `<unique>` avec un `<field>` facultatif. Cela ne pose pas de problème avec `<unique>`, mais parfois bien avec `<key>`.

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 15_samples_alt_key.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:esi="https://esi-bru.be/WEBR4/XML" elementFormDefault="qualified">

    <xsd:simpleType name="ControlType">
        <xsd:restriction base="xsd:integer"/>
    </xsd:simpleType>

    <xsd:simpleType name="DataType">
        <xsd:restriction base="xsd:integer"/>
    </xsd:simpleType>

    <xsd:complexType name="DataSetType">
        <xsd:sequence>
            <xsd:element name="control" type="esi:ControlType" minOccurs="0"/>
            <xsd:element name="data" type="esi:DataType" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="SampleType">
        <xsd:sequence >
            <xsd:element name="dataset" type="esi:DataSetType" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- on désire : <dataset> unique au sein de <sample> avec
                     <control> comme valeur de contrôle de l'unicité -->
    <xsd:complexType name="SamplesType">
        <xsd:sequence>
            <xsd:element name="sample" type="esi:SampleType" maxOccurs="unbounded">
                <!-- key à la place de unique -->
                <xsd:key name="unique_dataset_within_sample_as_a_key">
                    <xsd:selector xpath="esi:dataset"/>
                    <xsd:field xpath="esi:control"/>
                </xsd:key>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="samples" type="esi:SamplesType"/>

</xsd:schema>
```

On a ici :

+   la contrainte `<key>` est placé dans la définition de l'élément `<sample>` du fichier XML, l'élément qui doit être unique doit donc l'être au sein de chacun des éléments `<sample>` du document ;
+   l'expression XPath de l'élément `<selector>` renseigne, depuis chaque élément `<sample>` du document XML, les éléments `<dataset>` : ce sont donc les éléments `<dataset>` qui doivent être uniques dans les éléments `<sample>` ;
+   l'expression XPath de l'élément `<field>` renseigne, depuis chacun des éléments `<dataset>` (voir `<selector>` ci-dessus), son élément enfant `<control>` : c'est donc la valeur de l'élément `<control>` qui contrôle l'unicité d'un élément `<dataset>` au sein d'un `<sample>` ;
+   la présence d'un  `<control>` dans un `<sample>` n'est pas obligatoire ; cela n'empêche pas d'imposer une contrainte d'unicité sur les `<dataset>` via l'élément `<control>`, mais les fichiers XML pour lesquels des éléments `<control>` manquent ne sont pas valides.

Notez bien que les tests de nœuds dans les expressions XPath utilisent les noms complets des éléments : préfixe d'espace de noms et nom local.

#### XML OK

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 15_samples_alt_key_valid.xml -->
<samples xmlns="https://esi-bru.be/WEBR4/XML"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 15_samples_alt_key.xsd">
    <sample>
        <dataset>
            <control>11</control>
            <data>11</data>
            <data>33</data>
            <data>55</data>
            <data>66</data>
            <data>88</data>
        </dataset>
        <dataset>
            <control>22</control>
            <data>11</data>
            <data>44</data>
            <data>55</data>
        </dataset>
        <dataset>
            <control>55</control>
            <data>00</data>
            <data>22</data>
            <data>55</data>
            <data>11</data>
            <data>11</data>
        </dataset>
    </sample>
    <sample>
        <dataset>
            <control>11</control>
            <data>33</data>
        </dataset>
        <dataset>
            <control>88</control>
            <data>33</data>
            <data>11</data>
        </dataset>
    </sample>
    <sample>
        <dataset>
            <control>33</control>
            <data>33</data>
        </dataset>
        <dataset>
            <control>22</control>
            <data>33</data>
            <data>11</data>
            <data>33</data>
        </dataset>
        <dataset>
            <control>88</control>
            <data>33</data>
        </dataset>
    </sample>
</samples>
```

##### Validation

```shell
xmllint --noout --schema 15_samples_alt_key.xsd 15_samples_alt_key_valid.xml
```

Le fichier passe la validation car il n'y a pas de `<dataset>` sans `<control>`.

#### XML KO

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 16_samples_alt_key_invalid.xml -->
<samples xmlns="https://esi-bru.be/WEBR4/XML"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 15_samples_alt_key.xsd">
    <sample>
        <dataset>
            <control>11</control>
            <data>11</data>
            <data>33</data>
            <data>55</data>
            <data>66</data>
            <data>88</data>
        </dataset>
        <dataset>
            <control>22</control>
            <data>11</data>
            <data>44</data>
            <data>55</data>
        </dataset>
        <dataset>
            <control>55</control>
            <data>00</data>
            <data>22</data>
            <data>55</data>
            <data>11</data>
            <data>11</data>
        </dataset>
    </sample>
    <sample>
        <dataset>
            <control>11</control>
            <data>33</data>
        </dataset>
        <dataset>
            <control>88</control>
            <data>33</data>
            <data>11</data>
        </dataset>
    </sample>
    <sample>
        <dataset>
            <data>33</data>
            <!-- cvc-identity-constraint.4.2.1.a: Element "sample" has no value
            for the key "unique_dataset_within_sample_as_a_key".xml(AbsentKeyValue)
            -->
        </dataset>
        <dataset>
            <data>33</data>
            <data>11</data>
            <data>33</data>
            <!-- cvc-identity-constraint.4.2.1.a: Element "sample" has no value
            for the key "unique_dataset_within_sample_as_a_key".xml(AbsentKeyValue)
            -->
        </dataset>
        <dataset>
            <control>88</control>
            <data>33</data>
        </dataset>
    </sample>
</samples>
```

##### Validation

```shell
xmllint --noout --schema 15_samples_alt_key.xsd 16_samples_alt_key_invalid.xml

Element '{https://esi-bru.be/WEBR4/XML}dataset': Not all fields of key identity-constraint
'{https://esi-bru.be/WEBR4/XML}unique_dataset_within_sample_as_a_key' evaluate to a node.
Element '{https://esi-bru.be/WEBR4/XML}dataset': Not all fields of key identity-constraint
'{https://esi-bru.be/WEBR4/XML}unique_dataset_within_sample_as_a_key' evaluate to a node.
05_samples_alt_key_invalid.xml fails to validate
```

Le fichier ne passe pas la validation car il existe des `<dataset>` sans `<control>`.

### Exemple où key et keyref ne sont pas définis au même niveau

#### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 20_mails.xml -->
<mails xmlns="https://esi-bru.be/WEBR4/XML" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 20_mails.xsd">
    <messages>
        <message>
            <sender>u3</sender>
            <content>bla bla...</content>
        </message>
    </messages>

    <users>
        <user id="u31" />
        <user id="u3" />
    </users>
</mails>
```

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 20_mails.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:esi="https://esi-bru.be/WEBR4/XML" elementFormDefault="qualified">

    <xsd:simpleType name="SenderType">
        <xsd:restriction base="esi:UserIdType" />
    </xsd:simpleType>

    <xsd:simpleType name="ContentType">
        <xsd:restriction base="xsd:string" />
    </xsd:simpleType>

    <xsd:complexType name="MessageType">
        <xsd:sequence>
            <xsd:element name="sender" type="esi:SenderType">
                <!-- keyref _pas_ au même niveau que key -->
                <!-- <xsd:keyref name="refIdUser" refer="esi:idUser">
                    <xsd:selector xpath="." />
                    <xsd:field xpath="." />
                </xsd:keyref> -->
                <!-- Identity Constraint error: the keyref identity constraint
                     "refIdUser" refers to a key or unique that is out of scope. -->
            </xsd:element>
            <xsd:element name="content" type="esi:ContentType" />
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="MessagesType">
        <xsd:sequence>
            <xsd:element name="message" type="esi:MessageType" maxOccurs="unbounded">
                <!-- keyref _pas_ au même niveau que key -->
                <!-- <xsd:keyref name="refIdUser" refer="esi:idUser">
                        <xsd:selector xpath="esi:sender" />
                        <xsd:field xpath="." />
                     </xsd:keyref> -->
                <!-- Identity Constraint error: the keyref identity constraint
                     "refIdUser" refers to a key or unique that is out of scope. -->
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:simpleType name="UserIdType">
        <xsd:restriction base="xsd:string">
            <xsd:pattern value="u[0-9]+" />
            <!-- <xsd:whiteSpace value="preserve" /> -->
            <xsd:whiteSpace value="collapse" />
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="UserType">
        <xsd:attribute name="id" type="esi:UserIdType" use="required" />
    </xsd:complexType>

    <xsd:complexType name="UsersType">
        <xsd:sequence>
            <xsd:element name="user" type="esi:UserType" maxOccurs="unbounded" />
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="MailsType">
        <xsd:sequence>
            <xsd:element name="messages" type="esi:MessagesType">
                <!-- keyref _pas_ au même niveau que key -->
                <!-- <xsd:keyref name="refIdUser" refer="esi:idUser">
                        <xsd:selector xpath="esi:message/esi:sender" />
                        <xsd:field xpath="." />
                     </xsd:keyref> -->
                <!-- Identity Constraint error: the keyref identity constraint
                     "refIdUser" refers to a key or unique that is out of scope. -->
            </xsd:element>
            <xsd:element name="users" type="esi:UsersType">
                <!-- on désire unicité au sein de users -->
                <xsd:key name="idUser">
                    <!-- user doit être unique -->
                    <xsd:selector xpath="esi:user" />
                    <!-- id utilisé pour tester l'unicité -->
                    <xsd:field xpath="@id" />
                </xsd:key>
                <!-- keyref _pas_ au même niveau que key -->
                <!-- <xsd:keyref name="refIdUser" refer="esi:idUser">
                        <xsd:selector
                            xpath="../esi:messages/esi:message/esi:sender" />
                        <xsd:field xpath="." />
                     </xsd:keyref> -->
                <!-- c-general-xpath: The expression
                     '../esi:messages/esi:message/esi:sender' is not valid with
                     respect to the XPath subset supported by XML Schema. -->
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="mails" type="esi:MailsType">
        <!-- keyref _pas_ au même niveau que key -->
        <xsd:keyref name="refIdUser" refer="esi:idUser">
            <xsd:selector xpath="esi:messages/esi:message/esi:sender" />
            <xsd:field xpath="." />
        </xsd:keyref>
    </xsd:element>

</xsd:schema>
```

##### key

On a ici :

+   la contrainte `<key>` est placé dans la définition de l'élément `<users>` du fichier XML, l'élément qui doit être unique doit donc l'être au sein de chacun des éléments `<users>` du document ;
+   l'expression XPath de l'élément `<selector>` renseigne, depuis chaque élément `<users>` du document XML, les éléments `<user>` : ce sont donc les éléments `<user>` qui doivent être uniques dans les éléments `<users>` ;
+   l'expression XPath de l'élément `<field>` renseigne, depuis chacun des éléments `<user>` (voir `<selector>` ci-dessus), son attribut `id` : c'est donc la valeur de l'attribut `id` qui contrôle l'unicité d'un élément `<user>` au sein de `<users>`.

##### keyref

On désire que la valeur du nœuds textuel de l'élément `<sender>`, enfant de `<message>`, lui-même enfant de `<messages>` sous la racine `<mails>`, soit une référence de clé attachée à la clé définie par l'attribut `id` des `<user>`.

Une première tentative pour atteindre cet objectif peut consister en la définition de la contrainte `<keyref>` là où la contrainte `<key>` est définie, c'est-à-dire dans l'élément `<users>`. Il faut alors _remonter la hiérarchie des nœuds_ dans `<selector>` pour atteindre `<sender>` depuis `<users>`. Cela n'est pas possible. Seuls les axes _child_ et _attribute_ sont utilisables dans les expressions XPath d'un schéma XML.

Une seconde tentative est de définir la contrainte `<keyref>` exactement là où elle s'applique, c'est-à-dire dans la définition de l'élément `<sender>`. Cela non plus n'est pas possible, car la définition de la clé à laquelle la référence est attachée n'est pas dans la portée de l'élément `<sender>`. Pour être dans la portée d'un élément, la définition de la clé doit apparaître dans cet élément ou dans un élément de sa descendance.

Pour la même raison, il n'est pas possible de définir la contrainte de clé étrangère dans les éléments `<message>` ou `<messages>`.

On en arrive dès lors à la définition de la contrainte `<keyref>` dans celle de l'élément `<mails>`. La clé est définie dans la descendance de cet élément. Elle est donc à la portée de la définition de la référence de clé.

_Remarque_ : Notez l'utilisation de la restriction `<whiteSpace>` (<https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_whiteSpace>) dans le type de la clé (et de la référence de clé).

##### Validation

```shell
xmllint --noout --schema 20_mails.xsd 20_mails.xml
```

## Exercices

1.  Écrivez un schéma pour le fichier `30_esi.xml` utilisant adéquatement les contraintes d'unicité, de clé et de référence de clé.
    Comme il n'est pas possible d'associer une liste de `keyref` à des `key` (<https://stackoverflow.com/q/40428801>) sans recourir à XML Schema 1.1 (<https://xfront.com/xml-schema-1-1/index.html>, ), il est impossible de répondre complètement à la question précédente avec XML Schema 1.0.
2.  Le fichier `50_data_ko.xml` est mal formé. Modifiez-le de sorte qu'il soit bien formé. Nommez `50_data.xml` le fichier XML bien formé obtenu.
3.  Le fichier `50_data.xsd` contient le schéma du format XML `mediawiki` : <http://www.mediawiki.org/xml/export-0.10.xsd>. Complétez-le de sorte que dans le fichier `50_data.xml`, les éléments `<revision>` soient uniques au sein du fichier XML, leur enfant `<id>` contrôlant leur unicité. De plus, l'élément `<parentid>` de chaque `<revision>` doit obligatoirement faire référence à un élément `<id>` de `<revision>`.
    Dans le fichier `50_data.xml`, il existe un élément `<revision>` dont le `<parentid>` ne respecte pas cette contrainte de clé étrangère. Identifiez-le et modifier la valeur du `<parentid>` de sorte à ce que le document XML soit valide.
    Ne perdez pas de vue que tous les éléments de `mediawiki` se trouvent dans l'espace de nommage (_namespace_) <http://www.mediawiki.org/xml/export-0.10/>.
