# XML Schema : type simple : restriction, union, list

On peut construire un type simple (`<simpleType>`, <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleType>) comme une restriction, une union ou une liste.

## restriction

De nombreux exemples de restrictions ont déjà été vus. On ne consacre dès lors pas d'exemple spécifique ici.

### Documentation

Élément :

-   `<restriction>` : enfant de `<simpleType>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_restriction>
-   facets : <https://www.w3schools.com/XML/schema_facets.asp> :
    -   `<minExclusive>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_minExclusive>
    -   `<minInclusive>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_minInclusive>
    -   `<maxExclusive>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_maxExclusive>
    -   `<maxInclusive>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_maxInclusive>
    -   `<totalDigits>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_totalDigits>
    -   `<fractionDigits>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_fractionDigits>
    -   `<length>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_length>
    -   `<minLength>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_minLength>
    -   `<maxLength>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_maxLength>
    -   `<enumeration>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_enumeration>
    -   `<whiteSpace>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_whiteSpace>
    -   `<pattern>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_pattern>

## union

### XML

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<scores xmlns="https://esi-bru.be/WEBR4/XML"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 00_scores.xsd">
    <score>12</score>
    <score>GDIS</score>
    <score>6</score>
    <score>10</score>
    <score>NA</score>
    <score>SAT</score>
    <score>9</score>
    <score>18</score>
    <score>ACQ</score>
    <score>0</score>
    <score>12</score>
</scores>
```

### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_scores.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 xmlns:esi="https://esi-bru.be/WEBR4/XML"
 elementFormDefault="qualified">

  <xsd:simpleType name="NumScoreType">
    <xsd:restriction base="xsd:nonNegativeInteger">
        <xsd:maxInclusive value="20"/>
    </xsd:restriction>
  </xsd:simpleType>

  <xsd:simpleType name="TextScoreType">
    <xsd:restriction base="xsd:NMTOKEN">
        <xsd:enumeration value="NA"/>
        <xsd:enumeration value="ACQ"/>
        <xsd:enumeration value="SAT"/>
        <xsd:enumeration value="DIS"/>
        <xsd:enumeration value="GDIS"/>
        <xsd:enumeration value="TGDIS"/>
        <xsd:enumeration value="LPGDIS"/>
    </xsd:restriction>
  </xsd:simpleType>

  <xsd:simpleType name="ScoreType">
    <xsd:union memberTypes="esi:NumScoreType esi:TextScoreType"/>
  </xsd:simpleType>

  <xsd:complexType name="ScoresType">
    <xsd:sequence>
        <xsd:element name="score" type="esi:ScoreType"
                     maxOccurs="unbounded"/>
    </xsd:sequence>
  </xsd:complexType>

  <!-- global element -->
  <xsd:element name="scores" type="esi:ScoresType"/>

</xsd:schema>
```
### Documentation

Élement :

-   `<union>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_union>

### Validation

```shell
xmllint --noout --schema 00_scores.xsd 00_scores.xml
```

## list

### XML

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<periods xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 10_periods.xsd">
    <period>BIM3</period>
    <period>BIM3 BIM1</period>
    <period>BIM2 BIM4</period>
    <period>BIM1 BIM4 BIM3</period>
    <period>BIM1 BIM4 BIM1</period>
    <period>BIM1 BIM4 BIM3 BIM2</period>
    <period>BIM3 BIM3 BIM3 BIM3</period>
</periods>
```

### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_periods.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:esi="https://esi-bru.be/WEBR4/XML"
  elementFormDefault="qualified">

    <xsd:simpleType name="PeriodElementType">
        <xsd:restriction base="xsd:string">
            <xsd:pattern value="BIM[1-4]{1}"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:simpleType name="PeriodUnlimitedType">
        <xsd:list itemType="esi:PeriodElementType"/>
    </xsd:simpleType>

    <xsd:simpleType name="PeriodLimitedType">
        <xsd:restriction base="esi:PeriodUnlimitedType">
            <!-- obligatoire ici : 0 par défaut -->
            <xsd:minLength value="1"/>
            <xsd:maxLength value="4"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="PeriodsType">
        <xsd:sequence>
            <xsd:element name="period"
                         type="esi:PeriodLimitedType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="periods" type="esi:PeriodsType"/>

</xsd:schema>
```
### Documentation

Élement :

-   `<list>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_list>

### Validation

```shell
xmllint --noout --schema 10_periods.xsd 10_periods.xml
```

## Exercices

1.  Écrivez un schéma pour le fichier `30_esi.xml`.
