# XML Schema : premier contact

XSD : XML Schema Definition

## Références

### Documents W3C

XML Schema : <https://www.w3.org/XML/Schema>

-   Types de base : <https://www.w3.org/TR/xmlschema-2/>
-   Structures (constructions) : <https://www.w3.org/TR/xmlschema-1/>

### Tutoriels et références divers

À la base de ce document : Using W3C XML Schema : <https://www.xml.com/pub/a/2000/11/29/schemas/part1.html>

Référence des constructions XSD (pas les types de base) : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html>

Types de base :

-   <https://www.xml.com/pub/a/2000/11/29/schemas/dataref.html>
-   <https://www.w3.org/TR/xmlschema-2/#built-in-datatypes>

Wikipédia : <https://fr.wikipedia.org/wiki/XML_Schema>

Essential XML Quick Reference : <https://www.pearson.com/store/p/essential-xml-quick-reference-a-programmer-s-reference-to-xml-xpath-xslt-xml-schema-soap-and-more/P100000683470/9780201740950>

Structurez vos données avec XML (pour commencer en douceur, un peu léger) : <https://tutoriel-xml.rolandl.fr/>

#### w3schools.com

Tutoriel XSD : <https://www.w3schools.com/xml/schema_intro.asp>

Référence XSD : <https://www.w3schools.com/xml/schema_elements_ref.asp>

### Comparaison avec DTD et autres

voir ici : <https://www.xml.com/pub/a/2001/12/12/schemacompare.html>

## xmllint

Avec `file.xml` et `file.xsd` :

-   XML bien formé :

    ```shell
    xmllint --noout file.xml
    ```

-   XML bien formé et valide au regard de `file.xsd` :

    ```shell
    xmllint --noout --schema file.xsd file.xml
    ```

Dans tous les cas, si rien ne s'affiche, c'est que tout est ok.

Documentation `xmllint` : <http://xmlsoft.org/xmllint.html>

## Premiers exemples

### Fichier XML

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 00_teachers.xml -->
<!-- https://www.he2b.be/personnel-esi -->
<teachers>
    <person>
        <id>BEJ</id>
        <firstname>Jonas</firstname>
        <name>Beleho</name>
    </person>
    <person>
        <id>HAL</id>
        <firstname>Amine</firstname>
        <name>Hallal</name>
    </person>
    <person>
        <id>XPA</id>
        <firstname>Xavier</firstname>
        <name>Paulus</name>
    </person>
</teachers>
```

### Fichier XSD : poupées russes

#### Espace de nommage

Les structures et types définis par le standard XML Schema sont définis
dans l'espace de noms
[http://www.w3.org/2001/XMLSchema](https://www.w3.org/TR/xmlschema-1/#Instance_Document_Constructions).

#### Définitions

-   Élément global : élément enfant de l'élément racine `<schema>` du schéma XML.
-   Élément local : élément petit-enfant, arrière petit-enfant, etc. de l'élément racine `<schema>` du schéma XML.

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 00_teachers_pr.xsd -->
<!-- variante poupées russes -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <xsd:element name="teachers">
    <xsd:complexType>
      <xsd:sequence>
        <xsd:element name="person" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:sequence>
              <xsd:element name="id" type="xsd:string"/>
              <xsd:element name="firstname" type="xsd:string"/>
              <xsd:element name="name" type="xsd:string"/>
            </xsd:sequence>
          </xsd:complexType>
        </xsd:element>
      </xsd:sequence>
    </xsd:complexType>
  </xsd:element>
</xsd:schema>
```

#### Documentation

Élément :

-   `<schema>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_schema>
-   `<element>` :
    -   global : utilisé ici pour `teachers` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_element>
    -   local : utilisé ici pour `person` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_choice_element>
-   `<complexType>` :
    -   global : pas utilisé ici mais plus bas dans `00_teachers_dt.xsd` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_complexType>
    -   local : utilisé ici pour `teachers` et `person` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all_element_complexType>
-   `<sequence>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_sequence>

#### Validation

```shell
xmllint --noout --schema 00_teachers_pr.xsd 00_teachers.xml
```

### Fichier XSD : éléments globaux

#### Rappel

-   Élément global : élément enfant de l'élément racine `<schema>` du schéma XML.
-   Élément local : élément petit-enfant, arrière petit-enfant, etc. de l'élément racine `<schema>` du schéma XML.

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 00_teachers_eg.xsd -->
<!-- variante éléments globaux -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">

    <!-- éléments de types simples -->

    <xsd:element name="id" type="xsd:string"/>

    <xsd:element name="firstname" type="xsd:string"/>

    <xsd:element name="name" type="xsd:string"/>

    <!-- éléments de types complexes -->

    <xsd:element name="person">
        <xsd:complexType>
            <xsd:sequence>
                <xsd:element ref="id"/>
                <xsd:element ref="firstname"/>
                <xsd:element ref="name"/>
            </xsd:sequence>
        </xsd:complexType>
    </xsd:element>

    <xsd:element name="teachers">
        <xsd:complexType>
            <xsd:sequence>
                <xsd:element ref="person" maxOccurs="unbounded"/>
            </xsd:sequence>
        </xsd:complexType>
    </xsd:element>
</xsd:schema>
```

#### Validation

```shell
xmllint --noout --schema 00_teachers_eg.xsd 00_teachers.xml
```

#### Autres fichiers XML

Ici, tous les éléments sont globaux. Le fichier `00_teachers_eg.xsd` peut valider un fichier XML d'élément racine `<teachers>`, comme `00_teachers.xml`, mais aussi, par exemple :

-   le fichier `10_person.xml` d'élément racine `<person>` :

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <!-- 10_person.xml -->
    <person>
        <id>HAL</id>
        <firstname>Amine</firstname>
        <name>Hallal</name>
    </person>
    ```
    ```shell
    xmllint --noout --schema 00_teachers_eg.xsd 10_person.xml
    ```

-   le fichier `10_id.xml` d'élément racine `<id>` :

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <!-- 10_id.xml -->
    <id>HAL</id>
    ```
    ```shell
    xmllint --noout --schema 00_teachers_eg.xsd 10_id.xml
    ```

Tout élément global d'un schéma peut servir de racine dans un document
XML valide au regard de ce schéma.

### Fichier XSD : définition de types

#### Notion de type et utilisations

Type simple :

+   attribut ;
+   élément sans enfant ni attribut.

Type complexe :

+   contenu simple : élément avec attributs mais sans enfant ;
+   contenu complexe : élément avec enfants et possiblement des attributs.

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 00_teachers_dt.xsd -->
<!-- variante définitions de types -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">

    <!-- définitions des types simples -->

    <xsd:simpleType name="IdType">
        <xsd:restriction base="xsd:string">
            <xsd:length value="3"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:simpleType name="FirstnameType">
        <xsd:restriction base="xsd:string"/>
    </xsd:simpleType>

    <xsd:simpleType name="NameType">
        <xsd:restriction base="xsd:string"/>
    </xsd:simpleType>

    <!-- définition des types complexes -->

    <xsd:complexType name="PersonType">
        <xsd:sequence>
            <xsd:element name="id" type="IdType"/>
            <xsd:element name="firstname" type="FirstnameType" />
            <!-- <xsd:element name="firstname" type="xsd:string" /> -->
            <!-- <xsd:element name="firstname" type="NameType" /> -->
            <xsd:element name="name" type="NameType"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="TeachersType">
        <xsd:sequence>
            <xsd:element name="person" type="PersonType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- élément global -->

    <xsd:element name="teachers" type="TeachersType"/>

</xsd:schema>
```

#### Documentation

Élément :

-   `<simpleType>` :
    -   global : utilisé ici pour `IdType` par exemple : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleType>
    -   local : pas utilisé ici : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all_element_simpleType>
-   `<restriction>` : enfant de `<simpleType>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_restriction>

#### Validation

```shell
xmllint --noout --schema 00_teachers_dt.xsd 00_teachers.xml
```

L'ordre de définition des types / éléments dans le fichier xsd n'a pas d'importance. Par exemple, essayer le fichier `10_teachers_dt_mel.xsd` :

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 10_teachers_dt_mel.xsd -->
<!-- variante définitions de types : mélange -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">

    <!-- élément global -->

    <xsd:element name="teachers" type="TeachersType"/>

    <!-- définition des types complexes -->

    <xsd:complexType name="TeachersType">
        <xsd:sequence>
            <xsd:element name="person" type="PersonType"
                         maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="PersonType">
        <xsd:sequence>
            <xsd:element name="id" type="IdType"/>
            <xsd:element name="firstname" type="FirstnameType"/>
            <xsd:element name="name" type="NameType"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- définitions des types simples -->

    <xsd:simpleType name="IdType">
        <xsd:restriction base="xsd:string">
            <xsd:length value="3"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:simpleType name="FirstnameType">
        <xsd:restriction base="xsd:string"/>
    </xsd:simpleType>

    <xsd:simpleType name="NameType">
        <xsd:restriction base="xsd:string"/>
    </xsd:simpleType>

</xsd:schema>
```
```shell
xmllint --noout --schema 10_teachers_dt_mel.xsd 00_teachers.xml
```

### Groupe d'éléments

Il est possible de créer des groupes d'éléments `<group>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_group> qu'on peut ensuite référencer. On ne le fait pas ici.

On peut faire de même avec des groupes d'attributs `<attributeGroup>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_attributeGroup>. On ne le fait pas ici. Peut-être plus tard quand on voit les attributs...

## Exercices

1.  Produire une DTD pour le fichier `00_teachers.xml`.

## Types de base

![Types de base](./images/type-hierarchy.gif "Types de base")
