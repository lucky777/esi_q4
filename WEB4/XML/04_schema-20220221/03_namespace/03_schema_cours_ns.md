# XML Schema : espace de noms

## Pas d'espace de noms

### XSD pas renseigné dans XML

#### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_datas_nons.xml -->
<datas>
    <value>23</value>
    <value>45</value>
    <value>12</value>
</datas>
```

#### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_datas_nons.xsd -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">

    <xsd:simpleType name="ValueType">
        <xsd:restriction base="xsd:integer">
            <xsd:minInclusive value="10"/>
            <xsd:maxExclusive value="50"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="value" type="ValueType"
                         minOccurs="0" maxOccurs="5"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="datas" type="DatasType"/>

</xsd:schema>
```

#### Validation

```shell
xmllint --noout --schema 00_datas_nons.xsd 00_datas_nons.xml
```

### XSD renseigné dans XML

#### Espace de nommage

Les attributs utilisables dans tout document XML et définis par le
standard XML Schema sont définis dans l'espace de noms
[http://www.w3.org/2001/XMLSchema-instance](https://www.w3.org/TR/xmlschema-1/#Instance_Document_Constructions).

#### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 05_datas_nons.xml -->
<datas xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:noNamespaceSchemaLocation="00_datas_nons.xsd">
    <value>23</value>
    <value>45</value>
    <value>12</value>
</datas>
```

À partir de maintenant, on prend l'habitude de renseigner le schéma dans le document XML.

#### XSD

Aucune différence avec le schéma précédent.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_datas_nons.xsd -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">

    <xsd:simpleType name="ValueType">
        <xsd:restriction base="xsd:integer">
            <xsd:minInclusive value="10"/>
            <xsd:maxExclusive value="50"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="value" type="ValueType"
                         minOccurs="0" maxOccurs="5"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="datas" type="DatasType"/>

</xsd:schema>
```

#### Documentation

Attribut dans le document XML :

- `noNamespaceLocation` : <https://www.w3.org/TR/xmlschema-1/#xsi_schemaLocation>

#### Validation

`xmllint` n'utilise pas les information de localistion du fichier `xsd` présentes dans le fichier `xml`. Il faut, comme précédemment, utiliser l'option `--schema` :

```shell
xmllint --noout --schema 00_datas_nons.xsd 05_datas_nons.xml
```

## Espace de noms

### XML : espace de noms préfixé

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_datas_ns_pref.xml -->
<webr4:datas xmlns:webr4="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 10_datas_ns.xsd">
<!-- <webr4:datas xmlns:webr4="https://esi-bru.be/WEBR4/XML"> -->
    <webr4:value>23</webr4:value>
    <webr4:value>45</webr4:value>
    <webr4:value>12</webr4:value>
</webr4:datas>
```

### XSD

Ne pas oublier `elementFormDefault="qualified"`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_datas_ns.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="ValueType">
        <xsd:restriction base="xsd:integer">
            <xsd:minInclusive value="10"/>
            <xsd:maxExclusive value="50"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="value" type="esi:ValueType"
                         minOccurs="0" maxOccurs="5"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="datas" type="esi:DatasType"/>

</xsd:schema>
```

Notez que les types `ValueType` et `DatasType`, ainsi que les éléments `value` et `datas`, sont placés et définis dans le _target namespace_ `https://esi-bru.be/WEBR4/XML`.

Les choix de préfixes dans les fichiers `xml` et `xsd` sont totalement indépendants. Ce qui compte, c'est le nom de l'espace de noms.

#### Documentation

##### Dans le document XML

Attribut :

- `schemaLocation` : <https://www.w3.org/TR/xmlschema-1/#xsi_schemaLocation>

##### Dans le schéma

Élément :

-   `<schema>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_schema>

### Validation

```shell
xmllint --noout --schema 10_datas_ns.xsd 10_datas_ns_pref.xml
```

### XSD alternatif

Sans `elementFormDefault="qualified"`, il faut ajouter `form="qualified"` aux définitions d'éléments, mais pas à celle de l'élément racine (?).

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 12_datas_ns.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML">

    <xsd:simpleType name="ValueType">
        <xsd:restriction base="xsd:integer">
            <xsd:minInclusive value="10"/>
            <xsd:maxExclusive value="50"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="value" type="esi:ValueType"
                         minOccurs="0" maxOccurs="5"
                         form="qualified"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="datas" type="esi:DatasType"/>

</xsd:schema>
```

### Validation

```shell
xmllint --noout --schema 12_datas_ns.xsd 10_datas_ns_pref.xml
```

### XML : espace de noms par défaut

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_datas_ns.xml -->
<datas xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 10_datas_ns.xsd">
    <value>23</value>
    <value>45</value>
    <value>12</value>
</datas>
```

### XSD

Les fichiers `xsd` précédents font l'affaire.

### Validation

```shell
xmllint --noout --schema 10_datas_ns.xsd 10_datas_ns.xml
```

ou

```shell
xmllint --noout --schema 12_datas_ns.xsd 10_datas_ns.xml
```

## Exercices

1.  Modifiez le fichier `20_teachers_ns.xml` de sorte qu'il contiennent les informations de localisation de son fichier schéma... et produisez ce fichier `xsd`.
