# XML Schema : compositors

Un type complexe peut être obtenu par composition ou par extension. Les _compositors_ sont illustrés ici. La notion d'extension est abordée lorsque les types d'éléments sans enfants mais avec attributs et contenu textuel (complexType, simpleContent) sont envisagés.

Il y a 3 _compositors_ :

-   `<sequence>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_sequence> ;
-   `<choice>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_choice> ;
-   `<all>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all>.

## sequence

### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_datas_seq.xml -->
<datas xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 00_datas_seq.xsd">

    <value>23.12</value>
    <text>1234567</text>
    <text>12</text>

    <value>4511</value>
    <text>1234567890</text>
    <text>12345</text>

    <value>123.2</value>
    <text>123456</text>
    <text>123456</text>

    <value>.12</value>
    <text>1234567</text>
    <text>12</text>

    <value>-231</value>
    <text>1234567</text>
    <text>12</text>

</datas>
```

### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_datas_seq.xsd -->
<xsd:schema
targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="ValueType">
        <xsd:restriction base="xsd:decimal">
            <xsd:totalDigits value="4"/>
            <xsd:fractionDigits value="2"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:simpleType name="TextType">
        <xsd:restriction base="xsd:string">
            <xsd:minLength value="2"/>
            <xsd:maxLength value="10"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="DatasType">
        <xsd:sequence maxOccurs="5">
            <xsd:element name="value" type="esi:ValueType"/>
            <xsd:element name="text" type="esi:TextType"
                         minOccurs="2" maxOccurs="2"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="datas" type="esi:DatasType"/>

</xsd:schema>
```

### Documentation

Élément :

-   `<sequence>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_sequence>

### Validation

```shell
xmllint --noout --schema 00_datas_seq.xsd 00_datas_seq.xml
```

## choice

### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_datas_choice.xml -->
<datas xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 10_datas_choice.xsd">

    <item>
        <numericalValue>23</numericalValue>
        <textualValue>abcd</textualValue>
    </item>

    <item>
        <numericalValue>-14</numericalValue>
        <textualValue>abcd</textualValue>
    </item>

    <item>
        <numTextValue>16aXAd</numTextValue>
    </item>

</datas>
```

### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_datas_choice.xsd -->
<xsd:schema
targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="NumericalValueType">
        <xsd:restriction base="xsd:integer">
            <xsd:totalDigits value="2"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:simpleType name="TextualValueType">
        <xsd:restriction base="xsd:string">
            <xsd:length value="4"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:simpleType name="NumTextValueType">
        <xsd:restriction base="xsd:string">
            <xsd:pattern value="[0-9]{2}[a-zA-Z]{4}"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="ItemType">
        <xsd:choice>
            <xsd:sequence>
                <xsd:element name="numericalValue"
                             type="esi:NumericalValueType"/>
                <xsd:element name="textualValue"
                             type="esi:TextualValueType"/>
            </xsd:sequence>
            <xsd:element name="numTextValue"
                         type="esi:NumTextValueType"/>
        </xsd:choice>
    </xsd:complexType>

    <xsd:complexType name="DatasType">
        <xsd:sequence minOccurs="0" maxOccurs="5">
            <xsd:element name="item" type="esi:ItemType"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="datas" type="esi:DatasType"/>

</xsd:schema>
```

### Documentation

Élément :

-   `<choice>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_choice>

### Validation

```shell
xmllint --noout --schema 10_datas_choice.xsd 10_datas_choice.xml
```

## all

On dispose aussi du compositor `<all>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all> pour indiquer que les éléments d'un ensemble doivent chacun apparaître une et une seule fois, mais dans n'importe quel ordre.

## Contenu mixte

Pour la validation de documents XML orientés texte plutôt que données.

### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 20_mixed_content.xml -->
<!-- https://fr.wikipedia.org/wiki/Casimir_(personnage) -->
<w:misc xmlns:w="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 20_mixed_content.xsd">
    Un peu de <w:a>texte et</w:a> des balises de <w:b>différents
    types pour</w:b><w:a>voir</w:a> ce que ça <w:a>donne</w:a>.
</w:misc>
```

### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 20_mixed_content.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:complexType name="MiscType" mixed="true">
        <xsd:choice minOccurs="0" maxOccurs="unbounded">
            <xsd:element name="a" type="xsd:string"/>
            <xsd:element name="b" type="xsd:string"/>
        </xsd:choice>
    </xsd:complexType>

    <!-- élément racine -->
    <xsd:element name="misc" type="esi:MiscType"/>

</xsd:schema>
```

### Validation

```shell
xmllint --noout --schema 20_mixed_content.xsd 20_mixed_content.xml
```

## Exercices

1.  Écrivez un schéma pour le fichier `30_biographie.xml`.
