# XML Schema : attributs

## Attribut d'élément avec contenu

### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_datas_attr.xml -->
<datas year="2020" xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 00_datas_attr.xsd">
    <value section="R">23</value>
    <value section="G">45</value>
    <value section="I">12</value>
</datas>
```

### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 00_datas_attr.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:esi="https://esi-bru.be/WEBR4/XML"
            elementFormDefault="qualified">

    <xsd:simpleType name="SectionType">
        <xsd:restriction base="xsd:string">
            <xsd:enumeration value="G"/>
            <xsd:enumeration value="I"/>
            <xsd:enumeration value="R"/>
            <!-- pattern également possible... -->
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:simpleType name="IntValueType">
        <xsd:restriction base="xsd:integer">
            <xsd:minInclusive value="10"/>
            <xsd:maxExclusive value="50"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="ValueType">
        <xsd:simpleContent>
            <xsd:extension base="esi:IntValueType">
                <xsd:attribute name="section"
                               type="esi:SectionType"
                               use="required"/>
            </xsd:extension>
        </xsd:simpleContent>
    </xsd:complexType>

    <xsd:complexType name="DatasType">
        <xsd:sequence minOccurs="0" maxOccurs="5">
            <xsd:element name="value" type="esi:ValueType"/>
        </xsd:sequence>
        <xsd:attribute name="year" type="xsd:integer"/>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="datas" type="esi:DatasType"/>

</xsd:schema>
```
### Documentation

Élement :

-   `<simpleContent>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleContent>
-   `<extension>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleContent_extension>
-   `<attribute>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all_element_complexType_attribute>

### Validation

```shell
xmllint --noout --schema 00_datas_seq.xsd 00_datas_seq.xml
```

## Élément vide

### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_datas_empty.xml -->
<datas xmlns="https://esi-bru.be/WEBR4/XML"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://esi-bru.be/WEBR4/XML 10_datas_empty.xsd">
    <item section="R"/>
    <item section="G"/>
    <item section="I"/>
    <item section="I"/>
</datas>
```

### XSD

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 10_datas_empty.xsd -->
<xsd:schema targetNamespace="https://esi-bru.be/WEBR4/XML"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:esi="https://esi-bru.be/WEBR4/XML"
elementFormDefault="qualified">

    <xsd:simpleType name="SectionType">
        <xsd:restriction base="xsd:string">
            <xsd:pattern value="[IRG]{1}"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:complexType name="ItemType">
        <xsd:attribute name="section" type="esi:SectionType"
                       use="required"/>
    </xsd:complexType>

    <xsd:complexType name="DatasType">
        <xsd:sequence>
            <xsd:element name="item" type="esi:ItemType"
                         minOccurs="4" maxOccurs="4"/>
        </xsd:sequence>
    </xsd:complexType>

    <!-- global element -->
    <xsd:element name="datas" type="esi:DatasType"/>

</xsd:schema>
```

### Validation

```shell
xmllint --noout --schema 10_datas_empty.xsd 10_datas_empty.xml
```

## Exercices

1.  Écrivez un schéma pour le fichier `30_produits.xml`.
2.  Donnez la (ou un exemple de) syntaxe XML Schema pour définir un élément vide _sans_ attribut.
