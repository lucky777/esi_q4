# XML Schema : types et contenus

## Type simple

`<simpleType>`

Les [types de base](https://www.w3.org/TR/xmlschema-2/#built-in-datatypes) sont des types simples.

### Utilisation

Pour définir le type :

- d'un attribut ;
- d'un élément sans attribut ni enfant.

### Contenu

Liste non exhaustive :

- `<restriction>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_restriction>
- `<list>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_list>
- `<union>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_union>

### Documentation

Types de base : <https://www.w3.org/TR/xmlschema-2/#built-in-datatypes>

Comme élément :

- global `<simpleType>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleType>
- local `<simpleType>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all_element_simpleType>

## Type complexe

`<complexType>`

### Utilisation

Pour définir le type :

- d'un élément avec un ou des attributs ;
- d'un élément avec un ou des enfants ;
- d'un élément avec un ou des attributs et un ou des enfants.

### Contenu

Liste non exhaustive :

- au plus un _compositor_ et un nombre non limité d'attributs :

    + `<sequence>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_sequence>
    + `<choice>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_choice>
    +  `<all>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all>
    +  `<attribute>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all_element_complexType_attribute>

- un contenu simple :

    + `<simpleContent>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleContent>

- un contenu complexe :

    + `<complexContent>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_complexContent>

La définition du type d'un élément sans contenu mais avec un ou des attributs utilise la première des constructions ci-dessus, _sans_ _compositor_.

### Documentation

Comme élément :

- global `<complexType>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_complexType>
- local `<complexType>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_all_element_complexType>

## Contenu simple

`<simpleContent>`

### Utilisation

À l'intérieur d'un type complexe (`<complexType>`) pour définir le type d'un élément avec du contenu textuel, sans enfant mais avec un ou des attributs.

### Contenu

Liste non exhaustive :

- `<restriction>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_restriction>
- `<extension>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleContent_extension>

d'un type simple (`<simpleType`).

### Documentation

`<simpleContent>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleContent>

## Contenu complexe

`<complexContent>`

### Utilisation

À l'intérieur d'un type complexe (`<complexType>`) pour définir le type d'un élément avec un ou des enfants ou un ou des attributs _dont le type se base sur un type complexe_.

### Contenu

Liste non exhaustive :

- `<restriction>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_restriction>
- `<extension>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_simpleContent_extension>

d'un type complexe (`<complexType`).

### Documentation

`<complexContent>` : <https://www.xml.com/pub/a/2000/11/29/schemas/structuresref.html#Elt_complexContent>
