$(document).ready(function () {
    var flickerUrl = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
    $.getJSON(flickerUrl,
        {
            tags: "rue royale,bruxelles",
            tagmode: "all", format: "json"
        },
        function (data) {
            var images = "";
            $.each(data.items, function (i, item) {
                images += "<img src='" + item.media.m + "' >";
                if (i === 4) {
                    return false;
                }
            });
            $("#images").html(images);
        });
});