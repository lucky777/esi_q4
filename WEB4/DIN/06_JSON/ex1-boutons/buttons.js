var json1 = '{ "nom": "MonNom", "prenom" : "MonPrenom"}'; 
var json2 = '[{ "nom": "MonNom", "prenom" : "MonPrenom"}, \
    { "nom": "SonNom", "prenom" : "SonPrenom"}]'; 
$(document).ready(function () {
    $("#b1").click(function () {
        var unePersonne = $.parseJSON(json1); 
        $("#personne").html("<p class='personne'>"
            + unePersonne.nom + ", " + unePersonne.prenom + "</p>");
    });
    $("#b2").click(function () {
        var desPersonnes = $.parseJSON(json2);
        var liste = "<ul>";
        $.each(desPersonnes, function (idx, personne) {
            liste += '<li class="personne">' + personne.nom
                + ", " + personne.prenom
                + "</li>";
        });
        liste += "</ul>"; 
        $("#personnes").html(liste);
    });
    $("#b3").click(function () {
        $.ajaxSetup({ mimeType: "text/plain" }); 
        $.getJSON("personnes.json", function (desPersonnes) {
            var liste = "<ul>";
            $.each(desPersonnes, function (key, personne) {
                liste += "<li class='personne'>"
                    + personne.nom + ", "
                    + personne.prenom + "</li>";
            });
            liste += "</ul>";
            $("#personnesAjax").html(liste);
        });
    });
});