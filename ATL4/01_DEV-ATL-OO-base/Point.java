class Point {
    private double x;
    private double y;

    public Point() {
        this(0, 0);
    }

    public Point(double x, double y) {
        LuckyTools lt = new LuckyTools();
        this.x=x;
        this.y=y;
        lt.println(Col.GREEN, "A baby Point was born!");
    }

    public Point(Point p) {
        this(p.x, p.y);
    }

    public double getX() {return this.x;}
    public double getY() {return this.y;}

    public void move(double dx, double dy) {
        this.x += dx;
        this.y += dy;
    }

    @Override
    public String toString() {
        return "(" +this.x+ ", " +this.y+ ")";
    }
}

class TestPoint {
    public static void main(String[] args) {
        Point p = new Point();
        System.out.println(p);
        p.move(2, 2);
        System.out.println(p);
    }
}