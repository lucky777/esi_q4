# Introduction

This module contains an ASCII painting application. It lets the user draw different shapes on a canvas as well as move
them around, change their colors. The main goal is to show how to create different commands (such as undo) as well as
different design patterns.

# Structure

For now the structure of the project is the following :

```
└── ascii
    ├── commands
    │   ├── AddCommand.java
    │   ├── ColorCommand.java
    │   ├── Command.java
    │   ├── DeleteCommand.java
    │   ├── GroupCommand.java
    │   ├── MoveCommand.java
    │   └── UngroupCommand.java
    ├── composite
    │   └── Group.java
    ├── controller
    │   └── Application.java
    ├── Main.java
    ├── model
    │   ├── AsciiPaint.java
    │   ├── Circle.java
    │   ├── ColoredShape.java
    │   ├── Drawing.java
    │   ├── Line.java
    │   ├── Point.java
    │   ├── Rectangle.java
    │   ├── Shape.java
    │   └── Square.java
    └── view
        └── View.java
```

## Design patterns

### MVC

The main design of this project is the Model-View-Controller (MVC) design pattern. Where the model manages the logic of
the application, the view provides user-facing displays and information and the controller manages the interaction
between the model and the view as well as parsing and internal actions.

### Command

The command pattern encapsulates specific actions, here : application commands. Each command is an object managed by an
invoker (here it is the controller) and expected to call a receiver (the model) which provides an action. Thanks to that
pattern, an undo/redo command was created.

### Composite

The composite pattern allows treating leaves and composite objects as one and the same for a specific purpose. Here it
was used to create groups of shapes. Each shape and group of shapes should be treated the same as they are all shapes
but the behavior of a group will have to differ from the one of the shapes. The composite pattern was implemented to
allow both groups and shapes to be used with the same methods. 