package g55803.atl.ascii.model;

/**
 * Provides a Point object to map coordinates.
 *
 * @author Nathan Furnal
 */
public class Point {
    private double x;
    private double y;

    /**
     * Constructs a Point from coordinates.
     *
     * @param x the x coordinate.
     * @param y the y coordinate.
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Constructs a point from another point, using a constructor copy.
     *
     * @param p a point object.
     */
    public Point(Point p) {
        this(p.x, p.y);
    }

    /**
     * Gets the x coordinate of the point.
     *
     * @return the x coordinate.
     */
    public double getX() {
        return x;
    }

    /**
     * Gets the y coordinate of the point.
     *
     * @return the y coordinate.
     */
    public double getY() {
        return y;
    }

    /**
     * Moves a point.
     *
     * @param dx the delta on the x-axis.
     * @param dy the delta on the y-axis.
     */
    public void move(double dx, double dy) {
        x += dx;
        y += dy;
    }

    /**
     * Computes to distance between two points in a 2D plane.
     *
     * @param other the other point.
     * @return the distance between the points.
     * @throws NullPointerException when the other point is <code>null</code>.
     */
    public double distanceTo(Point other) throws NullPointerException {
        if (other == null) {
            throw new NullPointerException("other point cannot be null.");
        }
        return Math.sqrt(Math.pow(other.x - this.x, 2) +
                Math.pow(other.y - this.y, 2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (Double.compare(point.x, x) != 0) return false;
        return Double.compare(point.getY(), getY()) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getX());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getY());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
