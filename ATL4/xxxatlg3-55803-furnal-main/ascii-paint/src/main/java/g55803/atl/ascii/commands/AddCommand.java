package g55803.atl.ascii.commands;

import g55803.atl.ascii.model.AsciiPaint;
import g55803.atl.ascii.model.Shape;

/**
 * Provides a command to add new shapes to the ASCII painting as well as delete them.
 *
 * @author Nathan Furnal
 */
public class AddCommand implements Command {
    private final AsciiPaint receiver;
    private final String[] args;
    private Shape inMemoryShape;

    /**
     * Constructs a command with a receiver and its arguments.
     *
     * @param receiver the receiver that will execute the actions for the command.
     * @param args     the arguments to pass to the receiver.
     */
    public AddCommand(AsciiPaint receiver, String[] args) {
        this.receiver = receiver;
        this.args = args;
    }

    @Override
    public void execute() {
        String shape = args[1];
        int pointX = Integer.parseInt(args[2]); // @srv: on parse dans le controlleur pas dans la commande.
        int pointY = Integer.parseInt(args[3]);
        char color = args[args.length - 1].charAt(0);
        try {
            switch (shape) {
                case "circle" -> receiver.newCircle(pointX, pointY, Double.parseDouble(args[4]), color);

                case "rectangle" -> receiver.newRectangle(pointX, pointY, Double.parseDouble(args[4]),
                        Double.parseDouble(args[5]), color);

                case "square" -> receiver.newSquare(pointX, pointY, Double.parseDouble(args[4]), color);

                case "line" -> {
                    int pointX2 = Integer.parseInt(args[4]);
                    int pointY2 = Integer.parseInt(args[5]);
                    receiver.newLine(pointX, pointY, pointX2, pointY2, color);
                }
                default -> throw new IllegalStateException("Unexpected value: " + shape);
            }
            System.out.println(shape + " added!");
            inMemoryShape = receiver.getLastShape();
        } catch (IllegalStateException e) {
            System.out.println("This shape does not exist! Try a different one among [circle|rectangle|square|line].");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void cancel() {
        if (receiver.removeShape(inMemoryShape)) {
            System.out.println(inMemoryShape + " was removed!"); // pas de sout dans les commandes.
        }
    }
}
