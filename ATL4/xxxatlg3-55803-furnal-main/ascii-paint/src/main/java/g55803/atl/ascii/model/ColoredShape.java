package g55803.atl.ascii.model;

/**
 * Provides an abstract implementation of the shape interface.
 *
 * @author Nathan Furnal
 */
public abstract class ColoredShape implements Shape {
    private char color;

    /**
     * Constructs a shape with a color.
     *
     * @param color the given color.
     */
    public ColoredShape(char color) {
        this.color = color;
    }

    /**
     * Gets the shape color.
     *
     * @return the shape color.
     */
    public char getColor() {
        return color;
    }

    /**
     * Sets the shape color.
     *
     * @param color the shape color.
     */
    public void setColor(char color) {
        this.color = color;
    }
}
