package g55803.atl.ascii.model;

/**
 * Provides a Rectangle object.
 *
 * @author Nathan Furnal
 */
public class Rectangle extends ColoredShape {
    private final Point upperLeft;
    private final double width;
    private final double height;


    /**
     * Constructs a rectangle object.
     *
     * @param upperLeft the upper left point.
     * @param width     the width of the rectangle.
     * @param height    the height of the rectangle.
     * @param color     the color of the rectangle.
     * @throws IllegalArgumentException when the width or the color are negative.
     */
    public Rectangle(Point upperLeft, double width, double height, char color) throws IllegalArgumentException {
        super(color);
        if (width <= 0) {
            throw new IllegalArgumentException("Width should be positive but got : " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("Height should be positive but got : " + height);
        }
        this.upperLeft = new Point(upperLeft.getX(), upperLeft.getY());
        this.width = width;
        this.height = height;
    }

    public void move(double dx, double dy) {
        upperLeft.move(dx, dy);
    }

    public boolean isInside(Point p) {
        Point bottomRight = new Point(upperLeft.getX() + width, upperLeft.getY() - height);
        boolean xCheck = p.getX() > upperLeft.getX() && p.getX() <= bottomRight.getX();
        boolean yCheck = p.getY() < upperLeft.getY() && p.getY() >= bottomRight.getY();
        return xCheck && yCheck;
    }

    /**
     * Gets the upper left point.
     * Only for test purpose.
     *
     * @return a copy of the upper left corner point.
     */
    Point getUpperLeft() {
        return new Point(upperLeft.getX(), upperLeft.getY());
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(getClass().getSimpleName()).append("{");
        s.append("upperLeft=").append(upperLeft);
        if (width == height) {
            s.append(", side=").append(width);
        } else {
            s.append(", width=").append(width).append(", height=").append(height);
        }
        s.append(", color=").append(getColor());
        s.append("}");
        return s.toString();
    }
}
