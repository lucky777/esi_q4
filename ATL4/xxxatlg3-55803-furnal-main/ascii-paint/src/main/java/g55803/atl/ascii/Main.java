package g55803.atl.ascii;

import g55803.atl.ascii.controller.Application;
import g55803.atl.ascii.model.AsciiPaint;
import g55803.atl.ascii.view.View;

/**
 * Main application loop.
 *
 * @author Nathan Furnal
 */
public class Main {
    public static void main(String[] args) {
        AsciiPaint model = new AsciiPaint();
        Application app = new Application(model, new View(model));
        app.start();
    }
}
