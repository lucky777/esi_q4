package g55803.atl.ascii.commands;

import g55803.atl.ascii.model.AsciiPaint;
import g55803.atl.ascii.model.Shape;

/**
 * Provides a command to delete a shape from the painting as well as one to cancel the deletion.
 *
 * @author Nathan Furnal
 */
public class DeleteCommand implements Command {
    private final AsciiPaint receiver;
    private final String[] args;
    private Shape inMemoryShape;

    /**
     * Constructs a command with a receiver and its arguments.
     *
     * @param receiver the receiver that will execute the actions for the command.
     * @param args     the arguments to pass to the receiver.
     */
    public DeleteCommand(AsciiPaint receiver, String[] args) {
        this.receiver = receiver;
        this.args = args;
    }

    @Override
    public void execute() {
        // User numbering starts at 1 but list numbering starts at 0.
        try {
            int shapeIdx = Integer.parseInt(args[1]) - 1;
            inMemoryShape = receiver.getShape(shapeIdx);
            receiver.removeShape(shapeIdx);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("There is no shape at " + args[1]);
        } catch (NumberFormatException e) {
            System.out.println("'delete' expects numbers as arguments, please try again.");
        }
    }

    @Override
    public void cancel() {
        receiver.addShape(inMemoryShape); //@srv tu rajoute la shape � la fin de la la liste du drawing mais elle �tait peut-�tre ailleurs.
    }
}
