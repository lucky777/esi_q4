package g55803.atl.ascii.model;

import g55803.atl.ascii.model.ColoredShape;
import g55803.atl.ascii.model.Point;
import g55803.atl.ascii.model.Shape;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides a way to group shapes together. Grouped shapes behave as one in the sense that they move together, have
 * the same color and are overall considered to be one shape. Groups can be added or ungrouped.
 *
 * @author Nathan Furnal
 */
public class Group extends ColoredShape {
    private final List<Shape> children;

    /**
     * Constructs a group from two shapes, a shape can be a regular shape or another group. A group takes the color
     * of the last (the 2nd) shape added.
     *
     * @param s1 the first shape to group.
     * @param s2 the second shape to group.
     */
    public Group(Shape s1, Shape s2) { //@srv on a envie d'avoir plus de 2 shapes
        super(s2.getColor());
        this.children = new ArrayList<>();
        children.add(s1);
        children.add(s2);
    }

    @Override
    public void move(double dx, double dy) {
        for (Shape s : children) {
            s.move(dx, dy);
        }
    }

    @Override
    public boolean isInside(Point p) {
        for (Shape s : children) {
            if (s.isInside(p))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(this.getClass().getSimpleName()).append("{children=(");
        for (Shape shape : children) {
            s.append(shape.getClass().getSimpleName()).append(" ");
        }
        s.replace(s.length() - 1, s.length(), "");
        s.append(")").append(", color=").append(getColor());
        s.append("}");
        return s.toString();
    }

    /**
     * Gets the children of the group.
     *
     * @return the children of the group.
     */
    public List<Shape> getChildren() { //@srv on adore pas.
        return new ArrayList(children); //@srv: v�rifier que cela fonctionne toujours.
    }

    @Override
    public void setColor(char color) {
        //@srv: tu changes pas la couleur du groupe ? super.setColor(color)
        for (Shape s : children) {
            ((ColoredShape) s).setColor(color);
        }
    }
}
