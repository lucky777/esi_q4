package g55803.atl.ascii.controller;

import g55803.atl.ascii.commands.*;
import g55803.atl.ascii.model.AsciiPaint;
import g55803.atl.ascii.view.View;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Provides an application to interact with the user and draw shapes.
 *
 * @author Nathan Furnal
 */
public class Application {
    private static final String showCommand = "show";
    private static final String addCommand = "add";
    private static final String quitCommand = "quit";
    private static final String exitCommand = "exit";
    private static final String listCommand = "list";
    private static final String moveCommand = "move";
    private static final String deleteCommand = "delete";
    private static final String undoCommand = "undo";
    private static final String redoCommand = "redo";
    private static final String colorCommand = "color";
    private static final String groupCommand = "group";
    private static final String ungroupCommand = "ungroup";
    private final View view;
    private final AsciiPaint model;
    private final Deque<Command> history;
    private Command lastCommand;

    public Application(AsciiPaint model, View view) {
        this.model = model;
        this.view = view;
        this.history = new LinkedList<>();
        this.lastCommand = null;
    }

    /**
     * Reads and parses the user input. The first argument defines the command and the behavior of the method
     * changes accordingly. Either by calling the showing function to display the shapes or add a new shape.
     * There is also an option to quit the program.
     *
     * @param args command line arguments.
     */
    private void parseCalls(String[] args) {
        String command = args[0];
        switch (command) {
            case showCommand -> view.showShapes();
            case addCommand -> parseAddCommand(args);
            case listCommand -> view.listItems();
            case moveCommand -> parseMoveCommand(args);
            case deleteCommand -> parseDeleteCommand(args);
            case colorCommand -> parseColorCommand(args);
            case undoCommand -> undo();
            case redoCommand -> redo();
            case groupCommand -> parseGroupCommand(args);
            case ungroupCommand -> parseUngroupCommand(args);
            case quitCommand, exitCommand -> {
                System.out.println("Thanks for playing, bye!");
                System.exit(0);
            }
            default -> System.out.println("This command is not known. Please try something else.");
        }
    }

    /**
     * Adds actions for the "add" command in the CLI.
     *
     * @param args the string arguments sent to the method.
     */
    private void parseAddCommand(String[] args) {
        AddCommand ac = new AddCommand(model, args);
        ac.execute();
        history.add(ac);
    }

    /**
     * Parses the movements for the shapes.
     *
     * @param args the string arguments sent to the method.
     */
    private void parseMoveCommand(String[] args) {
        MoveCommand mc = new MoveCommand(model, args);
        mc.execute();
        history.add(mc);
    }

    /**
     * Parses the commands to delete a shape.
     *
     * @param args the string arguments sent to the method.
     */
    private void parseDeleteCommand(String[] args) {
        DeleteCommand dc = new DeleteCommand(model, args);
        dc.execute();
        history.add(dc);
    }

    /**
     * Parses the commands to re-color a shape.
     *
     * @param args the string arguments sent to the method.
     */
    private void parseColorCommand(String[] args) {
        ColorCommand cc = new ColorCommand(model, args);
        cc.execute();
        history.add(cc);
    }

    /**
     * Parses the command to group shapes.
     *
     * @param args the string arguments sent to the method.
     */
    private void parseGroupCommand(String[] args) {
        GroupCommand gc = new GroupCommand(model, args);
        gc.execute();
        history.add(gc);
    }

    /**
     * Parses the command to ungroup shapes.
     *
     * @param args the string arguments sent to the method.
     */
    private void parseUngroupCommand(String[] args) {
        UngroupCommand uc = new UngroupCommand(model, args);
        uc.execute();
        history.add(uc);
    }

    /**
     * Undoes the most recent action.
     */
    public void undo() {
        if (history.isEmpty()) {
            System.out.println("Nothing to undo!");
        } else {
            lastCommand = history.removeLast();
            lastCommand.cancel();
        }
    }

    /**
     * Re-does the last action.
     */
    public void redo() {
        if (lastCommand == null) {
            System.out.println("Nothing to redo!");
        } else {
            lastCommand.execute();
            if (history.isEmpty()) {
                lastCommand = null;
            } else {
                lastCommand = history.removeLast();
            }
        }
    }

    /**
     * Main application loop, asks for user input to build a canvas and create shapes.
     * Showing or adding shapes is then available.
     */
    public void start() {
        view.startBanner();
        view.addNewDrawing();
        while (true) {
            String command = view.readUserString("> ");
            String[] commands = view.splitString(command);
            parseCalls(commands);
        }
    }
}
