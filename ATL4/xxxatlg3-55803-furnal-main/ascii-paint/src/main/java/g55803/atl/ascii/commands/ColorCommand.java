package g55803.atl.ascii.commands;

import g55803.atl.ascii.model.AsciiPaint;
import g55803.atl.ascii.model.ColoredShape;

/**
 * Provides a command to manage the colors of shapes and groups in the ASCII painting.
 *
 * @author Nathan Furnal
 */
public class ColorCommand implements Command {
    private final AsciiPaint receiver;
    private final String[] args;
    private ColoredShape inMemoryShape;
    private char inMemoryColor;

    /**
     * Constructs a command with a receiver and its arguments.
     *
     * @param receiver the receiver that will execute the actions for the command.
     * @param args     the arguments to pass to the receiver.
     */
    public ColorCommand(AsciiPaint receiver, String[] args) {
        this.receiver = receiver;
        this.args = args;
    }

    @Override
    public void execute() {
        try {
            int shapeIdx = Integer.parseInt(args[1]);
            char newColor = args[2].charAt(0);
            inMemoryShape = (ColoredShape) receiver.getShape(shapeIdx);
            inMemoryColor = inMemoryShape.getColor();
            inMemoryShape.setColor(newColor);

        } catch (IndexOutOfBoundsException e) {
            System.out.println("There is no shape at " + args[1]);
        } catch (NumberFormatException e) {
            System.out.println("'color' expects numbers as arguments, please try again.");
        }
    }

    @Override
    public void cancel() {
        inMemoryShape.setColor(inMemoryColor);
    }
}
