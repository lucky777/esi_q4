package g55803.atl.ascii.commands;

import g55803.atl.ascii.model.Group;
import g55803.atl.ascii.model.AsciiPaint;
import g55803.atl.ascii.model.Shape;

/**
 * Provides a command to group shapes together in the ASCII painting, groups behave as one, meaning that movements
 * or color modifications apply to all the elements of the group.
 *
 * @author Nathan Furnal
 */
public class GroupCommand implements Command {
    private final AsciiPaint receiver;
    private final String[] args;
    private Group inMemoryShape;

    /**
     * Constructs a command with a receiver and its arguments.
     *
     * @param receiver the receiver that will execute the actions for the command.
     * @param args     the arguments to pass to the receiver.
     */
    public GroupCommand(AsciiPaint receiver, String[] args) {
        this.receiver = receiver;
        this.args = args;
    }

    @Override
    public void execute() {
        try {
            // User numbering starts at 1 but list index starts at 0.
            int shape1Idx = Integer.parseInt(args[1]) - 1;
            int shape2Idx = Integer.parseInt(args[2]) - 1;
            Group group = new Group(receiver.getShape(shape1Idx), receiver.getShape(shape2Idx));
            inMemoryShape = group;
            receiver.removeShape(shape1Idx);
            shape2Idx--;
            receiver.removeShape(shape2Idx);
            receiver.addShape(group);
        } catch (NumberFormatException e) {
            System.out.println("'group' expects numbers as arguments.");
        }
    }

    @Override
    public void cancel() {//@srv ne fonctionnre pas vraiment: les indices sont pas bons.
        if (inMemoryShape == null) {
            System.out.println("There is nothing to ungroup.");
        } else {
            Shape shape1 = inMemoryShape.getChildren().get(0); 
            Shape shape2 = inMemoryShape.getChildren().get(1);
            receiver.removeShape(inMemoryShape);
            receiver.addShape(shape1);
            receiver.addShape(shape2);
        }
    }
}
