package g55803.atl.ascii.view;


import g55803.atl.ascii.model.AsciiPaint;
import g55803.atl.ascii.model.Point;

import java.util.Scanner;

/**
 * @author Nathan Furnal
 */
public class View {
    private final AsciiPaint paint;

    public View(AsciiPaint paint) {
        this.paint = paint;
    }

    /**
     * Creates a string representation of the shapes.
     *
     * @return the string representation for all available shapes.
     */
    private String makeStringRepr() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < paint.getHeight(); i++) {
            for (int j = 0; j < paint.getWidth(); j++) {
                Point p = new Point(j, i);
                if (paint.getShapeAt(p) != null) {
                    s.append(paint.getColor(p));
                } else {
                    s.append(" ");
                }
            }
            s.append("\n");
        }
        return s.toString();
    }

    /**
     * Shows the shapes on the terminal.
     */
    public void showShapes() {
        String repr = makeStringRepr();
        for (int i = 0; i < repr.length(); i++) {
            System.out.printf("%2s", repr.charAt(i));
        }
    }

    /**
     * Reads user input in a robust manner.
     *
     * @param message the message to display to the user.
     * @return the integer entered.
     */
    private int readUserInt(String message) {
        Scanner s = new Scanner(System.in);
        System.out.println(message);
        while (!s.hasNextInt() && s.nextInt() <= 0) {
            s.next();
            System.out.println("Please enter a positive integer.");
            System.out.println(message);
        }
        return s.nextInt();
    }

    /**
     * Reads the string input by the user then turns it lowercase and trim the edges.
     *
     * @param message the message to display to the user.
     * @return the curated string.
     */
    public String readUserString(String message) {
        Scanner s = new Scanner(System.in);
        System.out.printf("%s", message);
        return s.nextLine().toLowerCase().trim();
    }


    /**
     * Splits a string on whitespaces.
     *
     * @param command the string to split.
     * @return the split string.
     */
    public String[] splitString(String command) {
        return command.split(" ");
    }

    /**
     * Displays a multi-line banner for the drawing application start.
     */
    public void startBanner() {
        String banner = """
                    
                Welcome to the Shape builder.
                Please enter the height and width of your canvas.
                When the canvas is setup, you can add a shape with :
                    
                --> [circle|square|rectangle|line] startingPoint [radius|side|(height & width)|otherPoint] color
                    
                You can display the shapes with :
                --> show
                                
                You can show a list of the current shapes with :
                --> list
                                
                You can move a shape by referring its place in the list :
                                
                --> move 1 5 7 (moves the first shape by 5 horizontally and 7 vertically)
                                
                You can group and ungroup shapes, using their numbers in the list.
                                
                --> group 1 2 (groups shape 1 and shape 2 together)
                                
                --> ungroup 2 (ungroups both previously grouped shapes)
                """;
        System.out.println(banner);
    }

    /**
     * Adds a new drawing canvas to the painting.
     */
    public void addNewDrawing() {
        int width = readUserInt("Enter a width : ");
        int height = readUserInt("Enter a height : ");
        paint.newDrawing(width, height);
    }

    /**
     * Lists the items currently held in the drawing.
     */
    public void listItems() {
        StringBuilder items = new StringBuilder();
        int i = 1;
        for (String s : paint.getStringItems()) {
            items.append(i++).append(". ").append(s).append("\n");
        }
        System.out.println(items);
    }
}
