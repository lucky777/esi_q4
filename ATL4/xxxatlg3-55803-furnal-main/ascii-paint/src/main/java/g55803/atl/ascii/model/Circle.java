package g55803.atl.ascii.model;

/**
 * Provides a Circle object.
 *
 * @author Nathan Furnal
 */
public class Circle extends ColoredShape {
    private final Point center;
    private final double radius;

    /**
     * Constructs a circle.
     *
     * @param center the center of the circle.
     * @param radius the radius of the circle.
     * @param color  the color of the circle.
     * @throws IllegalArgumentException when the radius is negative.
     */
    public Circle(Point center, double radius, char color) throws IllegalArgumentException {
        super(color);
        if (radius <= 0) {
            throw new IllegalArgumentException("radius must be positive, received : " + radius);
        }
        this.center = new Point(center.getX(), center.getY());
        this.radius = radius;
    }

    /**
     * Gets the center of the circle.
     *
     * @return a copy of the circle's center.
     */
    public Point getCenter() {
        return new Point(center.getX(), center.getY());
    }

    public void move(double dx, double dy) {
        center.move(dx, dy);
    }

    public boolean isInside(Point p) {
        return center.distanceTo(p) <= radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "center=" + center +
                ", radius=" + radius +
                ", color=" + getColor() +
                '}';
    }
}
