package g55803.atl.ascii.commands;

import g55803.atl.ascii.model.Group;
import g55803.atl.ascii.model.AsciiPaint;
import g55803.atl.ascii.model.Shape;

/**
 * Provides a command to ungroup groups, ungrouped elements revert to their behavior and colors pre-grouping.
 *
 * @author Nathan Furnal
 */
public class UngroupCommand implements Command {
    private final AsciiPaint receiver;
    private final String[] args;
    private Shape s1;
    private Shape s2;

    /**
     * Constructs a command with a receiver and its arguments.
     *
     * @param receiver the receiver that will execute the actions for the command.
     * @param args     the arguments to pass to the receiver.
     */
    public UngroupCommand(AsciiPaint receiver, String[] args) {
        this.receiver = receiver;
        this.args = args;
    }

    @Override
    public void execute() {
        try {
            // User numbering starts at 1 but list index starts at 0.
            int ungroupIdx = Integer.parseInt(args[1]) - 1;
            if (!(receiver.getShape(ungroupIdx).getClass() == Group.class)) {
                System.out.println("Can't ungroup a simple shape");
            } else {
                Group group = (Group) receiver.getShape(ungroupIdx);
                receiver.removeShape(group);
                s1 = group.getChildren().get(0);
                s2 = group.getChildren().get(1);
                receiver.addShape(s1);
                receiver.addShape(s2);
            }
        } catch (NumberFormatException e) {
            System.out.println("'ungroup' expects numbers as arguments.");
        }
    }

    @Override
    public void cancel() {
        if (s1 == null || s2 == null) {
            System.out.println("There is nothing to group back!");
        }
        receiver.removeShape(s1);
        receiver.removeShape(s2);
        receiver.addShape(new Group(s1, s2));
    }
}
