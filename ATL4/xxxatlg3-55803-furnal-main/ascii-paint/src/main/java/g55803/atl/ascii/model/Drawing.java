package g55803.atl.ascii.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides a drawing object which will be the canvas to draw the shapes. Incidentally, it keeps track of
 * the shapes in a list.
 *
 * @author Nathan Furnal
 */
class Drawing {
    private final static int DEFAULT_WIDTH = 50;
    private final static int DEFAULT_HEIGHT = 50;
    private final List<Shape> shapes;
    private final int width;
    private final int height;

    /**
     * Constructs the drawing object and initialises the empty list of shapes.
     *
     * @param width  the width of the canvas.
     * @param height the height of the canvas.
     */
    Drawing(int width, int height) {
        this.width = width;
        this.height = height;
        this.shapes = new ArrayList<>();
    }

    /**
     * Default constructor with default static values of (50, 50).
     */
    Drawing() {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Adds a shape to the shape list.
     *
     * @param shape the shape to add.
     */
    void addShape(Shape shape) {
        shapes.add(shape);
    }

    /**
     * Gets the shape at a given point if it is inside said shape.
     *
     * @param p the point to check out.
     * @return the shape if the point lies within and <code>null</code> otherwise.
     */
    Shape getShapeAt(Point p) {
        for (Shape shape : shapes) {
            if (shape.isInside(p))
                return shape;
        }
        return null;
    }

    /**
     * Gets the width of the drawing.
     *
     * @return the width of the drawing.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the drawing.
     *
     * @return the height of the drawing.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Builds a list of the string representation of the shapes.
     *
     * @return a list of string representation of the shapes.
     */
    List<String> getStringItems() {
        List<String> names = new ArrayList<>();
        for (Shape s : shapes) {
            names.add(s.toString());
        }
        return names;
    }

    /**
     * Moves a shape.
     *
     * @param idx the index of the shape in the shapes list.
     * @param dx  the horizontal delta.
     * @param dy  the vertical delta.
     */
    void moveShape(int idx, double dx, double dy) {
        shapes.get(idx).move(dx, dy);
    }

    /**
     * Removes a shape from the shapes list.
     *
     * @param idx the index of the shape to remove;
     */
    void removeShape(int idx) {
        shapes.remove(idx);
    }

    /**
     * Gets the last shape of the shapes list.
     */
    Shape getLastShape() {
        return shapes.get(shapes.size() - 1);
    }


    /**
     * Removes a shape from the shapes list.
     *
     * @param shape the shape to remove.
     * @return true if the shape was removed and false otherwise.
     */
    boolean remove(Shape shape) {
        return shapes.remove(shape);
    }

    /**
     * Gets shape at the given index from the shapes list.
     *
     * @param idx index to get the shape at.
     * @return the shape at the given index.
     */
    Shape get(int idx) {
        return shapes.get(idx);
    }

    /**
     * Insert a shape at a given index of the shape list.
     *
     * @param idx   index to pick insert the shape.
     * @param shape the shape to insert.
     */
    void insertAt(int idx, Shape shape) {
        shapes.add(idx, shape);
    }
}
