package g55803.atl.ascii.model;

/**
 * Provides a Line shape. Most of the equality checks are converted to inequality checks with a tolerance threshold
 * to ensure a correct behavior with floats.
 *
 * @author Nathan Furnal
 */
public class Line extends ColoredShape {
    private final Point p1;
    private final Point p2;
    private final static double THRESHOLD = 1e-3;

    /**
     * Constructs a colored line shape which is defined by two distinct points. If the point are not distinct,
     * an exception is thrown.
     *
     * @param p1    the first defining point of the line.
     * @param p2    the second defining point of the line.
     * @param color the color of the line.
     */
    public Line(Point p1, Point p2, char color) {
        super(color);
        if (p1.equals(p2)) {
            throw new IllegalArgumentException("A line should be defined by two distinct points.");
        }
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
    public void move(double dx, double dy) {
        p1.move(dx, dy);
        p2.move(dx, dy);
    }

    @Override
    public boolean isInside(Point p) {
        if (isVertical()) {
            return Math.abs(p.getX() - p1.getX()) < THRESHOLD;
        }
        double slope = -slope(p1, p2);
        double numerator = Math.abs(slope * p.getX() - p.getY() - slope * p1.getX() + p2.getY());
        double denominator = Math.sqrt((slope * slope) + 1);
        return (numerator / denominator) < THRESHOLD;
    }

    /**
     * Computes the slope of the line.
     *
     * @param p1 the first point of the line.
     * @param p2 the second point of the line.
     * @return the slope.
     */
    private double slope(Point p1, Point p2) {
        if (Math.abs(p1.getY() - p2.getY()) < THRESHOLD) {
            return 0.;
        }
        return (p2.getY() - p1.getY()) / (p2.getX() - p1.getX());
    }

    /**
     * Checks if the line is vertical. A line is considered vertical if it goes through two points with the
     * same abscissas.
     *
     * @return true if the line is vertical and false otherwise.
     */
    private boolean isVertical() {
        return Math.abs(p1.getX() - p2.getX()) < THRESHOLD;
    }

    @Override
    public String toString() {
        return "Line{" +
                "p1=" + p1 +
                ", p2=" + p2 +
                '}';
    }
}
