package g55803.atl.ascii.model;

import java.util.List;

/**
 * Provides an AsciiPaint object. It interacts heavily with the {@code Drawing} to add shapes
 * to the canvas and render them to the terminal with ascii characters.
 *
 * @author Nathan Furnal
 */
public class AsciiPaint {

    private Drawing drawing;

    /**
     * Constructs the Ascii paint.
     *
     * @param width  the width to pass to the drawing.
     * @param height the height to pass to the height.
     */
    public AsciiPaint(int width, int height) {
        this.drawing = new Drawing(width, height);
    }

    /**
     * Default constructor with no values being set.
     */
    public AsciiPaint() {
        this.drawing = new Drawing();
    }

    /**
     * Adds a new circle to the drawing's list of shapes.
     *
     * @param x      the x coordinate for the center.
     * @param y      the y coordinate for the center.
     * @param radius the circle radius.
     * @param color  the circle color.
     */
    public void newCircle(int x, int y, double radius, char color) {
        if (radius <= 0) {
            throw new IllegalArgumentException("radius must be positive, received : " + radius);
        }
        drawing.addShape(new Circle(new Point(x, y), radius, color));
    }

    /**
     * Adds a new rectangle to the drawing's list of shapes.
     *
     * @param x      the x coordinate for the upper left corner.
     * @param y      the y coordinate for the upper left corner.
     * @param width  the width of the rectangle.
     * @param height the height of the rectangle.
     * @param color  the color of the rectangle.
     */
    public void newRectangle(int x, int y, double width, double height, char color) {
        if (width <= 0) {
            throw new IllegalArgumentException("Width should be positive but got : " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("Height should be positive but got : " + height);
        }
        drawing.addShape(new Rectangle(new Point(x, y), width, height, color));
    }

    /**
     * Adds a new square to the drawing's list of shapes.
     *
     * @param x     the x coordinate for the upper left corner.
     * @param y     the y coordinate for the upper left corner.
     * @param side  the length of the square.
     * @param color the color of the square.
     */
    public void newSquare(int x, int y, double side, char color) {
        if (side <= 0) {
            throw new IllegalArgumentException("Side length should be positive : " + side);
        }
        drawing.addShape(new Square(new Point(x, y), side, color));
    }

    /**
     * Adds a new line to the drawing's list of shapes.
     *
     * @param x1    the abscissa of the first point on the line.
     * @param y1    the ordinate of the first point on the line.
     * @param x2    the abscissa of the second point on the line.
     * @param y2    the ordinate of the second point on the line.
     * @param color the color of the line.
     */
    public void newLine(int x1, int y1, int x2, int y2, char color) {
        drawing.addShape(new Line(new Point(x1, y1), new Point(x2, y2), color));
    }

    /**
     * Gets the width of the canvas.
     *
     * @return the width of the drawing.
     */
    public int getWidth() {
        return drawing.getWidth();
    }

    /**
     * Gets the height of the canvas.
     *
     * @return the height of the canvas.
     */
    public int getHeight() {
        return drawing.getHeight();
    }

    /**
     * Gets the shape at the given point.
     *
     * @param p the point to get the shape at.
     * @return the shape at the given point and <code>null</code> if there aren't any.
     */
    public Shape getShapeAt(Point p) {
        return drawing.getShapeAt(p);
    }

    /**
     * Gets the shape color at the given point.
     *
     * @param p the point to get the color at.
     * @return the color of the shape at the given point and <code>null</code> otherwise.
     */
    public char getColor(Point p) {
        return getShapeAt(p).getColor();
    }


    /**
     * Sets the drawing (canvas) of the painting.
     *
     * @param width  the width of the canvas.
     * @param height the height of the canvas.
     */
    public void newDrawing(int width, int height) {
        if (width <= 0) {
            throw new IllegalArgumentException("Width should be positive but got : " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("Height should be positive but got : " + height);
        }
        drawing = new Drawing(width, height);
    }

    /**
     * Gets the shapes held by the drawing and return them as a list of string representation.
     *
     * @return a list of the string representations of the current shapes.
     */
    public List<String> getStringItems() {
        return drawing.getStringItems();
    }

    /**
     * Moves a shape.
     *
     * @param idx the index of the shape in the list.
     * @param dx  the horizontal delta.
     * @param dy  the vertical delta.
     */
    public void moveShape(int idx, double dx, double dy) {
        drawing.moveShape(idx, dx, dy);
    }

    /**
     * Removes a shape from the shapes list.
     *
     * @param idx the index of the shape to remove;
     */
    public void removeShape(int idx) {
        drawing.removeShape(idx);
    }

    /**
     * Gets the most recent shape.
     */
    public Shape getLastShape() {
        return drawing.getLastShape();
    }


    /**
     * Removes a shape from the drawing.
     *
     * @param shape the shape to remove.
     * @return true if the shape was removed and false otherwise.
     */
    public boolean removeShape(Shape shape) {
        return drawing.remove(shape);
    }

    /**
     * Gets the shape at a given index.
     *
     * @param idx the index to get the shape at.
     * @return the shape at the index.
     */
    public Shape getShape(int idx) {
        return drawing.get(idx);
    }

    /**
     * Adds a shape to the drawing.
     *
     * @param shape the shape to add.
     */
    public void addShape(Shape shape) {
        drawing.addShape(shape);
    }
}

