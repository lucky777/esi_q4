package g55803.atl.ascii.model;

/**
 * Provides a shape interface, it enforces two simple contracts.
 *
 * @author Nathan Furnal
 */
public interface Shape {
    /**
     * Moves a shape using a reference point.
     *
     * @param dx the delta on the x-axis.
     * @param dy the delta on the y-axis.
     */
    void move(double dx, double dy);

    /**
     * Checks if a point is inside a shape.
     *
     * @param p the point to check.
     * @return true if the point is inside the shape and false otherwise.
     */
    boolean isInside(Point p);


    /**
     * Gets the shape's color.
     *
     * @return the shape's color.
     */
    char getColor();
}
