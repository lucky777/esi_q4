package g55803.atl.ascii.model;

/**
 * Provides a square object which extends the rectangle.
 *
 * @author Nathan Furnal
 */
public class Square extends Rectangle {
    /**
     * Constructs a square using the {@code Rectangle} constructor.
     *
     * @param upperLeft the upper left point.
     * @param side      the length of the square's side.
     * @param color     the square color.
     */
    public Square(Point upperLeft, double side, char color) {
        super(upperLeft, side, side, color);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
