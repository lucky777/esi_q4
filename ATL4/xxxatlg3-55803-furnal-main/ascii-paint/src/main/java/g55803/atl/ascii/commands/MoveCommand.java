package g55803.atl.ascii.commands;

import g55803.atl.ascii.model.AsciiPaint;

/**
 * Provides a command to move a painting within the painting and move it back in its place.
 *
 * @author Nathan Furnal
 */
public class MoveCommand implements Command {
    private final AsciiPaint receiver;
    private final String[] args;

    /**
     * Constructs a command with a receiver and its arguments.
     *
     * @param receiver the receiver that will execute the actions for the command.
     * @param args     the arguments to pass to the receiver.
     */
    public MoveCommand(AsciiPaint receiver, String[] args) {
        this.receiver = receiver;
        this.args = args;
    }

    @Override
    public void execute() {
        // User numbering starts at 1 but list numbering starts at 0.
        try {
            int shapeIdx = Integer.parseInt(args[1]) - 1;
            double dx = Double.parseDouble(args[2]);
            double dy = Double.parseDouble(args[3]);
            receiver.moveShape(shapeIdx, dx, dy);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("There is no shape at " + args[1]);
        } catch (NumberFormatException e) {
            System.out.println("'move' expects numbers as arguments, please try again.");
        }
    }

    @Override
    public void cancel() {
        // User numbering starts at 1 but list numbering starts at 0.
        try {
            int shapeIdx = Integer.parseInt(args[1]) - 1;
            double dx = -Double.parseDouble(args[2]);
            double dy = -Double.parseDouble(args[3]);
            receiver.moveShape(shapeIdx, dx, dy);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("There is no shape at " + args[1]);
        } catch (NumberFormatException e) {
            System.out.println("'move' expects numbers as arguments, please try again.");
        }
    }
}
