package g55803.atl.ascii.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Nathan Furnal
 */
class CircleTest {

    @Test
    void test_construction_negative_radius() {
        double x = 1;
        double y = 2;
        Point p = new Point(x, y);
        double radius = -1;
        char color = 'c';
        assertThrows(IllegalArgumentException.class,
                () -> new Circle(p, radius, color));
    }

    @Test
    void test_move() {
        double x = 1;
        double y = 2;
        double dx = 3;
        double dy = 4;
        Point p1 = new Point(x, y);
        double radius = 3;
        char color = 'c';
        Circle circle = new Circle(p1, radius, color);
        circle.move(dx, dy);
        assertEquals(circle.getCenter(), new Point(x + dx, y + dy));
    }

    @Test
    void test_isInside_inside() {
        double x_c = 5;
        double y_c = 1;
        Point center = new Point(x_c, y_c);
        double radius = 3;
        char color = 'c';
        double x_p = 4;
        double y_p = 2;
        Point p = new Point(x_p, y_p);
        Circle c = new Circle(center, radius, color);
        assertTrue(c.isInside(p));
    }

    @Test
    void test_isInside_border() {
        double x_c = 0;
        double y_c = 0;
        Point center = new Point(x_c, y_c);
        double radius = 1;
        char color = 'c';
        double x_p = 0;
        double y_p = 1;
        Point p = new Point(x_p, y_p);
        Circle c = new Circle(center, radius, color);
        assertTrue(c.isInside(p));
    }

    @Test
    void test_isInside_outside() {
        double x_c = 0;
        double y_c = 0;
        Point center = new Point(x_c, y_c);
        double radius = 1;
        char color = 'c';
        double x_p = 2;
        double y_p = 2;
        Point p = new Point(x_p, y_p);
        Circle c = new Circle(center, radius, color);
        assertFalse(c.isInside(p));
    }
}