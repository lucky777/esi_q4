package g55803.atl.ascii.model;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Nathan Furnal
 */
class RectangleTest {

    @Test
    void test_Construction() {
        double x = 1;
        double y = 1;
        Point p = new Point(x, y);
        double width = -1;
        double height = -3;
        double positiveVal = 10;
        char color = 'c';
        assertAll("Negative width or height should fail.",
                () -> assertThrows(IllegalArgumentException.class,
                        () -> new Rectangle(p, width, positiveVal, color)),
                () -> assertThrows(IllegalArgumentException.class,
                        () -> new Rectangle(p, positiveVal, height, color)));
    }

    @Test
    void test_move() {
        double x = 2;
        double y = 5;
        double dx = 3;
        double dy = 4;
        Point p = new Point(x, y);
        double width = 7;
        double height = 11;
        char color = 'c';
        Rectangle r = new Rectangle(p, width, height, color);
        r.move(dx, dy);
        assertEquals(r.getUpperLeft(), new Point(x + dx, y + dy));
    }

    @Test
    void test_isInside_inside() {
        double x = 0;
        double y = 0;
        Point p = new Point(x, y);
        double width = 10;
        double height = 5;
        char color = 'c';
        Rectangle r = new Rectangle(p, width, height, color);
        Point testPoint = new Point(3, -2);
        assertTrue(r.isInside(testPoint));
    }

    @Test
    void test_isInside_border() {
        double x = 0;
        double y = 0;
        Point p = new Point(x, y);
        double width = 10;
        double height = 5;
        char color = 'c';
        Rectangle r = new Rectangle(p, width, height, color);
        Point testPoint = new Point(0, -4);
        assertFalse(r.isInside(testPoint));
    }

    @Test
    void test_isInside_outside() {
        double x = 1;
        double y = 1;
        Point p = new Point(x, y);
        double width = 4;
        double height = 2;
        char color = 'c';
        Rectangle r = new Rectangle(p, width, height, color);
        Point testPoint = new Point(10, 10);
        assertFalse(r.isInside(testPoint));
    }

    @Test
    void test_all_inside() {
        double x = 10;
        double y = 5;
        Point p = new Point(x, y);
        double width = 7;
        double height = 4;
        char color = 'r';
        double eps = 1e-6;
        Rectangle r = new Rectangle(p, width, height, color);
        assertAll("All points inside a rectangle return true",
                () -> {
                    for (double i = x + eps; i < x + width; i++) {
                        for (double j = y - eps; j > y - height; j--) {
                            Point pp = new Point(i, j);
                            assertTrue(r.isInside(pp));
                        }
                    }
                });
    }

    @Test
    void test_all_outside() {
        double x = 0;
        double y = 0;
        Point upperLeftRectangle = new Point(x, y);
        double width = 5;
        double height = 3;
        char color = 'r';
        Rectangle rectangle = new Rectangle(upperLeftRectangle, width, height, color);
        List<Point> points = List.of(new Point(-1, 0), new Point(3, 1), new Point(6, 0),
                new Point(6, 2), new Point(5, 4), new Point(3, 5), new Point(0, 4));
        assertAll("All points are outside the rectangle",
                () -> {
                    for (Point pp : points) {
                        assertFalse(rectangle.isInside(pp));
                    }
                });
    }
}