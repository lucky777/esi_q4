package g55803.atl.ascii.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Nathan Furnal
 */
class AsciiPaintTest {
    AsciiPaint paint = new AsciiPaint(30, 30);

    @BeforeEach
    void setUp() {
        paint.newDrawing(paint.getWidth(), paint.getHeight());
    }

    @Test
    void newCircle() {
        int x = 3;
        int y = 3;
        double radius = 5;
        char color = 'o';
        paint.newCircle(x, y, radius, color);
        assertEquals(color, paint.getShapeAt(new Point(x, y)).getColor());
    }

    @Test
    void newRectangle() {
        int x = 10;
        int y = 10;
        double width = 7;
        double height = 4;
        char color = 'r';
        double eps = 1e-6;
        paint.newRectangle(x, y, width, height, color);
        assertEquals(color, paint.getShapeAt(new Point(x + eps, y - eps)).getColor());
    }

    @Test
    void newSquare() {
        int x = 5;
        int y = 3;
        double side = 7;
        char color = 's';
        double eps = 1e-6;
        paint.newSquare(x, y, side, color);
        assertEquals(color, paint.getShapeAt(new Point(x + eps, y - eps)).getColor());
    }

    @Test
    void getWidth() {
        int w = 100;
        int h = 50;
        paint.newDrawing(w, h);
        assertEquals(w, paint.getWidth());
    }

    @Test
    void getHeight() {
        int w = 100;
        int h = 50;
        paint.newDrawing(w, h);
        assertEquals(h, paint.getHeight());
    }

    @Test
    void getShapeAt() {
        double eps = 1e-6;
        int xC = 1;
        int yC = 1;
        double radius = 5;
        char colorC = 'o';
        paint.newCircle(xC, yC, radius, colorC);
        int xS = 20;
        int yS = 20;
        double side = 7;
        char colorS = 's';
        paint.newSquare(xS, yS, side, colorS);
        int xR = 10;
        int yR = 10;
        double width = 7;
        double height = 4;
        char colorR = 'r';
        paint.newRectangle(xR, yR, width, height, colorR);
        assertAll("Gets the current shape at the given points",
                () -> {
                    assertEquals(colorC, paint.getShapeAt(new Point(xC, yC)).getColor());
                    assertEquals(colorS, paint.getShapeAt(new Point(xS + eps, yS - eps)).getColor());
                    assertEquals(colorR, paint.getShapeAt(new Point(xR + eps, yR - eps)).getColor());
                });
    }

    @Test
    void getColor() {
        int xC = 1;
        int yC = 1;
        double radius = 5;
        char colorC = 'w';
        paint.newCircle(xC, yC, radius, colorC);
        assertEquals(colorC, paint.getShapeAt(new Point(xC, yC)).getColor());
    }

    @Test
    void newDrawing() {
        int xInit = 20;
        int yInit = 20;
        int xAfter = 33;
        int yAfter = 33;
        AsciiPaint paint = new AsciiPaint(xInit, yInit);
        assertAll("Checks if the drawing is updated",
                () -> {
                    assertEquals(xInit, paint.getWidth());
                    assertEquals(yInit, paint.getHeight());
                    paint.newDrawing(xAfter, yAfter);
                    assertEquals(xAfter, paint.getWidth());
                    assertEquals(yAfter, paint.getHeight());
                });
    }
}