package g55803.atl.ascii.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Nathan Furnal
 */
class PointTest {

    @Test
    void test_getX() {
        double x = 11;
        double y = 22;
        Point p = new Point(x, y);
        assertEquals(x, p.getX(), 1e-6);
    }

    @Test
    void test_getY() {
        double x = 11;
        double y = 22;
        Point p = new Point(x, y);
        assertEquals(y, p.getY(), 1e-6);
    }

    @Test
    void test_getLocation() {
        double x = 1;
        double y = 3;
        Point p = new Point(x, y);
        assertAll("Check both x and y",
                () -> assertEquals(x, p.getX(), 1e-6),
                () -> assertEquals(y, p.getY(), 1e-6)
        );
    }

    @Test
    void test_move() {
        double x = 1;
        double dx = 4;
        double y = 3;
        double dy = 7;
        Point p1 = new Point(x, y);
        p1.move(dx, dy);
        Point p2 = new Point(x + dx, y + dy);
        assertEquals(p1, p2);
    }

    @Test
    void test_distanceTo() {
        double x1 = 0;
        double x2 = 3;
        double y1 = 0;
        double y2 = 4;
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        double expected = 5;
        assertEquals(0, Double.compare(p1.distanceTo(p2), expected));
    }

    @Test
    void test_distance_reciprocity() {
        double x1 = 3;
        double x2 = 2;
        double y1 = 1;
        double y2 = 7;
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        assertEquals(0, Double.compare(p1.distanceTo(p2), p2.distanceTo(p1)));
    }
}