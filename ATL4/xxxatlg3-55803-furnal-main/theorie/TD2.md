# Question 1

1.
```
(3.0, 6.0) - FF0000FF
x: 3.0
color : FF0000FF
```

2. Que la méthode `getColor` n'existe pas. Ce qui est normal puisqu'on déclare un type `Point`,
qui ne possède pas cette méthode. On a pas de problème si on supprime la ligne qui appelle cette méthode,
car les méthodes utilisées existent dans la classe `Point`, on a une référence implicite vers `super`. 

3. Non, car on utilise un type explicite `ColoredPoint` avec le constructeur de `Point`, ce qui provoque
une incompatibilité puisque les membres attendus ne correspondent pas. 

4. Non car on essaie de faire référence à des attributs qui sont privés et déclarés dans `Point`.

5. Qu'on a un problème d'héritage cyclique. 

6. Qu'on ne peut pas hériter d'une classe "finale". 

# Question 2

1. Oui on peut puisque `Object` est l'ancêtre de toutes les classes, rien ne s'oppose à déclarer un type objet. 
2. Oui pour la même raison que la question précédente. 
3. Oui car c'est une méthode héritée par défaut de `Object`. 
4. Fait.

# Question 3

1. Que l'appel à la méthode `super` doit être la première instruction du constructeur. 
2. Que les types d'arguments et nombre d'arguments attendus ne sont pas corrects. Cette ligne sert
à appeler le constructeur de `Point`. 
3. Non parce qu'on fournit un constructeur par défaut. 
4. Fait. 

# Question 4
1.
```
constructor of A
constructor of B
constructor of C
```
2.
```
constructor of A
constructor of B
```
3. Confirmé.

4. `public Object(){}`

5. Fait.

# Question 6

1. 
```
(0.0, 0.0) - not pinned
(1.0, 1.0) - pinned
``` 

2. Celle de `Point` car le type déclaré est `Point`. 
3. Que la méthode de la classe parent ne lance pas d'exception, cette méthode ne peut pas être _override_.
4. Plus d'erreur. Car on a utilisé une exception de **runtime** et non **checked**. Les exceptions de **runtime** et les erreurs
ne sont pas vérifiées par le compilateur, on ne doit pas les déclarer. Tandis que les **checked** exceptions doivent être déclarées. Voir [les exceptions](https://docs.oracle.com/javase/tutorial/essential/exceptions/catchOrDeclare.html).
5. Pas d'erreur pour la raison mentionnée au-dessus.
6. Pas d'erreur en changeant le type de retour. 
7. Oui, les types ne sont pas compatibles car on `return this`, qui doit être au moins un `Point`. 
8. Oui car la méthode est originellement publique et on ne peut pas utiliser l'héritage pour diminuer la visibilité, seulement pour l'élargir.
9. L'appel utilise la méthode `move` de la classe parent. 
10. Fait.

# Question 6 

1. 
```
(1.0, 1.0)
(3.0, 5.0) - FF0000FF
(2.0, 2.0) - not pinned
```

# Question 7

1. Ce n'est pas permis. 
2. Utiliser `public` est permis.
3.
```
(1.0, 1.0)
(3.0, 5.0) - FF0000FF
(2.0, 2.0) - not pinned
```


# Commentaires 

La différence dans l'usage du _declared type_ vs _actual type_ est un bon 
rappel, je ne me souvenais pas bien des conséquences. 

Je ne savais pas que l'utilisation de `throws` dans une méthode enfant demandait que la 
méthode parent lance aussi l'exception.

Je ne connaissais pas bien la différence entre les différents types d'exceptions (runtime vs checked). 

Je ne savais pas que l'héritage de la visibilité empêchait de diminuer la visibilité.