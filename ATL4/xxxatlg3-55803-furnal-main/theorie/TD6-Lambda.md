# Syntaxe des expressions lambda

1. Expressions acceptées :

L'expression 3. est acceptée car les types sont inférés. Lorsqu'on a une seule valeur de retour sans opérations intermédiaires,
on peut se débarasser du `return`, des accolades ainsi que du point-virgule tout en même temps ; mais pas par étapes intermédiaires.

De la même le 4) est accepté, le 6 est refusé car les parenthèses sont nécessaires
lorsque le type est précisé où qu'il y a plusieurs paramètres. Le 9) et 10) sont permis.

# Interfaces fonctionnelles standards

2. Exemples : 

i) 

```java
public record Polygon(int nSides) {
}

public class TestIntPredicate {
    public static void main(String[] args) {
        List<Polygon> arr = new ArrayList<>(List.of
                (new Polygon(3), new Polygon(10), new Polygon(5), new Polygon(2)));
        IntPredicate lessThan5 = x -> x < 5;
        List<Polygon> res = arr.stream().filter(p -> lessThan5.test(p.nSides())).toList();
        System.out.println(res);
    }
}
```

ii) 

```java
public record Person(String name, int age) {
}

public class TestBiPredicate {
    public static void main(String[] args) {
        List<Person> plist = new ArrayList<>(List.of
                (new Person("John", 25),
                        new Person("Mary", 30),
                        new Person("Jack", 57),
                        new Person("Nina", 17)));

        BiPredicate<String, Integer> nameAndAgeCheck = (n, a) -> n.charAt(0) == 'J' && a > 30;

        List<Person> res = plist.stream().filter(p -> nameAndAgeCheck.test(p.name(), p.age())).toList();
        System.out.println(res);
    }
}
```

iii) 

```java
public record Circle(double radius) {
}

public class TestDoubleConsumer {
    public static void main(String[] args) {
        List<Circle> cList = new ArrayList<>(List.of(new Circle(1.1),
                new Circle(3.4),
                new Circle(7.2)));
        DoubleConsumer showRadius = r -> System.out.println("The radius is " + r);
        cList.forEach(c -> showRadius.accept(c.radius()));
    }
}
```

iv)

```java
public class TestToIntFunc {
    public static void main(String[] args) {
        List<Circle> cList = new ArrayList<>(List.of(new Circle(1.1),
                new Circle(3.4),
                new Circle(7.2)));
        ToIntFunction<Double> roundDouble = d -> d - d.intValue() > .5 ? d.intValue() + 1 : d.intValue();
        List<Integer> rounded = cList.stream().map(c -> roundDouble.applyAsInt(c.radius())).toList();
        System.out.println(rounded);
    }
}
```

v)

```java
public class TestDoubleFunction {
    public static void main(String[] args) {
        List<Circle> cList = new ArrayList<>(List.of(new Circle(1.1),
                new Circle(3.4),
                new Circle(7.2)));
        DoubleFunction<String> converter = d -> {
            String s = Double.toString(d);
            s = "The number is " + s;
            return s;
        };
        List<String> out = cList.stream().map(c -> converter.apply(c.radius())).toList();
        System.out.println(out);
    }
}
```

vi) 

```java
public class TestIntToDoubleFunc {
    public static void main(String[] args) {
        IntToDoubleFunction halving = i -> i * .5;
        List<Person> plist = new ArrayList<>(List.of
                (new Person("John", 25),
                        new Person("Mary", 30),
                        new Person("Jack", 57),
                        new Person("Nina", 17)));
        List<Double> halfAges = plist.stream().map(Person::age).map(halving::applyAsDouble).toList();
        System.out.println(halfAges);
    }
}
```
