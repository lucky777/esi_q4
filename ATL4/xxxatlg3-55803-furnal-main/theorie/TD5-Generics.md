# Classes génériques

1. Une erreur de type, la classe s'attend à recevoir un int et reçoit un double.

2. Un avertissement de boxing non-nécessaire.

# Héritage et génériques

3. Non, car on ne peut pas convertir d'un type `Integer` vers un type `Object`.
4. Une erreur de types incompatibles.

5. Il n'y a pas d'erreurs à proprement parler mais le compilateur se plaint de boxing non nécessaire.

# Jokers

La première ligne implique que même si `Integer` est un sous-type de `Object`, une collection de `Object` comme une
liste ne permet pas forcément de contenir un sous-type comme un `Integer`. Plus particulièrement, comme on peut attendre
n'importe quel sous-type dans une collection d'objets, on risquerait une erreur de type puisque qu'une collection doit
être homogène.

6. Ligne 12, une erreur de type.

# Bornes supérieures et inférieures

7. Qu'il n'y a pas de méthode `compareTo` pour les types donnés.
8. Que le type `Number` n'est pas dans les bornes du type T. Comme `Number`
   peut être sous-classé par `Double` ou `Integer`, on aurait un problème de type en permettant l'évaluation de ce code.
9. Exemple :
```java
List<Number> arr = new ArrayList<>(List.of(1, 2, 3, 4));
List<Double> other = new ArrayList<>(List.of(1.1, 2.2, 3.3, 4.4));
Collections.copy(arr, other); // arr -> [1.1, 2.2, 3.3, 4.4];
```
10. Exemple :
```java
List<Integer> arr = new ArrayList<>(List.of(1, 3, 11, 5, 8));
Comparator<Number> cmp = new Comparator<>(){
    @Override
    public int compare(Number a, Number b){
        return a.intValue() - b.intValue();
        }
}

Collections.sort(arr, cmp);
```