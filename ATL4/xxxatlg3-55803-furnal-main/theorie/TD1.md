# Question 1 

1. Il affiche un point en (0, 0) et puis en (2, 2).
 
2. Que la classe `TestPoint` est publique et qu'il faut la déclarer dans un fichier `TestPoint.java`.

3. Si `TestPoint` est toujours publique, alors la même erreur qu'au point précédent. 
   Si aucune des deux classes n'est publique, pas d'erreur.

4. Fait. 

# Question 2

1. Que `x` est en accès privé dans la classe `Point`. 
2. Le programme fonctionne et affiche la ligne `méthode move(int, int)` avant le mouvement. 
3. Que la méthode `move(double, double)` est déjà définie dans la classe. 
4. Fait. 

# Question 3

1. Que l'appel `this()` doit être la première instruction dans le corps du constructeur.
2. Le compilateur attend la signature `double, double` mais ne trouve pas d'arguments correspondant à un constructeur sans arguments. 
On obtient pas d'erreur car sans constructeur défini, le constructeur par défaut met les attributs à zéro dans le cas des int/double. Le code affiche les positions (0, 0) et puis (2, 2)
3. Le code affiche les positions (10, 10) et puis (12, 12) après le mouvement.
4. Fait.

# Question 4

1. Le programme affiche : 
```
Circle : [(0.0, 0.0), 5.0]
Circle : [(2.0, 5.0), 5.0]
Circle : [(2.0, 5.0), 10.0] 
```
2. Il existe une seule instance de le classe `Point`. 

# Question 5

1. Le programme affiche : 
```
Circle : [(0.0, 0.0), 5.0]
Circle : [(2.0, 5.0), 5.0]
Circle : [(0.0, 0.0), 5.0]
```
2. On a deux instances de la classe `Point`, p et p2. Une seule instance de la classe `Circle`, c. 
3. Le programme affiche : 
```
Circle : [(0.0, 0.0), 5.0]
Circle : [(0.0, 0.0), 5.0]
Circle : [(-2.0, -5.0), 5.0]
```
4. Le programme affiche : 
```
Circle : [(0.0, 0.0), 5.0]
Circle : [(0.0, 0.0), 5.0]
Circle : [(0.0, 0.0), 5.0]
```

5. On a trois instances de la classe `Point`: p, celle créé par le constructeur du Cercle et p2. On a toujours qu'une instance du cercle. 
p est l'instance créée par le constructeur de `Point`, p2 est l'instance créée comme output de `getCenter`. L'instance de `center` est crée à la construction de l'objet cercle. 

# Question 6

1. Le programme affiche : 
```
Rectangle : [(0.0, 0.0), (5.0, 3.0)]
perimeter: 16.0
Rectangle : [(2.0, 5.0), (7.0, 8.0)]
perimeter: 16.0
```

2. Sans copies défensives, ce sont toujours les mêmes deux instances de points créées au début de la méthode. 
3. L'invariant n'est pas respecté, le programme affiche : 
```
Rectangle : [(0.0, 0.0), (5.0, 3.0)]
perimeter: 16.0
Rectangle : [(12.0, 15.0), (7.0, 8.0)]
perimeter: -24.0
```
Le programme affiche cette valeur car on a déplacé le point directement référencé par le rectangle. 
4. Fait. 