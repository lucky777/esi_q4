# Question 1

1. La taille de la fenêtre de démarrage de l'application change. 
2. Cela donne une fenêtre sans fond ni décoration autour. Les valeurs possibles sont `DECORATED`, 
`UNDECORATED`, `TRANSPARENT` et `UTILITY`. 
3. Le texte se déplace dans la fenêtre : en haut à gauche, en bas à gauche, en haut à gauche, en haut à droite.

# Question 2

1. Dans le premier cas, on ne peut que sélectionner ou pas. Dans le second, la case commence
commence comme indéterminée et puis sélectionnée ou pas. Dans le dernier cas, on a une possibilité
de rotation entre les trois états (sélectionné, non-sélectionné et indéterminé).
2. La première case et la troisième case se retrouvent respectivement en haut à gauche et en haut
à droite. 

# Question 3

1. Les valeurs du texte sont cachées.
2. Le programme demande de donner un nom d'utilisateur et puis affiche "User name saved! You can't change it".

# Question 4

1. Lorsqu'on appuie sur le bouton "Print", le texte de la région est affichée dans le terminal.

# Question 5

1. Elle retourne une liste de nœuds enfants qui peuvent être "écoutés" lorsqu'ils sont modifiés.
Le type de retour est `ObservableList<Node>`.

# Question 6 

1. Fait. 
2. Fait.
3. Cela peut avoir un impact au niveau du texte qui dépasse du cadre qui lui est associé. 
Il existe des méthodes `setClip` et `getClip` pour gérer cette notion.

# Question 7

1. Les composantes d'une même cellule sont empilées. 
2. Le texte "Password" est maintenant aligné au centre avec le reste du texte.
3. L'espace pour entrer le mot de passe prend maintenant toute la largeur disponible. 


