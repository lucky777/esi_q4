module nathan.project.myjavafx {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    opens nathan.project.myjavafx to javafx.fxml;
    exports nathan.project.myjavafx.hello;
    exports nathan.project.myjavafx.layout;
    opens nathan.project.myjavafx.hello to javafx.fxml;
}