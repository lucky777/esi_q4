package nathan.project.myjavafx.hello;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class HelloWorldTextFieldAction
        extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("My First JavaFX App");
        BorderPane root = new BorderPane();

        Label userName = new Label("User Name");
        Label test = new Label("User name saved! You can't change it");

        TextField tfdUserName = new TextField();
        tfdUserName.setAlignment(Pos.CENTER_LEFT);
        tfdUserName.setOnAction(event -> {
            root.setBottom(test);
            BorderPane.setAlignment(test, Pos.CENTER);
            tfdUserName.setEditable(false);
            tfdUserName.setAlignment(Pos.CENTER);
        });

        //Alignment
        root.setTop(userName);
        BorderPane.setAlignment(userName, Pos.CENTER);
        root.setCenter(tfdUserName);

        Scene scene = new Scene(root, 300, 100);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

