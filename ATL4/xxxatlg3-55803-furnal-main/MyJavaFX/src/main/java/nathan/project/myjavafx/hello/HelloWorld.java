package nathan.project.myjavafx.hello;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HelloWorld
        extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public void init(){
        System.out.println("Initialisation de l'écran.");
    }
    public void stop(){
        System.out.println("Fin de l'écran.");
    }
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("My very awesome APP");
        primaryStage.initStyle(StageStyle.TRANSPARENT);

        BorderPane root = new BorderPane();
        Text helloText = new Text("Hello World");
        helloText.setFont(Font.font("Roboto Mono", 20));
        helloText.setFill(Color.RED);
        root.setCenter(helloText);
        Scene scene = new Scene(root, 250, 100);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

