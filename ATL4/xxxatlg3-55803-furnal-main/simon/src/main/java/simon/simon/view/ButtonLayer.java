package simon.simon.view;

import javafx.animation.PauseTransition;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Provides a button layer for the Simon game. Button layout and access happens here.
 *
 * @author Nathan Furnal
 */
public class ButtonLayer extends GridPane {
    private final List<Color> colors;
    private final int nButtons;
    private final List<Button> buttons;
    private final int nCols;
    private final int nRows;
    /**
     * Default sound value to be played.
     */
    private final int baseSoundValue = 69;

    /**
     * Default note velocity.
     */
    private final int noteVelocity = 80;
    private final HashMap<Color, Button> colorButtonHashMap;
    private final HashMap<Button, Color> buttonColorHashMap;


    /**
     * Constructs a button layer with a list of colors, a number of buttons as well as columns and rows. As it
     * extends a grid pane, the rows and columns are needed to set the buttons' places. Maps of (button, color) and
     * (color, button) pairs are also created, which helps to update behavior based on specific buttons/colors.
     *
     * @param colors   the color list for the buttons.
     * @param nButtons the number of buttons.
     * @param nCols    the number of columns.
     * @param nRows    the number of rows.
     * @throws IllegalArgumentException when there is an odd number of buttons or an unequal number of buttons and
     *                                  colors. This is mostly to avoid incorrect layouts.
     */
    public ButtonLayer(List<Color> colors, int nButtons, int nCols, int nRows) {
        super();
        if (nButtons % 2 != 0 || nButtons < 4) {
            throw new IllegalArgumentException("Pick an even number of buttons over 4.");
        }
        if (colors.size() != nButtons) {
            throw new IllegalArgumentException("Mismatch between the number of colors and buttons.");
        }
        this.colors = colors;
        this.nButtons = nButtons;
        this.buttons = new ArrayList<>();
        this.nCols = nCols;
        this.nRows = nRows;
        this.colorButtonHashMap = new HashMap<>();
        this.buttonColorHashMap = new HashMap<>();
    }

    /**
     * Default constructor with sane default values.
     */
    public ButtonLayer() {
        this(List.of(Color.FORESTGREEN, Color.ORANGERED, Color.GOLDENROD, Color.ROYALBLUE), 4, 2, 2);
    }

    /**
     * Creates the correct number of buttons. Each button takes the full allowed space. A list of buttons also exists
     * to keep track of the existing buttons.
     */
    private void makeButtons() {
        for (int i = 0; i < nButtons / 2; i++) {
            for (int j = 0; j < nButtons / 2; j++) {
                Button btn = new Button();
                btn.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                btn.setBorder(new Border(new BorderStroke(Color.BLACK,
                        BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.THICK)));
                this.add(btn, j, i);
                buttons.add(btn);
            }
        }
    }

    /**
     * Sets the color of each button according to the color list.
     */
    private void makeButtonColors() {
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setBackground(
                    new Background(new BackgroundFill(colors.get(i), CornerRadii.EMPTY, Insets.EMPTY)));
            colorButtonHashMap.put(colors.get(i), buttons.get(i));
            buttonColorHashMap.put(buttons.get(i), colors.get(i));
        }
    }

    /**
     * Sets the column constraints in percent to match the number of columns.
     */
    private void makeColumnConstraints() {
        double percentage = (double) 100 / nCols;
        for (int i = 0; i < nCols; i++) {
            var colConstraint = new ColumnConstraints();
            colConstraint.setPercentWidth(percentage);
            this.getColumnConstraints().add(colConstraint);
        }
    }

    /**
     * Sets the row constraints in percent to match the number of rows.
     */
    private void makeRowConstraints() {
        double percentage = (double) 100 / nRows;
        for (int i = 0; i < nRows; i++) {
            var rowConstraint = new RowConstraints();
            rowConstraint.setPercentHeight(percentage);
            this.getRowConstraints().add(rowConstraint);
        }
    }

    /**
     * Sets the glowing effect of a button when it's pressed.
     */
    void setButtonGlowEffect() {
        ColorAdjust glowEffect = new ColorAdjust();
        glowEffect.setBrightness(.4);
        for (int i = 0; i < buttons.size(); i++) {
            int idx = i;
            buttons.get(i).setOnAction(e -> {
                buttons.get(idx).setEffect(glowEffect);
                var pause = new PauseTransition(Duration.seconds(1));
                pause.setOnFinished(event2 -> buttons.get(idx).setEffect(null));
                pause.play();
            });
        }
    }

    /**
     * Sets the glowing effect of a button being pressed as well as a sound.
     */
    void setButtonGlowAndSoundEffect() {
        ColorAdjust glowEffect = new ColorAdjust();
        glowEffect.setBrightness(.4);
        try {
            var synth = MidiSystem.getSynthesizer();
            synth.open();
            var channel = synth.getChannels()[0]; // Gets Piano channel
            for (int i = 0; i < buttons.size(); i++) {
                int idx = i;
                buttons.get(i).setOnAction(e -> {
                    buttons.get(idx).setEffect(glowEffect);
                    channel.noteOn(baseSoundValue + idx * 2, noteVelocity);
                    var pause = new PauseTransition(Duration.seconds(1));
                    pause.setOnFinished(event -> {
                        buttons.get(idx).setEffect(null);
                        channel.noteOff(baseSoundValue + idx * 2);
                    });
                    pause.play();
                });
            }
        } catch (MidiUnavailableException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Puts all the setting up together.
     */
    void setup() {
        makeButtons();
        makeButtonColors();
        makeColumnConstraints();
        makeRowConstraints();
    }

    /**
     * Gets the list of the buttons' colors.
     *
     * @return the buttons' colors.
     */
    public List<Color> getColors() {
        return colors;
    }

    /**
     * Gets the existing buttons.
     *
     * @return a list of existing buttons.
     */
    public List<Button> getButtons() {
        return buttons;
    }

    /**
     * Gets the current default value for the sound being played on button press.
     *
     * @return the default sound value.
     */
    int getBaseSoundValue() {
        return baseSoundValue;
    }

    /**
     * Gets the current default note velocity for the note being played on button press.
     *
     * @return the default note velocity.
     */
    int getNoteVelocity() {
        return noteVelocity;
    }

    /**
     * Gets a map of each color with its corresponding button.
     *
     * @return the color -> button map.
     */
    HashMap<Color, Button> getColorButtonHashMap() {
        return colorButtonHashMap;
    }

    /**
     * Gets the map of each button with its corresponding color.
     *
     * @return the button -> color map.
     */
    public HashMap<Button, Color> getButtonColorHashMap() {
        return buttonColorHashMap;
    }
}
