package simon.simon.util;

/**
 * Provides an enumeration of states for the different steps of the game.
 *
 * @author Nathan Furnal
 */
public enum State {

    /**
     * State of a game when it hasn't started or when it has been restarted after an error.
     */
    NOT_STARTED,
    /**
     * State of the game after the 'play' button has been pressed.
     */
    PLAY,
    /**
     * State of the game when the 'longest' button has been pressed.
     */
    LONGEST,
    /**
     * State of the game when the 'last' button has been pressed.
     */
    LAST,
    /**
     * State of the game for any error, specific error management happens between the controller and the model.
     */
    ERROR,
    /**
     * State of the game requiring an update as it informs the current sequence has been completed.
     */
    SEQ_ENDED,
    /**
     * Placeholder state of the game after a correct answer has been entered.
     */
    ANSWER_ENTERED,
}
