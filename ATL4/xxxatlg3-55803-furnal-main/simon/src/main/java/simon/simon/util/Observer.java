package simon.simon.util;

/**
 * @author Nathan Furnal
 */
public interface Observer {
    /**
     * Updates the observer's behavior given the state of an observable.
     */
    void update();
}
