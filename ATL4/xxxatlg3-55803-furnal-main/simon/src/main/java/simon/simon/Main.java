package simon.simon;

import javafx.application.Application;
import javafx.stage.Stage;
import simon.simon.controller.Controller;
import simon.simon.model.Model;

/**
 * Main entry point for the game, it extends a JavaFX application and starts up the game.
 *
 * @author Nathan Furnal
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Simon Game");
        Model model = new Model();
        Controller controller = new Controller(model, primaryStage);
    }
}
