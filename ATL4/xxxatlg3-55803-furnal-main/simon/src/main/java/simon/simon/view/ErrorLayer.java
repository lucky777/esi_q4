package simon.simon.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Provides a user facing error layer for the Simon game. This layer consists mostly of utilities to write error
 * messages.
 *
 * @author Nathan Furnal
 */
public class ErrorLayer extends VBox {
    private String errorMessage;
    private Text errorText;
    private Button errorButton;
    private final int fontSize = 20;
    private final int sharedRadius = 10;
    private final Font fontSetup = Font.font("System", FontWeight.BOLD, fontSize + 10);
    private final Border borderSetup = new Border(
            new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(sharedRadius),
                    BorderStroke.MEDIUM));

    /**
     * Constructs an invisible error layer with an error message.
     *
     * @param message the error message.
     */
    public ErrorLayer(String message) {
        super();
        this.errorMessage = message;
        this.setVisible(false);
    }

    /**
     * Default constructor with a default message.
     */
    public ErrorLayer() {
        this("Something went wrong!");
    }

    /**
     * Sets the error message and update the text displayed on the layer.
     *
     * @param errorMessage the new error message.
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        errorText.setText(errorMessage);
    }

    /**
     * Styles the layers.
     */
    private void styling() {
        this.setBorder(borderSetup);
        this.setAlignment(Pos.CENTER);
        int radiusOffset = 2;
        this.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(sharedRadius + radiusOffset),
                Insets.EMPTY)));
        this.setPadding(new Insets(fontSize, fontSize / 2., fontSize, fontSize / 2.));
        this.setSpacing(fontSize * 2);
    }

    /**
     * Styles the error button.
     */
    private void makeErrorButton() {
        int radiusOffset = 2;
        int sharedPrefWidth = 300;
        Button error = new Button("Restart");
        error.setFont(fontSetup);
        error.setTextFill(Color.GHOSTWHITE);
        error.setBorder(borderSetup);
        error.setBackground(new Background(new BackgroundFill(Color.CORAL,
                new CornerRadii(sharedRadius + radiusOffset), Insets.EMPTY)));
        error.setPrefWidth(sharedPrefWidth);
        this.errorButton = error;
        this.getChildren().add(errorButton);
    }

    /**
     * Sets up the layer.
     */
    void setup() {
        styling();
        Text errorText = new Text(errorMessage);
        this.errorText = errorText;
        errorText.setFont(fontSetup);
        errorText.setFill(Color.INDIANRED);
        this.getChildren().add(this.errorText);
        makeErrorButton();
    }

    /**
     * Gets the error button. Used to define its behavior in the view.
     *
     * @return the unique button of the error layer.
     */
    public Button getErrorButton() {
        return errorButton;
    }
}