package simon.simon.model;

import javafx.scene.paint.Color;
import simon.simon.util.Observable;
import simon.simon.util.Observer;
import simon.simon.util.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides the model of the Simon game. Its main role as an observable is to notify the view of any state change.
 *
 * @author Nathan Furnal
 */
public class Model implements Observable {
    private final List<Observer> observerList;
    private State state;

    private int counter;
    private final List<Color> gameSequence;
    private final List<Color> playerSequence;
    private List<Color> lastSequence;
    private List<Color> longestSequence;

    /**
     * Constructs the model with a list of observers to notify as well as : a list to keep track of the game's color
     * sequence, a list to keep track of the user's color sequence and lists to keep track of the longest and last
     * sequences played. There is also an internal counter to keep track of each move played by the user.
     */
    public Model() {
        this.observerList = new ArrayList<>();
        state = State.NOT_STARTED;
        this.gameSequence = new ArrayList<>();
        this.playerSequence = new ArrayList<>();
        this.lastSequence = new ArrayList<>();
        this.longestSequence = new ArrayList<>();
        this.counter = 0;
    }

    /**
     * Starts the game and notifies the observers.
     */
    public void start() {
        state = State.PLAY;
        notify(observerList);
    }

    /**
     * Plays the longest game sequence and notifies the observers.
     */
    public void longest() {
        state = State.LONGEST;
        notify(observerList);
    }

    /**
     * Plays the last game sequence and notifies the observers.
     */
    public void last() {
        state = State.LAST;
        notify(observerList);
    }

    /**
     * Reacts to a click from the user and notifies the observers.
     */
    public void click() {
        state = State.ANSWER_ENTERED;
        notify(observerList);
    }

    /**
     * Updates the state when a sequence as been played and needs to be updated and notifies the observers.
     */
    public void next() {
        state = State.SEQ_ENDED;
        notify(observerList);
    }

    /**
     * Updates the state for error management and notifies the observers.
     */
    public void error() {
        state = State.ERROR;
        notify(observerList);
    }

    /**
     * Updates the state for game ending or clearing purposes and notifies the observers.
     */
    public void clear() {
        state = State.NOT_STARTED;
        notify(observerList);
    }

    @Override
    public void subscribe(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void unsubscribe(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notify(List<Observer> observerList) {
        for (Observer observer : observerList) {
            observer.update();
        }
    }

    /**
     * Gets the current game state.
     *
     * @return the game state.
     */
    public State getState() {
        return state;
    }

    /**
     * Gets the current game sequence.
     *
     * @return the game sequence.
     */
    public List<Color> getGameSequence() {
        return gameSequence;
    }

    /**
     * Gets the current player's sequence.
     *
     * @return the player's sequence.
     */
    public List<Color> getPlayerSequence() {
        return playerSequence;
    }

    /**
     * Gets the last sequence played.
     *
     * @return the last sequence.
     */
    public List<Color> getLastSequence() {
        return lastSequence;
    }

    /**
     * Gets the longest sequence played in a series of games.
     *
     * @return the longest sequence.
     */
    public List<Color> getLongestSequence() {
        return longestSequence;
    }

    /**
     * Sets the longest sequence.
     *
     * @param longestSequence the sequence to be updated to.
     */
    public void setLongestSequence(List<Color> longestSequence) {
        this.longestSequence = new ArrayList<>(longestSequence);
    }

    /**
     * Sets the last sequence.
     *
     * @param lastSequence the sequence to be updated to.
     */
    public void setLastSequence(List<Color> lastSequence) {
        this.lastSequence = new ArrayList<>(lastSequence);
    }

    /**
     * Gets the current counter.
     *
     * @return the counter.
     */
    public int getCounter() {
        return counter;
    }

    /**
     * Sets the internal counter to a given integer.
     *
     * @param counter the new value of the counter.
     */
    public void setCounter(int counter) {
        this.counter = counter;
    }

    /**
     * Increments the counter by one.
     */
    public void incrementCounter() {
        counter++;
    }
}
