package simon.simon.view;

import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import simon.simon.controller.Controller;
import simon.simon.model.Model;
import simon.simon.util.Observer;
import simon.simon.util.State;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Provides a view for the Simon game. This layer ties all the other layer together and manages the interactions
 * with the controller and the model. As an observer, it also reacts to the state change of the model which is the
 * only observable for now.
 *
 * @author Nathan Furnal
 */
public class View extends StackPane implements Observer {

    /**
     * Default resizing ratio.
     */
    private final double sizeRatio = .5;
    private final Model model;
    private final Controller controller;
    private final ButtonLayer buttonLayer;
    private final StartLayer startLayer;
    private final ErrorLayer errorLayer;

    /**
     * Constructs the view with the model and the controller. It also subscribes the view to the model's observers' list.
     * Each layer is added to the view and setup with the proper styling as well as dynamic resizing. Notably, the button
     * layer is disabled on startup to avoid mis-clicks from messing with the game behavior.
     *
     * @param controller the game controller.
     * @param model      the game model.
     */
    public View(Controller controller, Model model) {
        this.controller = controller;
        this.model = model;
        model.subscribe(this);
        this.buttonLayer = new ButtonLayer();
        this.startLayer = new StartLayer();
        this.errorLayer = new ErrorLayer();
        this.heightProperty().addListener((obs, oldVal, newVal) -> startLayer.maxHeightProperty().bind(this.heightProperty().multiply(sizeRatio)));
        this.widthProperty().addListener((obs, oldVal, newVal) -> startLayer.maxWidthProperty().bind(this.widthProperty().multiply(sizeRatio)));
        this.heightProperty().addListener((obs, oldVal, newVal) -> errorLayer.maxHeightProperty().bind(this.heightProperty().multiply(sizeRatio)));
        this.widthProperty().addListener((obs, oldVal, newVal) -> errorLayer.maxWidthProperty().bind(this.widthProperty().multiply(sizeRatio)));
        this.getChildren().addAll(buttonLayer, startLayer, errorLayer);
        buttonLayer.setup();
        startLayer.setup();
        errorLayer.setup();
        setup();
        buttonLayer.setDisable(true);
    }

    /**
     * Gets the button layer of the view.
     *
     * @return the button layer.
     */
    public ButtonLayer getButtonLayer() {
        return buttonLayer;
    }

    /**
     * Gets the error layer of the view.
     *
     * @return the error layer.
     */
    public ErrorLayer getErrorLayer() {
        return errorLayer;
    }

    /**
     * Updates the view based on the model state. Most of the work is done on ensuring proper visibility
     * for the different layers as well as playing the correct sequences to the user. The button is also disabled
     * as much as possible to avoid mis-clicks or game issues.
     */
    @Override
    public void update() {
        switch (model.getState()) {

            case NOT_STARTED -> {
                buttonLayer.setDisable(true);
                startLayer.setVisible(true);
            }
            case PLAY -> {
                startLayer.setVisible(false);
                makeButtonLayerWithOrWithSilent();
                playSequence(model.getGameSequence());
            }

            case SEQ_ENDED -> playSequence(model.getGameSequence());

            case LAST -> {
                startLayer.setVisible(false);
                buttonLayer.setDisable(true);
                playSequence(model.getLastSequence());
            }

            case LONGEST -> {
                startLayer.setVisible(false);
                buttonLayer.setDisable(true);
                playSequence(model.getLongestSequence());
            }

            case ERROR -> {
                buttonLayer.setDisable(true);
                errorLayer.setVisible(true);
            }
        }
    }

    /**
     * Converts the current view to a JavaFX scene for display.
     *
     * @return a new 1000 x 1000 scene.
     */
    public Scene toScene() {
        int HEIGHT = 1000;
        int WIDTH = 1000;
        Scene scene = new Scene(this, WIDTH, HEIGHT);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/main.css")).toString());
        return scene;
    }

    /**
     * Show the stage passed as parameter, its scene is the view.
     *
     * @param stage a stage to display the view on.
     */
    public void show(Stage stage) {
        stage.setScene(toScene());
        stage.show();
    }

    /**
     * Gets the starting layer of the view.
     *
     * @return the starting layer.
     */
    private StartLayer getStartLayer() {
        return startLayer;
    }

    /**
     * Checks if the silent option has been selected.
     *
     * @return true if the silent checkbox is selected and false otherwise.
     */
    private boolean isSilent() {
        return getStartLayer().getSilentCheckBox().isSelected();
    }

    /**
     * Builds the button layer with button effects and sounds if they are enabled.
     */
    private void makeButtonLayerWithOrWithSilent() {
        if (isSilent()) {
            getButtonLayer().setButtonGlowEffect();
        } else {
            getButtonLayer().setButtonGlowAndSoundEffect();
        }
    }

    /**
     * Gets the slider speed value. It's required to set the difficulty.
     *
     * @return the slider value.
     */
    private double getSliderValue() {
        return getStartLayer().getSpeedSlider().getValue();
    }

    /**
     * Lights up a button for a given amount of time with sound as an option.
     *
     * @param button a given button.
     */
    private void lightButtonAndSound(Button button) {
        int idx = getButtonLayer().getButtons().indexOf(button);
        double waitTime = 1 / getSliderValue();
        ColorAdjust glowEffect = new ColorAdjust();
        glowEffect.setBrightness(.4);
        if (!isSilent()) {
            try {
                int soundVal = getButtonLayer().getBaseSoundValue();
                int noteVelocity = getButtonLayer().getNoteVelocity();
                var synth = MidiSystem.getSynthesizer();
                synth.open();
                var channel = synth.getChannels()[0];
                button.setEffect(glowEffect);
                channel.noteOn(soundVal + idx * 2, noteVelocity);
                var pause = new PauseTransition(Duration.seconds(waitTime));
                pause.setOnFinished(event -> {
                    button.setEffect(null);
                    channel.noteOff(soundVal + idx * 2);
                });
                pause.play();
            } catch (MidiUnavailableException ex) {
                ex.printStackTrace();
            }
        } else {
            button.setEffect(glowEffect);
            var pause = new PauseTransition(Duration.seconds(waitTime));
            pause.setOnFinished(event -> button.setEffect(null));
            pause.play();
        }
    }

    /**
     * Plays a sequence of colors. Each color matches to a button which will light up and optionally make sound.
     *
     * @param colors the color list.
     */
    private void playSequence(List<Color> colors) {
        if (colors.size() == 0) {
            return;
        }
        buttonLayer.setDisable(true);
        // Adding a bit of leeway to allow animation to be seen (+.2)
        Duration duration = Duration.seconds(1 / getSliderValue() + .2);
        HashMap<Color, Button> colorButtonHashMap = getButtonLayer().getColorButtonHashMap();
        var colorItem = colors.iterator();
        var timeline = new Timeline(new KeyFrame(
                duration,
                event -> lightButtonAndSound(colorButtonHashMap.get(colorItem.next()))
        ));
        timeline.setCycleCount(colors.size());
        timeline.setOnFinished(e ->
        {
            buttonLayer.setDisable(false);
            if (model.getState() == State.LONGEST || model.getState() == State.LAST) {
                if (!startLayer.isVisible()) {
                    startLayer.setVisible(true);
                    buttonLayer.setDisable(true);
                }
            }
        });
        timeline.play();
    }

    /**
     * Sets up the layer to provide specific interactions for each button.
     */
    private void setup() {
        startLayer.getStartButton().setOnAction(e -> controller.start());
        startLayer.getLongestButton().setOnAction(e -> controller.longest());
        startLayer.getLastButton().setOnAction(e -> controller.last());
        for (Button button : getButtonLayer().getButtons()) {
            button.setOnMouseClicked(e -> controller.click(button));
        }
        errorManagement();
    }

    /**
     * Sets up an error button which calls the clearing function of the controller when needed. It also hides the error
     * layer.
     */
    private void errorManagement() {
        errorLayer.getErrorButton().setOnAction(e -> {
            errorLayer.setVisible(false);
            controller.clear();
        });
    }
}
