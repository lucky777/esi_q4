package simon.simon.controller;

import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import simon.simon.model.Model;
import simon.simon.view.View;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Provides a controller for the Simon game. The controller manages state between the View and the Model.
 *
 * @author Nathan Furnal
 */
public class Controller {
    private final Model model;
    private final View view;
    private Timer timer;

    /**
     * Constructs a controller with a model and a JavaFX Stage passed as parameter. A view also wraps the controller
     * and the model. A timer is also set for later use.
     *
     * @param model the model.
     * @param stage the JavaFX stage.
     */
    public Controller(Model model, Stage stage) {
        this.model = model;
        this.view = new View(this, model);
        view.show(stage);
        this.timer = new Timer();
    }

    /**
     * Starts the game by adding a random color to the game sequence and then send the start signal to the model.
     */
    public void start() {
        addRandomColorToGameSequence();
        model.start();
    }

    /**
     * Sends a state change to the model, to play the longest sequence unless there is none. In which case
     * it will show an error to the user.
     */
    public void longest() {
        if (model.getLongestSequence().size() == 0) {
            view.getErrorLayer().setErrorMessage("No longest sequence to play.");
            model.error();
        }
        model.longest();
    }

    /**
     * Sends a state change to the model, to play the last sequence unless there is none. In which cas
     * it will show an error to the user.
     */
    public void last() {
        if (model.getLastSequence().size() == 0) {
            view.getErrorLayer().setErrorMessage("No last sequence to play.");
            model.error();
        }
        model.last();
    }

    /**
     * Manages most of the game logic through the model. When a game button is clicked, the controller will add
     * said button's color to the list of played colors. If the color is erroneous compared to the game sequence,
     * the user is shown an error. If the color is correct then an internal counter is incremented and the state of the
     * model changes to reflect the action. If the color is correct as well as the entire sequence, the sequence to be
     * played is updated and the timer is reset.
     *
     * @param button the pressed button.
     */
    public void click(Button button) {
        model.getPlayerSequence().add(view.getButtonLayer().getButtonColorHashMap().get(button));
        if (!model.getPlayerSequence().get(model.getCounter()).equals(model.getGameSequence().get(model.getCounter()))) {
            model.error();
        } else {
            if (model.getPlayerSequence().size() == model.getGameSequence().size() &&
                    model.getPlayerSequence().equals(model.getGameSequence())) {
                if (model.getGameSequence().size() > 1) {
                    timer.cancel();
                    timer = new Timer();
                }
                model.setCounter(0);
                model.getPlayerSequence().clear();
                next();
                model.setLastSequence(model.getGameSequence());
            } else {
                model.incrementCounter();
                model.click();
            }
        }
    }

    /**
     * Adds a random color to the game sequence and changes the model's state. The timer is updated to reflect
     * the insertion of a new color in the sequence.
     */
    private void next() {
        addRandomColorToGameSequence();
        model.next();
        var timerTask = new TimerTask() {
            @Override
            public void run() {
                timeOver();
            }
        };
        timer.schedule(timerTask, model.getGameSequence().size() * 2000L);
    }

    /**
     * Provides a specific error message when the time is over and updates the state of the model.
     */
    public void timeOver() {
        view.getErrorLayer().setErrorMessage("You're out of time!");
        model.error();
    }

    /**
     * Provides a random color among the existing button colors in the game.
     *
     * @return a random color picked among the button colors.
     */
    private Color randomColor() {
        Random rng = new Random();
        int idx = rng.nextInt(view.getButtonLayer().getColors().size());
        return view.getButtonLayer().getColors().get(idx);
    }


    /**
     * Clears the game and the relevant variables when there is an error and sends a state update to the model.
     */
    public void clear() {
        if (model.getGameSequence().size() > model.getLongestSequence().size()) {
            model.setLongestSequence(model.getGameSequence());
        }
        model.getPlayerSequence().clear();
        model.getGameSequence().clear();
        model.setCounter(0);
        model.clear();
    }

    /**
     * Adds a random color to the current game sequence.
     */
    private void addRandomColorToGameSequence() {
        model.getGameSequence().add(randomColor());
    }
}
