package simon.simon.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Provides the starting layer of the Simon game. It mostly consists of user facing buttons/sliders/boxes to define
 * the game's behavior.It consists of a vertical box holding a horizontal box.
 * The longest/start/last buttons are held in the horizontal box. The speed slider, horizontal box and silent checkbox
 * are held within the vertical box.
 *
 * @author Nathan Furnal
 */
public class StartLayer extends VBox {
    /**
     * Some default values are defined below, they're constants used for consistant graphical display.
     */
    private final int spacing = 20;
    private final int commonWidth = 150;
    private final int sharedRadius = 10;
    private final int radiusOffset = 2;
    private final Font fontSetup = Font.font("System", FontWeight.BOLD, spacing);
    private final Border borderSetup = new Border(
            new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(sharedRadius),
                    BorderStroke.MEDIUM));
    private final HBox horizontalBox;
    private Button longestButton;
    private Button startButton;
    private Button lastButton;
    private Slider speedSlider;
    private CheckBox silentCheckBox;

    /**
     * Constructs the starting layer with a horizontal box within.
     */
    public StartLayer() {
        super();
        this.horizontalBox = new HBox(spacing);
    }

    /**
     * Makes each button within the horizontal box, with similar styling.
     */
    private void makeHorizontalButtons() {
        Color fontColor = Color.GHOSTWHITE;
        Background lightBack = new Background(
                new BackgroundFill(Color.DARKGOLDENROD, new CornerRadii(sharedRadius + radiusOffset), Insets.EMPTY));
        Background darkBack = new Background(
                new BackgroundFill(Color.DARKRED, new CornerRadii(sharedRadius + radiusOffset), Insets.EMPTY));
        Button longest = new Button("Longest");
        Button start = new Button("Start");
        Button last = new Button("Last");
        //// Longest button setup
        longest.setFont(fontSetup);
        longest.setTextFill(fontColor);
        longest.setBorder(borderSetup);
        longest.setBackground(lightBack);
        longest.setPrefWidth(commonWidth);
        this.longestButton = longest;
        //// Start button setup
        start.setFont(fontSetup);
        start.setTextFill(fontColor);
        start.setBorder(borderSetup);
        start.setBackground(darkBack);
        start.setPrefWidth(commonWidth);
        this.startButton = start;
        //// Last button setup
        last.setFont(fontSetup);
        last.setTextFill(fontColor);
        last.setBorder(borderSetup);
        last.setBackground(lightBack);
        last.setPrefWidth(commonWidth);
        this.lastButton = last;
        horizontalBox.getChildren().addAll(longest, start, last);
        this.getChildren().add(horizontalBox);
    }

    /**
     * Makes the game title.
     */
    private void makeTitle() {
        Font fontSetup = Font.font("System", FontWeight.BOLD, spacing * 2);
        Label title = new Label("Simon");
        title.setFont(fontSetup);
        this.getChildren().add(title);
    }

    /**
     * Makes the game slider to pick the game speed.
     */
    private void makeSpeedSlider() {
        this.speedSlider = new Slider(0.5, 2, 1.);
        Label sliderLabel = new Label("Speed");
        sliderLabel.setPadding(new Insets(-spacing * 2, 0, 0, 0));
        sliderLabel.setFont(Font.font("System", FontWeight.BOLD, spacing - spacing / 5.));
        double steps = (speedSlider.getMax() - speedSlider.getMin()) / 10;
        speedSlider.setBlockIncrement(steps);
        speedSlider.setShowTickLabels(true);
        speedSlider.setShowTickMarks(true);
        speedSlider.setMajorTickUnit(.5);
        speedSlider.setMaxWidth(commonWidth * 2);
        this.getChildren().addAll(speedSlider, sliderLabel);
    }

    /**
     * Makes the silent checkbox.
     */
    private void makeSilentCheckBox() {
        this.silentCheckBox = new CheckBox();
        silentCheckBox.setText("Silent mode");
        this.getChildren().add(silentCheckBox);
    }

    /**
     * Styles the layer.
     */
    private void styling() {
        this.setBorder(borderSetup);
        this.setAlignment(Pos.CENTER);
        horizontalBox.setAlignment(Pos.CENTER);
        this.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(sharedRadius + radiusOffset),
                Insets.EMPTY)));
        this.setPadding(new Insets(spacing, spacing / 2., spacing, spacing / 2.));
        this.setSpacing(spacing * 2);
    }

    /**
     * Gets the speed slider.
     *
     * @return the speed slider.
     */
    Slider getSpeedSlider() {
        return speedSlider;
    }

    /**
     * Gets the silent option checkbox.
     *
     * @return the silent option checkbox.
     */
    CheckBox getSilentCheckBox() {
        return silentCheckBox;
    }

    /**
     * Gets the "longest" button.
     *
     * @return the left most button.
     */
    Button getLongestButton() {
        return longestButton;
    }

    /**
     * Gets the "start" button.
     *
     * @return the center button.
     */
    Button getStartButton() {
        return startButton;
    }

    /**
     * Gets the "last" button.
     *
     * @return the right most button.
     */
    Button getLastButton() {
        return lastButton;
    }

    /**
     * Sets up all the layer styles.
     */
    void setup() {
        makeTitle();
        makeSpeedSlider();
        makeHorizontalButtons();
        makeSilentCheckBox();
        styling();
    }
}
