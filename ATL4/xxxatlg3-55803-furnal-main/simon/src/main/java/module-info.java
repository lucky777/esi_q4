module simon.simon {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires java.desktop;

    opens simon.simon to javafx.fxml;
    exports simon.simon;
}