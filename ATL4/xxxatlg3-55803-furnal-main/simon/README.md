# Introduction 

This repository holds the code for the [game called 'Simon'](https://en.wikipedia.org/wiki/Simon_(game)).

Basically, a sequence of colors becoming increasingly larger is played to the player. The player has to remember the 
sequence and play it back in the same order. Every time a sequence is played correctly, a new color is added to it. 
Then the player has to play it back again and so on, until the player makes a mistake. 

# Code structure

The code consists of the following structure :

```
└── simon
    ├── controller
    │     └── Controller.java
    ├── Main.java
    ├── model
    │     └── Model.java
    ├── util
    │     ├── Observable.java
    │     ├── Observer.java
    │     └── State.java
    └── view
        ├── ButtonLayer.java
        ├── ErrorLayer.java
        ├── StartLayer.java
        └── View.java
```

## `Main`

The `Main.java` class is the entry point for the game, it only extends a JavaFX Application and starts up the game.

## Model

The model mainly consists of state changes and notifications ot the observer (the view). 

## View 

The view consists of three layers : 

1. The button layer which setups up the colored buttons. 
2. The startup layer which provides interactions to start the game with some options (silent, difficulty) as well
replay some sequences.
3. The error layer which is invisible on startup and appears to notify the user of errors such as game errors or when
the player is out of time.

The view ties those layer together, the `View` class interacts with the controller and the model to update the 
game behavior and respond to button presses. 

## Controller 

The controller manages the interactions between the view and the model. It also helps manage the state changes in the 
model when some buttons are pressed as well as check if the user entries match the game entries. 

## Util

This section contains the utilities such as enumerations or interfaces for the game. It holds a state enumeration to 
help manage to different steps of the game as well as observer/observable interfaces to implement the *Observer* design pattern.
